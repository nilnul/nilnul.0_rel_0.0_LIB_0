﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.relation.re_
{
	/// <summary>
	/// Eg: for binary relation.
	/// If the domains are the same set or type,  and ranges are the same set or type,
	/// 
	/// we allow Type1 * Type2 to have two or more different/same RelS. Thus we can take one or two of them at different times, for example from db, resulting as subdb, or dataset.
	///
	/// To address same schemaed rels, we may index them or name them
	/// 
	/// </summary>
	public interface ISchemaSame
	{
	}
}


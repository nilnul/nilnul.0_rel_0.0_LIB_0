﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.relations
{
	/// <summary>
	/// given natural number :<var>n</var>, and a type G
	/// 
	///		for a family:{ Set[0],Set[1], ... Set[n-1]}, where the member of Set[i] is of type T[i], where T[i] is a subtype of G. T[i] and T[j] might be the same, So Set[i] and Set[j] might be the same. To address them separately, we can index them (thus put them in a str), or name them (give name to the type.)
	///
	///		(in fact , we have a apriori type system; type system is not part of the relations definition.)
	///		
	///		we can get a collection of tables. some of them might be of the same schema, say, T1*T2; So they are two (eq or not equal) instances of same relation schema. To address them, index or name them.
	///
	/// This is to the effece, a database (if type system is extracted in advance.)
	///			
	///
	/// 
	/// eg:
	///		we may have one instance (table) of Type1*Type2 rel, and have another instance(table) of type1*type2 rel. To different the same schema but different instances, we might index them by placing them into a str, or index them by name.
	///	vs: <see cref="nameof(ICombinated)"/>
	///		in <see cref="nameof(ICombinated)"/>, we don't need to keep more than one instances of same schema
	///		in <see cref="nameof(ICombinated)"/>, we can remove the conversed schema. So if T1*T2 schema exits, then T2*T1 schema is not needed.
	///	
	/// </summary>
	/// <remarks>
	/// alias:
	///		dataset
	/// </remarks>
	public interface IDb
	{
	}
}
/*
  this is infact can accomodate any relations
 */


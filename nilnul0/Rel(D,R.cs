﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.tuple
{
	public interface RelI<D,   R>
	{
		bool contains(D d, R r);
	}

	public interface RelI<T> : RelI<T, T> {

	}

	public interface RelI<D, TD_Eq,  R, TR_Eq>
		:RelI<D,R>
		where TD_Eq:IEqualityComparer<D>
		where TR_Eq:IEqualityComparer<R>
	{
	}


	public class Rel<D, R>
		: HashSet<Tuple<D, R>>
	{

		public Rel(IEqualityComparer<D> domainEq, IEqualityComparer<R> rangeEq)
			:base(
				new Eq<D,R>(domainEq,rangeEq)	 
			 )

		{

		}

		public Rel(

IEqualityComparer<D> domainEq, IEqualityComparer<R> rangeEq
			,IEnumerable<Tuple<D,R>> tuples
				
		)
			:base(tuples, new Eq<D, R>(domainEq, rangeEq))
		{

		}










	}
}

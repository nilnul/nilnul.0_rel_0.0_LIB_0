﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.trees_.elementCount_
{
	/*
	 发信人: gtgtjing (哦), 信区: Mathematics
标  题: 帮忙研究个计数问题
发信站: 水木社区 (Fri Jul 28 01:15:38 2017), 站内

正整数元素的n维向量，要求其中至少有1个不超过1的元素，至少有2个不超过的2的元素，……

这样的向量共有（n+1）^（n-1）个，恰好是n+1元带编号顶点的树的个数，这两者之间有什么内在联系呢？
--
※ 修改:·gtgtjing 于 Jul 28 11:20:20 2017 修改本文·[FROM: 111.30.132.*]
※ 来源:·水木社区 http://m.newsmth.net·[FROM: 111.30.132.*]



[本篇全文] [回复文章] [本篇作者：bsxfun] [回信给作者] [进入讨论区] [返回顶部][分享到 腾讯微博]
2

发信人: bsxfun (色即是空), 信区: Mathematics
标  题: Re: 帮忙研究个计数问题
发信站: 水木社区 (Fri Jul 28 09:22:19 2017), 站内

这个不同的树是怎么定义的？
比如n+1 = 3的时候，为什么是3颗不同的树？

【 在 gtgtjing 的大作中提到: 】
: 正整数元素的n维向量，要求其中至少有1个不超过1的元素，至少有2个不超过的2的元素，……
: 这样的向量共有（n+1）^（n-1）个，[color=#DC143C]恰好是n+1个顶点的树的个数[/color]，这两者之间有什么内在联系呢？

--

※ 来源:·水木社区 http://www.newsmth.net·[FROM: 180.173.67.*]



[本篇全文] [回复文章] [本篇作者：gtgtjing] [回信给作者] [进入讨论区] [返回顶部][分享到 腾讯微博]
3

发信人: gtgtjing (哦), 信区: Mathematics
标  题: Re: 帮忙研究个计数问题
发信站: 水木社区 (Fri Jul 28 11:15:24 2017), 站内

顶点带编号的不同的树，三个顶点就是全排列除以对称性的2
四个顶点就是12种一条线，和4种T型的

五个顶点就是60种一条线，60种Y型，5种十字型
【 在 bsxfun 的大作中提到: 】
: 这个不同的树是怎么定义的？
: 比如n+1 = 3的时候，为什么是3颗不同的树？
:
--
※ 修改:·gtgtjing 于 Jul 28 11:17:56 2017 修改本文·[FROM: 111.30.132.*]
※ 来源:·水木社区 http://m.newsmth.net·[FROM: 111.30.132.*]
		 */
	class Cardinality
	{

	}
}

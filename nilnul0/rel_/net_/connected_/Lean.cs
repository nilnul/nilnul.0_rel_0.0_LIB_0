﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.connected_
{
	/// <summary>
	/// should any step is removed, the net becomes not connected.
	/// </summary>
	/// <remarks>
	/// note :
	///		acyclic might be not lean.
	///			eg:
	///				a->b, b->c, a->c is acyclic, but not lean, as it's still connnected if  a->c were removed.
	///		lean cannot be cyclic. If there is a cycle, then there is some edge that can be leaned.
	///
	/// alias:
	///		minimal
	///		dedudant
	///		
	///	
	/// </remarks>
	public interface ILean {

	}
}

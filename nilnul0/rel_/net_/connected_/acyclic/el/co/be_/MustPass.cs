﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.dag.ele.duo.be_
{
	/// <summary>
	/// meld (ele is melded. ele1 is melding ele. ele1 is a meld)
	/// unison (ele is united; ele1 is a unison of ele. ele is united by ele1)
	/// juncture
	/// must pass
	/// merge
	/// converge
	/// control/mask/central/focus./junction/
	/// </summary>
	/// <!-- antonym: disjunction-->
	/// <remarks>
	/// given a type A, we have B and C extended from A. If D is also extended from B and C, thus also A, then D should be placed under namespace A. 
	/// For example, for a matrix, cell can be placed under both column (if it's a column major database) and row (if it's a row-major structure), so cell shall be placed under matrix. matrix is the cell's focal point
	/// </remarks>
	public class Meld<T>
	{
		/// <summary>
		/// ele must pass ele1 to get to the end. in other words, single source of authority.
		/// </summary>
		/// <param name="ele"></param>
		/// <param name="ele1"></param>
		/// <returns></returns>
		/// <remarks>
		/// for an organization, the president has the single source of authority over all the employees. But he exerts this by different paths: via different depts such as HR, Finance, etc to an employee.
		/// </remarks>
		public bool be(T ele, T ele1)
		{
			throw new NotImplementedException();
		}
	}
}

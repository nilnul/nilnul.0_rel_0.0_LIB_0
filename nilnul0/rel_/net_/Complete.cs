﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// mate includes all the cartesian product
	/// </summary>
	/// <remarks>
	/// different from total, which might be irreflexive, or be symmetric
	/// </remarks>
	public interface IComplete:
		INet

	{
	}
}

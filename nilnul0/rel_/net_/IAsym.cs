﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// for any co, either it is reflexive, or asymmetric.
	/// </summary>
	public interface IAsym :
		antisym_.irreflex_.IAsym, INet
	{
	}
}

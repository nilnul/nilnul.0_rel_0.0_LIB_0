﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net._mat
{
	public class Adjace : nilnul.obj.matrix.be_.square.vow.En<bool>

	{
		public Adjace(bool[,] matrix) : base(matrix)
		{
		}
		public Adjace():this(new bool[0,0])
		{

		}

		public int width {
			get {
				return this.matrix.GetLength(0);
			}
		}

		public bool this[int row, int col] {
			get {
				return this.matrix[row, col];
			}
		}
		public void orOtherRow(int rowsAugend, int rowsAddend)
		{
			for (int i = 0; i < matrix.GetLength(0); i++)
			{
				matrix[rowsAugend, i] |= matrix[rowsAddend, i];
			}
		}

		static public void OrOtherRow(bool[,] matrix, int rowsAugend, int rowsAddend)
		{
			for (int i = 0; i < matrix.GetLength(0); i++)
			{
				matrix[rowsAugend, i] |= matrix[rowsAddend, i];
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// there is at least one node.
	/// This is the primary taxonomy
	/// </summary>
	public interface IDwelt:INet { }
	public class Dwelt<TEl>
		: nilnul.rel_.net.vow.ee_.VowDefaulted<TEl, nilnul.rel_.net.be_.dwelt.Vow<TEl>>
	{
		public Dwelt(Net<TEl> val) : base(val)
		{
		}
	}
}

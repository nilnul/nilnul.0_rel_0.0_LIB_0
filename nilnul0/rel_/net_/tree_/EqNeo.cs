﻿using nilnul.rel_.net_._tree_;
using nilnul.rel_.net_.tree.set.be_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree_
{
	
	/// <summary>
	/// the root shouldn't be in any children
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TEq"></typeparam>
	public class EqNeo<T, TEq> : TreeI<T, TEq>
		where TEq : IEqualityComparer<T>, new()

	{
		private tree.set.be_.disjoint.En<T, TEq> _children;
		public tree.set.be_.disjoint.En<T, TEq> children
		{
			get
			{
				return _children;
				throw new NotImplementedException();
			}
		}

		private T _root;
		public T root
		{
			get
			{
				return _root;
				throw new NotImplementedException();
			}
		}

		Disjoint<T, TEq>.En Children<Disjoint<T, TEq>.En>.children => new Disjoint<T, TEq>.En(
			this._children.val
		);

		public EqNeo(T root, tree.set.be_.disjoint.En<T, TEq> children)
		{
			nilnul.Assert1.False(
				children.contains(root)	
			);

			_root = root;
			_children = children;
		}

		 public EqNeo(T root):this(
			root, new tree.set.be_.disjoint.En<T, TEq>() 
		)
		{

		}



	}


}

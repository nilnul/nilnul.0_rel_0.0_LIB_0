﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// 
	/// </summary>
	public class Path<T> :
		List<T>
		,


		_path_.BlankI
	{
		public Path()
		{
		}

		

		public Path(IEnumerable<T> collection) : base(collection)
		{
		}

		public override string ToString()
		{
			return string.Join(" --> ", this);
		}

		static public Path<T> Head(T head, Path<T> path ) {
			return new Path<T>( nilnul.obj.seq.convert_._PrependX.Prepend(path,head) );
		}

		static public Path<T> CreateSingular(T obj) {
			return new Path<T>(new T[] { obj} );
		}

	}
}

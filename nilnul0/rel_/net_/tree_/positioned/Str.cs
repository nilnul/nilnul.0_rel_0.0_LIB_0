﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree.positioned
{
	public class Str<T, TEq>
		: nilnul.obj.str_.Seq2<Positioned<T, TEq>>
		where TEq : IEqualityComparer<T>, new()
	{
		public Str(IEnumerable<Positioned<T, TEq>> boxed) : base(boxed)
		{
		}
		public Str():this( new Positioned<T,TEq>[0])
		{

		}
	}
}

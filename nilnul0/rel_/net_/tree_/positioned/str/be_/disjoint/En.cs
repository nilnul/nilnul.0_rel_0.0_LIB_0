﻿using nilnul.obj.be.vow_.xpN_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree.positioned.str.be_.disjoint
{


	public class En<T, TEq> :
		nilnul.obj.be.en_.BeDefaulted1<
			nilnul.rel_.net_.tree.positioned.Str<T, TEq>, Disjoint<T, TEq>
		>
		

	where TEq : IEqualityComparer<T>, new()
	{
		public En(Str<T, TEq> val) : base(val)
		{
		}
		public En():this(new Str<T, TEq>())
		{

		}

	

		public bool contains(T element)
		{

			return boxed.Any(tree =>  nilnul.rel_.net_.tree.positioned._ContainX.Contain(tree, element));

		}

	}


}

﻿using nilnul.rel_.net_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree.positioned
{
	static public class _ContainX
	{
		static public bool Contain<T,TEq>(this tree.Positioned<T,TEq> tree, T element)
			where TEq:IEqualityComparer<T>,new()
		{
			return SingletonByDefault<TEq>.Instance.Equals(tree.root, element) || tree.children.contains(element);

		}
	}
}

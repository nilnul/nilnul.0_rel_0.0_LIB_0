﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree.positioned
{
	static public class _NodesX
	{
		/// <summary>
		/// depth first
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TEq"></typeparam>
		/// <param name="tree"></param>
		/// <returns></returns>
		static public IEnumerable<T> Nodes<T,TEq>( this
			tree.Positioned<T,TEq> tree
			
			)
			where TEq :IEqualityComparer<T>,new()

		{
			yield return tree.root;

			foreach (var item in tree.children.ee)
			{
				foreach (var item1 in item.field)
				{
					yield return item1;
				}
			}


		
		}
	}
}

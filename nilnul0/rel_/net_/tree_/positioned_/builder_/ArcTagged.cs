﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree_.positioned_.builder_
{
	public class ArcTagged<TNode, TNodeEq, TTag>
		:
		nilnul.obj.Box_pub<Builder1<TNode, TNodeEq>>

		where TNodeEq:IEqualityComparer<TNode>,new()
	{
		public nilnul.rel_.net_.tree_.positioned_.builder_._arcTagged.Tags<TNode,TNodeEq,TTag>  dict
			= new _arcTagged.Tags<TNode, TNodeEq, TTag>();

		public ArcTagged(Builder1<TNode, TNodeEq> val) : base(val)
		{

		}
	}
}

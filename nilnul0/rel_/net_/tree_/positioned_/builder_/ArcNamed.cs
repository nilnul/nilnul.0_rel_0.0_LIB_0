﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree_.positioned_.builder_
{
	public class ArcNamed<T, TEq>
		:
		nilnul.obj.Box_pub<Builder1<T, TEq>>
		where TEq:IEqualityComparer<T>,new()
	{
		public nilnul.obj.Dict<(T, T), string, nilnul.obj.co.eq_.Defaulted<T, TEq>> dict
			= new obj.Dict<(T, T), string, obj.co.eq_.Defaulted<T, TEq>>();

		public ArcNamed(Builder1<T, TEq> val) : base(val)
		{

		}
	}
}

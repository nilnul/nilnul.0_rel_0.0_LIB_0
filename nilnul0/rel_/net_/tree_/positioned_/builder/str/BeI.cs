﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree_.positioned_.builder.str
{
	public interface BeI<T,TEq>
		:
		nilnul.obj.BeI1< rel_.net_.tree_.positioned_.builder.Str<T,TEq>>
		where TEq:IEqualityComparer<T>,new()
	{
	}

	public class Be<T, TEq>
		: nilnul.obj.Be1<rel_.net_.tree_.positioned_.builder.Str<T, TEq>>
		,
		BeI<T, TEq>

		where TEq : IEqualityComparer<T>, new()
	{
		public Be(System.Func<Str<T, TEq>, bool> func) : base(func)
		{
		}
	}
}

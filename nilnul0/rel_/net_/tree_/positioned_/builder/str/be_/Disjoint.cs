﻿using nilnul.rel_.net_;
using nilnul.rel_.net_.tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree_.positioned_.builder.str.be_
{
	static public class _DisjointX
	{
		static public bool _Be<T,TEq>(
			IEnumerable<Builder1<T,TEq>> _trees_finite
		)
		where TEq:IEqualityComparer<T>,new()
		{
			return nilnul.set.family.be_._DisjointX.Be<T,TEq>(
				_trees_finite.Select(tree=>tree.field)
			);
		}


	}
	public class Disjoint<T,TEq> :positioned_.builder.str.Be<T,TEq>
		where TEq:IEqualityComparer<T>,new()
	{



		public Disjoint() : base(_DisjointX._Be)
		{
		}

		static public Disjoint<T,TEq> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<Disjoint<T,TEq>>.Instance;
			}
		}

	}
}

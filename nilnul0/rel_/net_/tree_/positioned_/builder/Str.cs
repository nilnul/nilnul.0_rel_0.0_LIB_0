﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree_.positioned_.builder
{
	public class Str<T, TEq>
		: nilnul.obj.str_.Seq2<Builder1<T, TEq>>
		where TEq : IEqualityComparer<T>, new()
	{
		public Str(IEnumerable<Builder1<T, TEq>> boxed) : base(boxed)
		{
		}
		public Str():this( new Builder1<T,TEq>[0])
		{

		}
	}
}

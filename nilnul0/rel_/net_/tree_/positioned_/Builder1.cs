﻿//extern alias obj;

using nilnul.rel_.net_.tree.set.be_;
using /*obj::*/nilnul.obj.seq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree_.positioned_
{


	/// <summary>
	/// children are put as a str. A tree, but the position of children is significant
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TEq"></typeparam>
	public class Builder1<T, TEq>
		:
	net_._tree_.RootI<T>
		

		where TEq : IEqualityComparer<T>, new()

	{
		static public TEq ObjEq { get { return nilnul.obj_.Singleton<TEq>.Instance; } }
		private List<Builder1<T,TEq>> _children;
		public List<Builder1<T, TEq>> children
		{
			get
			{
				return _children;
			}
		}

		private T _root;
		public T root
		{
			get
			{
				return _root;
				throw new NotImplementedException();
			}
		}

		public Builder1(T root, List<Builder1<T, TEq>> children)
		{

			new nilnul.bit.vow_.true_.Unacceptable($"{children} is not disjoint").vow(

				nilnul.rel_.net_.tree.set.be_.Disjoint<T, TEq>.Singleton.be(children)
			)

			;

			new nilnul.bit.vow_.true_.Unacceptable($"{children} is not disjoint").vow(

				nilnul.obj.seq.be_._NoneX.None(children, x => x.field.Contains(root, ObjEq))
			);

			_root = root;
			_children = children;
		}

		public Builder1(T root) : this(
		   root,new List<Builder1<T, TEq>>()
	   )
		{
		}

		 public void append( T startPointInTree, T leaf)
		{
			if (ObjEq.Equals(startPointInTree,root))
			{
				this.children.Add(
					new Builder1<T, TEq>(leaf)
				);
				return;
			}
			 children.First(
				x=>x.contain(startPointInTree)
			).append(startPointInTree,leaf);

			//if (first is null)
			//{
			//	throw 
			//}
		}

		static public Builder1<T, TEq> Prepend( Builder1<T,TEq> tree,  T newRoot)
		{
			return new Builder1<T, TEq>(newRoot, new List<Builder1<T, TEq>>() { tree });
		}

		static public bool Contain(T root, List<Builder1<T, TEq>> children, T node) {
			return ObjEq.Equals(root, node) || children.Any(x => x.contain(node));
		}

		

		public bool contain(T node) {
			return Contain(this.root, this.children,node);
		}

		public bool notContain(T element)
		{
			return !contain(element);
		}


		public IEnumerable<T> field {
			get {
				yield return root;
				foreach (var item in children.SelectMany(x=>x.field))
				{
					yield return item;
				}
			}
		}

		public int depth()
		{
			if (children.Any())
			{
				return 1 + children.Select(c => c.depth()).Max();

			}
			return 1;
		}
		public int width()
		{

			if (children.Any())
			{
				return children.Select(c => c.width()).Sum();

			}
			return 1;
		}

		/// <summary>
		/// for leaves, get path from root to each of them
		/// </summary>
		public IEnumerable<nilnul.rel_.net_.Path<T>> paths
		{
			get
			{
				if (this.children.Any())
				{
					foreach (var item in this.children.SelectMany(i => i.paths))
					{
						yield return Path<T>.Head(
							root, item
						);
					}

				}
				else
				{
					yield return Path<T>.CreateSingular(root);
				}
			}
		}

		public override string ToString()
		{
			return string.Join(Environment.NewLine, paths);


		}


	}


}

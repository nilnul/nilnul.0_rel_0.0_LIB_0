﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.net.tree_
{
	public class Builded<T,TEq>
		:nilnul.Box1<nilnul.rel.Net_eqDefault<T,TEq>>

		where TEq: IEqualityComparer<T>,new()
	{


		private Builded(nilnul.rel.Net_eqDefault<T,TEq> net):base(net)
		{

		}

		public bool contains(T element) {
			return this.boxed.nodes.Contains(element);
		}

		public bool notContains(T element) {
			return !contains(element);
		}
		static public Builded<T, TEq> Create(T root) {
			return new Builded<T, TEq>(
				Net_eqDefault<T, TEq>.Create_byAppendPoints(root)
			);
		}

		static public Builded<T, TEq> Create(Builded<T,TEq> builded, T startPointInTree, T leaf) {
			nilnul.Assert1.True(
				builded.contains(startPointInTree)
				,
				builded.notContains(leaf)
			);
			return new Builded<T, TEq>(
				Net_eqDefault<T, TEq>.Create_byAppendPoints(

					new Set_eqDefault<T, TEq>(
						builded.boxed.nodes.Concat(new[] { leaf })

					)
					, 
					Rel_elementEqDefault<T, TEq>.Create(
					
						builded.boxed.edges.Concat(new [] { new Tuple<T,T>(startPointInTree,leaf) })
					)

				)
			);
		}
	}
}

﻿namespace nilnul.rel_.net_.tree_
{
	/// <summary>
	/// each node can have at most two children.
	/// In other words, a node can have 0, 1, or 2 children.
	/// </summary>
	public interface IBinary:ITree { }
}

﻿using nilnul.rel_.net_.tree.set.be_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree
{


	/// <summary>
	/// children are put as a str. A tree, but the position of children is significant
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TEq"></typeparam>
	public class Positioned<T, TEq>
		:
	net_._tree_.RootI<T>


		where TEq : IEqualityComparer<T>, new()

	{
		private positioned.str.be_.disjoint.En<T, TEq> _children;
		public positioned.str.be_.disjoint.En<T, TEq> children
		{
			get
			{
				return _children;
			}
		}

		private T _root;
		public T root
		{
			get
			{
				return _root;
				throw new NotImplementedException();
			}
		}
		public Positioned(T root, positioned.str.be_.disjoint.En<T, TEq> children)
		{
			nilnul.Assert1.False(
				children.contains(root)
			);

			_root = root;
			_children = children;
		}

		public Positioned(T root) : this(
		   root, new positioned.str.be_.disjoint.En<T, TEq>()
	   )
		{
		}

		public IEnumerable<T> field
		{
			get
			{
				yield return root;
				foreach (var item in children.ee.SelectMany(x => x.field))
				{
					yield return item;
				}
			}
		}

		public int depth()
		{
			if (children.ee.Any())
			{
				return 1 + children.ee.Select(c => c.depth()).Max();

			}
			return 1;
		}
		public int width()
		{

			if (children.ee.Any())
			{
				return children.ee.Select(c => c.width()).Sum();

			}
			return 1;
		}

		/// <summary>
		/// for leaves, get path from root to each of them
		/// </summary>
		public IEnumerable<nilnul.rel_.net_.Path<T>> paths
		{
			get
			{
				if (this.children.ee.boxed.Any())
				{
					foreach (var item in this.children.ee.boxed.SelectMany(i => i.paths))
					{
						yield return Path<T>.Head(
							root, item
						);
					}

				}
				else
				{
					yield return Path<T>.CreateSingular(root);
				}
			}
		}

		public override string ToString()
		{
			return string.Join(Environment.NewLine, paths);


		}


	}


}

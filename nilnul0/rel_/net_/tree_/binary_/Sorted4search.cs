﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree_.binary_
{
	/// <summary>
	/// The left subtree of a node contains only nodes with keys lesser than the node’s key.
	/// The right subtree of a node contains only nodes with keys greater than the node’s key.
	/// </summary>
	///The left and right subtree each must also be a binary search tree.
	///<remarks>
	/// 
	public interface ISorted4search:IBinary
	{
	}
}

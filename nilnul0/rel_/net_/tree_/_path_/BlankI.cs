﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._path_
{
	/// <summary>
	/// from one node:"start", goes thru a path, we can get to another node:"stop". "stop" may be the same as "start" when path is empty.
	/// </summary>
	public interface BlankI
	{
	}
}

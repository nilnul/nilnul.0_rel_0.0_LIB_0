﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.equiv_
{
	/// <summary>
	/// for a <see cref="nilnul.obj.algebra_.pargoid_.magma_.semigrp_.monoid_.grp"/>
	/// ,
	///  a R b, cR d, means a*b R cd
	///  then R is called congruence.
	/// </summary>
	/// <see cref="nilnul.obj.algebra_.pargoid_.magma_.semigrp_.monoid_.grp.el.rel_.Congruence"/>
	class Congruence
	{
	}
}

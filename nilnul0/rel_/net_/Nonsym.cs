﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// for any co, either it is reflexive, or asymmetric.
	/// </summary>
	public interface INonsym:
		INet
		,
		_obj.typ.child.calc_.unary_.IComplement<INet,ISym>

	{
	}
}

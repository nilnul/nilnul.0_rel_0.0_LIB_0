﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// for poly (more than 2) nodes, there is no cycle.
	/// but this might be reflexivesome, or symmetric4some
	/// eg:
	/// a->b, b -> c, c->a
	/// </summary>
	public interface IAcyclic4poly
	{
	}
}

﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dwelt.be
{
	public class Vow<T> : nilnul.obj.be.Vow4<nilnul.rel_.net_.Dwelt<T>>
		,
		rel_.net_.dwelt.VowI<T>
	{
		public Vow(BeI1<Dwelt<T>> be) : base(be)
		{
		}
	}
}

﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dwelt.vow
{
	public class Ee<T>
		: nilnul.obj.vow.Ee1<rel_.net_.Dwelt<T>>
	{
		public Ee(net_.Dwelt<T> val, VowI2<Dwelt<T>> vow) : base(val, vow)
		{
		}
	}
}

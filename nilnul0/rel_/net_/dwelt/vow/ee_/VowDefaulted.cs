﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dwelt.vow.ee_
{
	public class VowDefaulted<T, TVow> :
		Ee<T>
		where TVow: VowI2<Dwelt<T>> ,new()
	{
		public VowDefaulted(Dwelt<T> val) : base(val, nilnul.obj_.Singleton<TVow>.Instance)
		{
		}
	}
}

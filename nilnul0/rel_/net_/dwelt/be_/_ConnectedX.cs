﻿////extern alias obj;

using nilnul.collection.set;
using nilnul.obj.seq.be_;
//using obj::nilnul.obj.seq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel_.net_.dwelt.be_
{
	/// <summary>
	/// partiton the net into components, each of which is connected.
	/// Note: empty net is defined as connected
	/// </summary>
	static public class _ConnectedX
	{
		static public bool IsConnected<T>(
			this nilnul.rel_.net_.Dwelt<T> net
		)
		{
			var remainedNodes = net.ee.field;

			var connectedNodes = new HashSet<T>(net.ee.match.objEq) {  };

			var newlyConnected = new[] { remainedNodes.First()}; 

			while (newlyConnected.Any())
			{
				connectedNodes.AddRange(newlyConnected);

				remainedNodes.ExceptWith(newlyConnected);
				
				;

				newlyConnected = remainedNodes.Where(
					node => connectedNodes.Any(
						current => net.ee.match.has(current, node)
						||
						net.ee.match.has(node, current)
					)
				).ToArray();
			}
			return remainedNodes.None();




		}
		
	}
}

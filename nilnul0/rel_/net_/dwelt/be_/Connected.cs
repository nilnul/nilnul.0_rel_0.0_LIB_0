﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dwelt.be_
{
	/// <summary>
	/// </summary>
	/// <remarks>
	/// <see cref="nameof(nilnul.rel_.net.be_.Cruxed)"/>
	/// </remarks>
	public class Connected<T>
		:nilnul.rel_.net_.dwelt.Be<T>
	{
		public Connected() : base(_ConnectedX.IsConnected)
		{
		}

		


		static public Connected<T> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<Connected<T>>.Instance;
			}
		}




	}
	

}

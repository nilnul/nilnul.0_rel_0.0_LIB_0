﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dwelt
{
	//extern alias obj;
	public interface BeI<T>
		:
		nilnul.obj.BeI1< nilnul.rel_.net_.Dwelt<T>>

	{

	}

	public class Be<T> :
		nilnul.obj.Be1<
nilnul.rel_.net_.Dwelt<T>
		>,

		BeI<T>
	{
		public Be(System.Func<Dwelt<T>, bool> func) : base(func)
		{
		}
	}



}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// <see cref="nameof(net_.ICompatible)"/>
	/// In order theory, a branch of mathematics, a semiorder is a type of ordering that may be determined for a set of items with numerical scores by declaring two items to be incomparable when their scores are within a given margin of error of each other, and by using the numerical comparison of their scores when those scores are sufficiently far apart. 
	/// </summary>
	public interface ISemiOrder
		:
		net_.reflex_.ISym
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.reflex_.sym_
{
	public interface ICompatible
		:
		ISym
		,
		_obj.typ.child.calc_.unary_.IAlias<ISym,ICompatible>
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.reflex_
{
	public interface ICyclic
		:
		IReflex
		,
		_obj.typ.child.calc_.unary_.comple_.IReflec<IReflex,AcyclicException>
		,
		_obj.typ.child.calc_.unary_.IAlias<IReflex,ICyclic>
		,
		net_.ICyclic4some
		
	{
	}
}

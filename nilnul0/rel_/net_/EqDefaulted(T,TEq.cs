﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nilnul.obj;
using nilnul.rel_._net;

namespace nilnul.rel_.net_
{
	public class EqDefault1<T,TEq> :
		Net1<T>
		where TEq:IEqualityComparer<T>,new()
	{
		public EqDefault1(
			List<T> nodes
			,
			List< (T, T) > duos
		):
			base(
			nilnul.obj_.Singleton<TEq>.Instance
				,duos
			,
			nodes
			)
		{
			
		}

		public EqDefault1(List<T> objs, List<Tuple<T, T>> duos)
			:
base(
			nilnul.obj_.Singleton<TEq>.Instance
			,duos
			,
			objs
			)
		{
			
		}
		public EqDefault1(IEnumerable<T> objs, List<Tuple<T, T>> duos)
			:
base(
			nilnul.obj_.Singleton<TEq>.Instance
			,duos
			,
			objs
			)
		{
			
		}
		public EqDefault1(IEnumerable<T> objs, IEnumerable<Tuple<T, T>> duos)
			:
base(
			nilnul.obj_.Singleton<TEq>.Instance
			,duos
			,
			objs
			)
		{
			
		}

		public EqDefault1(IEnumerable<T> objs, IEnumerable<(T, T)> duos)
			:
base(
			nilnul.obj_.Singleton<TEq>.Instance
			,duos
			,
			objs
			)
		{
			
		}

		public EqDefault1(IEnumerable<(T, T)> duos, List<T> objs) : base(nilnul.obj_.Singleton<TEq>.Instance, duos, objs)
		{
		}
		public EqDefault1(List<Tuple<T, T>> duos, List<T> objs) : base(nilnul.obj_.Singleton<TEq>.Instance, duos, objs)
		{
		}
		public EqDefault1( IEnumerable<Tuple<T, T>> duos, IEnumerable<T> objs) : base(nilnul.obj_.Singleton<TEq>.Instance, duos, objs)
		{
		}

		public EqDefault1(Mate<T> mate0, IEnumerable<T> extraNodes) : base(mate0, extraNodes)
		{
		}




		public EqDefault1( IEnumerable<CoI2<T>> duos, IEnumerable<T> objs) : base(nilnul.obj_.Singleton<TEq>.Instance, duos, objs)
		{
		}

		public EqDefault1( IEnumerable<(T, T)> duos, IEnumerable<T> objs) : base(nilnul.obj_.Singleton<TEq>.Instance, duos, objs)
		{
		}
		public EqDefault1( IEnumerable<T> duos) : base(nilnul.obj_.Singleton<TEq>.Instance, duos)
		{
		}
		public EqDefault1( IEnumerable<(T, T)> duos) : base(nilnul.obj_.Singleton<TEq>.Instance, duos)
		{
		}
		public EqDefault1( IEnumerable<Tuple<T, T>> duos) : base(nilnul.obj_.Singleton<TEq>.Instance, duos)
		{
		}

		public EqDefault1(Mate<T> mate0) : base(mate0)
		{
		}

		public EqDefault1():this(new List<T>(),new List<(T, T)>())
		{

		}
		



		

		

		
	}
}

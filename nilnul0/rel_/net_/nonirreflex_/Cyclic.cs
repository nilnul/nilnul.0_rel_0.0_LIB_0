﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.nonirreflex_
{
	public interface ICyclic
		:
		INonirreflex
		,
		nilnul._obj.typ.child.calc_.unary_.IAlias<INonirreflex,ICyclic> /// if it's nonirreflex, then it's cyclic
	{
	}
}

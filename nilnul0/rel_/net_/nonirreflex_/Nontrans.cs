﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.nonirreflex_
{
	/// <summary>
	/// eg:
	///		a->a, a->b, b->c
	/// </summary>
	public interface INontrans
		:
		INonirreflex
		,
		
		_obj.typ.child.calc_.unary_.IComplement<
			INonirreflex,
			rel_.net_.nonirreflex_.ITrans
		>
		,
		net_.INontrans

	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.nonirreflex_
{
	/// <summary>
	/// eg:
	///		a->a 
	/// </summary>
	public interface ITrans
		:
		INonirreflex
		,

		nilnul._obj.typ.child.calc_.unary_.IComplement<INonirreflex, INontrans> 
		,
		_obj.typ.calc_.unary_.IAlias<trans_.IAcyclic>

		,
		net_.ITrans
		


		
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// some cos is reflexive. some cos might be not.
	/// </summary>
	/// <remarks>
	/// </remarks>
	public interface INonreflex:
		INet
		,
		_obj.typ.child.calc_.unary_.IComplement<INet, IReflex>
	{
	}
}

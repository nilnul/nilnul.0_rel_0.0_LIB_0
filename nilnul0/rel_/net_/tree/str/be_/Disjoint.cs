﻿using nilnul.rel_.net_;
using nilnul.rel_.net_.tree;
using nilnul.rel_.net_.tree_.positioned_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree.str.be_
{
	public class Disjoint
	{
		static public bool _Be<T,TEq>(
			IEnumerable<TreeI<T,TEq>> _trees_finite
		)
		where TEq:IEqualityComparer<T>,new()
		{
			return nilnul.set.str.be.Disjoint<T,TEq>.Eval(
				_trees_finite.Select(tree=>tree.Nodes())
			);
		}

		static public bool _Be<T,TEq>(
			IEnumerable<Builder1<T, TEq>> _trees_finite
		)
		where TEq:IEqualityComparer<T>,new()
		{
			return nilnul.obj.set.str.be_._DisjointX.Be<T,TEq>(
				_trees_finite.Select(tree=>tree.field)
			);
		}

	}
}

﻿using nilnul.rel_.net_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.net.tree
{
	static public class _ContainsX
	{
		static public bool Contains<T,TEq>(this TreeI<T,TEq> tree, T element)
			where TEq:IEqualityComparer<T>,new()
		{
			return SingletonByDefault<TEq>.Instance.Equals(tree.root, element) || tree.children.contains(element);

		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree
{
	static public class _NodesX
	{
		static public Set_eqDefault<T, TEq> Nodes<T,TEq>( this
			TreeI<T,TEq> tree
			
			)
			where TEq :IEqualityComparer<T>,new()

		{
			var r = new Set_eqDefault<T, TEq>();
			r.Add(tree.root);
			foreach (var item in tree.children.toEnumerable())
			{

				r.add(_NodesX.Nodes(item));
			}

			return r;

		
		}
	}
}

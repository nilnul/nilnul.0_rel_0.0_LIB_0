﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree.set.be_.disjoint
{
	public class En<T, TEq> :
		nilnul.be.Asserted<nilnul.rel_.net_.tree.Set<T, TEq>, Disjoint<T, TEq>>
	where TEq : IEqualityComparer<T>, new()
	{
		public En() : this(
			new Set<T, TEq>()
		)
		{
		}
		public En(
			nilnul.rel_.net_.tree.Set<T, TEq> set
		) : base(set)
		{
		}
		public IEnumerable<TreeI<T, TEq>> toEnumerable()
		{
			return this.val;
		}
		public bool contains(T element)
		{
			return val.Any(tree => nilnul.rel.net.tree._ContainsX.Contains(tree, element));
		}
	}
}
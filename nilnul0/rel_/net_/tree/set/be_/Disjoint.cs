﻿using nilnul.rel_.net_.tree_.positioned_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.tree.set.be_
{
	/// <summary>
	/// any pair in the set is disjoint
	/// </summary>
	public class Disjoint<T,TEq>
		:nilnul.BeI<nilnul.rel_.net_.tree.Set<T,TEq>>

		where TEq :IEqualityComparer<T>,new()
	{
		public bool be(Set<T, TEq> obj)
		{
			return nilnul.rel_.net_.tree.str.be_.Disjoint._Be(obj);

			throw new NotImplementedException();
		}

		public bool be(List<Builder1<T, TEq>> children) {
			return nilnul.rel_.net_.tree.str.be_.Disjoint._Be(children);

		}

		static public Disjoint<T,TEq> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<Disjoint<T,TEq>>.Instance;
			}
		}


		[Obsolete(nameof(disjoint.En<T,TEq>))]
		public class En:nilnul.be.Asserted<nilnul.rel_.net_.tree.Set<T,TEq>,Disjoint<T,TEq>>
		{
			public En():this(
				new Set<T, TEq>()
			)
			{

			}

			public En(
				nilnul.rel_.net_.tree.Set<T,TEq> set
			):base(set)
			{

			}
			public IEnumerable<TreeI<T, TEq>> toEnumerable() {


				return this.val;


				throw new NotImplementedException();
			}

			public bool contains(T element) {

				return val.Any(tree=>nilnul.rel.net.tree._ContainsX.Contains(tree,element));

			}

		}
	}


	[Obsolete("Disjoint")]
	public class Forest
	{

	}
}

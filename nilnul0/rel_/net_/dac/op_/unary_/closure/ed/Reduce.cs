﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dwelt_.connected_.acyclic.op_.unary_.closure.ed
{

	static public class _ReduceX
	{
		static public void Act_assumeClosure(bool[,] _closured)
		{
			var len = _closured.GetLength(0);

			for (int col = 0; col < len; col++)
			{
				for (int row = 0; row < len; row++)
				{
					if (_closured[row, col])
					{
						for (int k = 0; k < len; k++)
						{
							if (_closured[col, k])
							{
								_closured[row, k] = false;
							}
						}
					}
				}
			}
		}
	}

	/// <summary>
	/// According to Wikipedia, "If a given graph is a finite directed acyclic graph, its transitive reduction is unique"
	/// </summary>
	 class Reduce
	{
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dwelt_.connected_.acyclic.op_.unary_
{
	/// <summary>
	/// make a transitivity reduction
	/// </summary>
	static  public class _ReduceX
	{
		static nilnul.rel_.Net1<T> Net_assumeDweltConnectedAcyclic<T>(
			nilnul.rel_.Net1<T> net

		) {
			var matrix = nilnul.rel_.net._mat.Adjacency.GetRelationMatrix(net);

			///closure it
			///
			nilnul.rel_.net_.dwelt_.connected_.acyclic.op_.unary_._ClosureX._Act(matrix);

			//reduce matrix

			nilnul.rel_.net_.dwelt_.connected_.acyclic.op_.unary_.closure.ed._ReduceX.Act_assumeClosure(
				matrix
			);



			///generate net from matrix
			///

			var matrixed = new nilnul.rel_.net_.Matrixed<T>(
				net.field
				,
				matrix
			);

			return matrixed.clone();





		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// some cirle exists should we make it transitive
	/// </summary>
	public interface ICyclic4some
		: _obj.typ.child.calc_.unary_.IComplement<INet,IAcyclic4all>
		,
		INet
	{	}

	public interface ICyclic4some<T> : ICyclic4some { }


}

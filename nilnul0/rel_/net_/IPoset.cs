﻿namespace nilnul.rel_.net_
{
	/// <summary>
	/// partially ordered set. 
	/// </summary>
	/// <remarks>
	/// nomenclature:
	///		ord is used as nilnul.num.Ord, which is the index of a linear|complete/total order
	///	alias:
	///		taxinomy
	/// </remarks>
	public interface IPoset :
		preord_.IAntisym
		//,
		//IAcyclic4all
	{
	}
}

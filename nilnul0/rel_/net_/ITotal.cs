﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// for any two nodes,  either (x,y) or (y,x)  is in mate, or both  is in. (if the net is sym 4 some)
	/// 
	/// </summary>
	/// <remarks>
	/// different from complete, as for a co and its converse, we may have only one, not two.
	/// </remarks>
	/// <see cref="rel_.ITotal"/>,  <see cref="net_.ITotal"/> is also <see cref="rel_.ITotal"/>
	public interface ITotal:
		INet
		,
		rel_.ITotal
		

	{
	}
}

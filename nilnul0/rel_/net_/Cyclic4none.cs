﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// all in circle.
	/// Note for empty net, this is also cyclic4none
	/// </summary>
	public interface ICyclic4none : INet        //: _obj.typ.child.calc_.unary_.IComplement<INet,IAcyclic>
	{ }

	public interface ICyclic4none<T> : ICyclic4none { }


}

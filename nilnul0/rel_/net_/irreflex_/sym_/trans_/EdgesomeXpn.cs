﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.irreflex_.sym_.trans_
{
	

	[Serializable]
	public class EdgesomeException : Exception
	{
		public EdgesomeException() { }
		public EdgesomeException(string message) : base(message) { }
		public EdgesomeException(string message, Exception inner) : base(message, inner) { }
		protected EdgesomeException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

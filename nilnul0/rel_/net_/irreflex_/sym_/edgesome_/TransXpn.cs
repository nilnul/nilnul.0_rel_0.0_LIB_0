﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.irreflex_.sym_.edgesome_
{

	/// <summary>
	/// somewhere it might be trans. But it cannot be all trans.
	/// </summary>

	[Serializable]
	public class TransException : Exception
	{
		public TransException() { }
		public TransException(string message) : base(message) { }
		public TransException(string message, Exception inner) : base(message, inner) { }
		protected TransException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

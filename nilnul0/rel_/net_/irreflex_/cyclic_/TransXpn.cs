﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.irreflex_.cyclic_
{
	/// <summary>
	/// if it's trans, then due to being cyclic, it's reflexive, contradicting to <see cref="nameof(irreflex_)"/>
	/// 
	/// </summary>

	[Serializable]
	public class TransException : Exception
		,
		ICyclic
		,
		nilnul._obj.typ.calc_.unary_.IAlias<trans_.CyclicException>
		,
		_obj.typ.calc_.binary_.intersect_.xpn.IAlias<irreflex_.ITrans, irreflex_.ICyclic>

	{
		public TransException() { }
		public TransException(string message) : base(message) { }
		public TransException(string message, Exception inner) : base(message, inner) { }
		protected TransException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.irreflex_.asym_
{
	/// <summary>
	/// for poly (more than 2) nodes, there is no cycle.
	/// eg:
	/// a->b, b -> c, c->a
	/// </summary>
	public interface IAcyclic4poly:IAsym,net_.IAcyclic4poly
	{
	}
}

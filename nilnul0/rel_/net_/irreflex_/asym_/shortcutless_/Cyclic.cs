﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.irreflex_.asym_.shortcutless_
{
	/// <summary>
	/// can still be cyclic.
	///	eg:
	///		a->b, b->c, c->a. This cannot be reduced.
	/// </summary>
	class Cyclic
	{
	}
}

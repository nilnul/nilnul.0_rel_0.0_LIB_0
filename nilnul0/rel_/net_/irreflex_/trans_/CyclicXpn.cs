﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.irreflex_.trans_
{
	/// <summary>
	/// if there is a cycle, then one of the el is reflexive due to the transitivity.
	/// </summary>

	[Serializable]
	public class CyclicException
		:
		Exception
		,
		ITotal
		,
		_obj.typ.calc_.binary_.intersect_.xpn.IAlias< irreflex_.ITrans,irreflex_.ICyclic >

		//,
		//nilnul._obj.typ.calc_.unary_.IAlias< cyclic_.TransException>


	{
		public CyclicException() { }
		public CyclicException(string message) : base(message) { }
		public CyclicException(string message, Exception inner) : base(message, inner) { }
		protected CyclicException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.irreflex_.trans_
{
	/// <summary>
	/// cannot be cyclic
	/// </summary>
	public interface IAcyclic
		:
		ITotal
		,
		irreflex_.IAcyclic
		,

		_obj.typ.child.calc_.unary_.comple_.IReflec<ITotal,CyclicException>

	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.irreflex_.trans_.edgesome_
{
	/// <summary>
	/// 
	/// </summary>

	[Serializable]
	public class SymException : Exception
	{
		public SymException() { }
		public SymException(string message) : base(message) { }
		public SymException(string message, Exception inner) : base(message, inner) { }
		protected SymException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}



}

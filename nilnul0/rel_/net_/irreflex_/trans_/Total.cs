﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.irreflex_.trans_
{
	/// <summary>
	/// </summary>
	/// <remarks>
	/// strict weak order. <see cref="nameof(net_.IWeakOrder)"/>
	/// 
	/// </remarks>
	public interface ITotal
		:
		irreflex_.ITrans
		,
		net_.ITotal
		
		


		
	{
	}
}

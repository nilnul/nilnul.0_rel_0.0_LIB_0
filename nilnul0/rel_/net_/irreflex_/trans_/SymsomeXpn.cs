﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.irreflex_.trans_
{
	/// <summary>
	/// alias: nonAsym
	/// a subtype of <see cref="nameof(net_.IDominant)"/>
	/// </summary>
	public class SymsomeException : Exception
	{
		public SymsomeException() { }
		public SymsomeException(string message) : base(message) { }
		public SymsomeException(string message, Exception inner) : base(message, inner) { }
		protected SymsomeException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
	
}

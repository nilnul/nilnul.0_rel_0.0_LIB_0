﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// taxonomy, unlike type, allows no mulitiple super category.
	/// </summary>
	/// <seealso cref="rel_.net_.tree.to_.Poset"/>
	/// alias:
	///		category
	public interface ITaxonomy
		:
		INet
		,
		net_.poset_.unimin_.ITaxonomy
	{

	}
}

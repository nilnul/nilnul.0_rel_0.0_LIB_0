﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.uncruxed.to_.cruxed_
{

	/// <summary>
	/// using a special element called: Inf or God, then we can draw arrow from the god to all other elements such that:
	///		1) all elements connecting to the god in the same direction: from Element to Infinity
	///		1.1) Infinity cannot be pass thru such that a message can be passed by one Element thru Infinti to other Element.
	/// </summary>
	/// <remarks>
	/// We can relax the unidirection constraint: let Ghost can connect to each element in one direction or the other or both.
	/// </remarks>
	class Inf
	{
	}
}

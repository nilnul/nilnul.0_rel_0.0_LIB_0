﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.acyclic_
{
	/// <summary>
	/// transitive closure. No more edges can be added to keep it being acyclic.
	/// Note: this is not reflexive; hence it's not a poset.
	/// </summary>
	public interface IClosured
	{
	}
}

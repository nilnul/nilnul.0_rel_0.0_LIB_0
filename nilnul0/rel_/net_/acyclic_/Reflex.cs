﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.acyclic_
{

	[Serializable]
	public class ReflexException : Exception
		,
		IAcyclic4all
	{
		public ReflexException() { }
		public ReflexException(string message) : base(message) { }
		public ReflexException(string message, Exception inner) : base(message, inner) { }
		protected ReflexException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

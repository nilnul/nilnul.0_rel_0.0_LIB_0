﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.acyclic_
{

	/// <summary>
	/// if there is some co that is reflexive, then there is a cycle, resulting a contradiction. So no co is reflexive; And this is irreflex.
	/// </summary>
	/// <remarks>
	/// alias:
	///		reflexsome Xpn
	/// </remarks>
	[Serializable]
	public class NonirreflexException : Exception
		,
		IReflex
	{
		public NonirreflexException() { }
		public NonirreflexException(string message) : base(message) { }
		public NonirreflexException(string message, Exception inner) : base(message, inner) { }
		protected NonirreflexException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

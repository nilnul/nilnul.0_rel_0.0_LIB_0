﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.acyclic_
{
	/// <summary>
	/// eg:
	///		a->b, b->a
	/// </summary>
	public interface INonreflex
		:net_.IAcyclic4all
		,
		_obj.typ.child.calc_.unary_.comple_.IReflec<IAcyclic4all, ReflexException>

		,
		net_.INonreflex
	{
	}
}

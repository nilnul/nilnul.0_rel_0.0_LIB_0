﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.acyclic_
{
	public interface IIrreflex
		:IAcyclic4all
		,
		nilnul._obj.typ.child.calc_.unary_.IAlias<IAcyclic4all,IIrreflex> /// if any co is reflexive, then this is cyclic
	{
	}
}

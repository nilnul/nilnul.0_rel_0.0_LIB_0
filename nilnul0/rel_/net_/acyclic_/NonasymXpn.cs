﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.acyclic_
{

	/// <summary>
	/// if there is some sym cos, then there is a cycle, resulting a contradiction.
	/// </summary>
	/// <remarks>
	/// alias:
	///		reflexsome Xpn
	/// </remarks>
	[Serializable]
	public class Sym4someException : Exception
		,
		IReflex
	{
		public Sym4someException() { }
		public Sym4someException(string message) : base(message) { }
		public Sym4someException(string message, Exception inner) : base(message, inner) { }
		protected Sym4someException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// </summary>
	/// <remarks>
	/// alias:
	///		transducible
	///		shortcutsome
	/// </remarks>
	public interface IShortcutsome:
		INet
		,
		_obj.typ.child.calc_.unary_.IComplement<INet, IShortcutless>	//if it's reduced, it's not reducible. if it's reducible, then it's not reduced. We proved that the pair is disjoint; but we havenot proven it's complement. If it's not reduced, then it's reducible; it it's not reducible, then it's reduced.
	{
	}
}

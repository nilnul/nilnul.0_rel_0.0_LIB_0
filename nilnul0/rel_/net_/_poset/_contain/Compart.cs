﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.poset
{
	public interface ICompart
	{
	}
	public interface CompartI<T> {
		/// <summary>
		/// 
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns>
		/// false, when x is inferior or eq
		/// true, when x is superior or eq
		/// 
		/// null, when incomparable.
		/// </returns>
		bool? compare(T x, T y);
	}

	public interface CompartByIntI<T> {
		/// <summary>
		/// 
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns>
		/// -1, when x is inferior or eq
		/// 1, when x is superior or eq
		/// 0, when incomparable.
		/// </returns>
		int compare(T x, T y);
	}

}

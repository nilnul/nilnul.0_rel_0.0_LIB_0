﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._poset
{
	public interface IHas:_net.IHas
	{
	}
	public interface HasI<T>
	:
		_net.HasI<T>
		,
		IHas
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns>
		///  true if x le y
		///  false if x >y.
		///
		///  so: 
		///  contain(x,y) and contain(y,x), ==> x==y
		///  contain(x,y) and !contain(y,x) ==> x lt y
		///  !contain(x,y) and contain(y,x) => x gt y.
		///  !contain(x,y) and !contain(y,x) => in comparable.
		/// </returns>
		//bool contain(T x, T y);
	}


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_
{
	/// <summary>
	/// eg:
	///		a->b, b->c, a->c is cruxed, acyclic, but not leaned, as a->c can be removed to maintain the connectedness.
	///	different from <see cref="nameof(IAcoil)"/>.
	///		a->b,b->c, a->d, d->c is shortcutless, but not acoil
	/// </summary>
	/// alias:
	///		reduced
	public interface IShortcutless
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.unimin_
{
	/// <summary>
	/// each node has one or nil parent.(as this is <see cref="dac_.IUniminimal"/>, so only the root has nil parent; any node else has one parent.)
	/// 
	/// </summary>
	/// <remarks>
	/// </remarks>
	/// This is a tree
	///
	//[Obsolete(nameof(dac_.acoil_.IUniminimal) + " is a more prime subtype.")]
	public interface INonmerge:
		IUniminimal
		
		
		
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.unimin_.acoil_
{
	/// <summary>
	/// if we pour some liquid to the root and let it tricle down the <see cref="nameof(IDac)"/>, no node can have plural inlets or else this would be coily.
	/// </summary>
	/// <remarks>
	/// </remarks>

	[Serializable]
	//[Obsolete()]
	public class NontreeException :
		Exception
		//dac_.acoil_.unimin_.NontreeException
		
		,
		dac_.unimin_.IAcoilar

	//,
	//dac_.acoil_.IUniminimal
	{
		public NontreeException() { }
		public NontreeException(string message) : base(message) { }
		public NontreeException(string message, Exception inner) : base(message, inner) { }
		protected NontreeException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

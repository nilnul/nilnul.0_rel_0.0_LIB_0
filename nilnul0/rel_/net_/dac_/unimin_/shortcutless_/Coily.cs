﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.unimin_.shortcutless_
{
	/// <summary>
	///	eg: a->b->c, a->d->c
	/// </summary>
	/// <remarks>
	/// alias:
	///		coiline
	/// </remarks>
	///
	//[Obsolete()]
	public interface ICoilsome
		:
		dac_.unimin_.IShortcutless
		,
		dac_.ICoilsome
		
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.unimin_.shortcutless_
{
	/// <summary>
	///	eg: a->b,a->c
	/// </summary>
	///
	[Obsolete(nameof(unimin_.IAcoilar) + " is more strict" )]
	public interface IAcoil
		:
		dac_.unimin_.IShortcutless
		,
		dac_.IAcoil
		//,
		//dac_.shortcutless_.unimin_.IAcoil
		//,
		//dac_.acoil_.IUniminimal
		
	{
	}
}

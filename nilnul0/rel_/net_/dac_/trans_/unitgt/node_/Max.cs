﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.trans_.unitgt.node_
{
	static public class _MaxX
	{
		static public T Max<T>(
			IEnumerator<T> nodes
			,
			System.Func<T,T,bool> rel
		) {

			nodes.MoveNext();
			var prevTgt = nodes.Current;
			while (nodes.MoveNext())
			{
				if (
					rel(prevTgt,nodes.Current)
				)
				{
					prevTgt = nodes.Current;
				}
			}
			return prevTgt;

		}
		static public T Max<T>(
			IEnumerable<T> nodes
			,
			System.Func<T,T,bool> rel
		) {

			return Max(
				nodes.GetEnumerator()
				,
				rel
			);

		}

	}
}

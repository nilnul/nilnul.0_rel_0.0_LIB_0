﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.trans_.unisrc.node_
{
	static public class _MinX
	{
		static public T Min<T>(
			IEnumerator<T> nodes
			,
			System.Func<T,T,bool> rel
		) {

			return unitgt.node_._MaxX.Max(
				nodes
				,
				(s,t)=> rel(t,s)
			);

		}
		static public T Min<T>(
			IEnumerable<T> nodes
			,
			System.Func<T,T,bool> rel
		) {

			return Min(
				nodes.GetEnumerator()
				,
				rel
			);

		}

	}
}

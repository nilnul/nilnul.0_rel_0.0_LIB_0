﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_
{
	/// <summary>
	/// the start/min/src is one and only one.
	/// eg:
	///		the type system of C# is unimin. we start with one base type:object
	/// </summary>
	/// <remarks>
	/// alias:
	///		unimin
	///		unistart
	///		unibegin
	///		uniroot
	///		
	/// </remarks>
	public interface IUniminimal:
		IPolar
		//,
		//net_.IDac
		
		
	{
	}
}

﻿using nilnul.rel_.net_.dac_.polar_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_
{
	/// <summary>
	/// </summary>
	/// <remarks>
	/// alias:
	///		fusiform
	///		shuttle
	///		lemon
	///		spindle
	///		纺锤
	///			As dag can mean a roll of sheep wool, fusiform means to make that coil of wool with bipolar
	///		梭
	///		bipolar
	///		unibound
	/// </remarks>
	public interface IFusiform:IUniminimal,IUnifinal
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_
{
	/// <summary>
	/// transitive
	/// </summary>
	/// <remarks>
	/// aka:
	///		strict partial order
	///		not taxonomy as taxonomy is reflexive.
	/// </remarks>
	/// dwelt,
	/// irreflexive,transitive,
	/// connected
	public interface IDominant
		:net_.IDac
		,
		net_.IDominant
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.unimax_
{
	/// <summary>
	/// each node has one or nil child.(as this is <see cref="dac_.IUnimax"/>, so only the max has nil parent; any node else has one child.)
	/// 
	/// a tree
	/// </summary>
	/// <remarks>
	/// </remarks>
	///
	//[Obsolete(nameof(dac_.acoil_.IUniminimal) + " is a more prime subtype.")]
	public interface IAcoilar:
		IUnimax
		,dac_.IAcoil
		//,
		//dac_.IShortcutless
		
		//,
		//dac_.acoil_.IUniminimal
	{
	}
}

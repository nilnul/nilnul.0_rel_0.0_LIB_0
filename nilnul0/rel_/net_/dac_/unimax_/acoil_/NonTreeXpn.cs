﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.unimax_.acoil_
{
	/// <summary>
	/// </summary>
	/// <remarks>
	/// </remarks>

	[Serializable]
	//[Obsolete()]
	public class NontreeException :
		Exception
		//dac_.acoil_.unimin_.NontreeException
		
		,
		dac_.unimax_.IAcoilar

	//,
	//dac_.acoil_.IUniminimal
	{
		public NontreeException() { }
		public NontreeException(string message) : base(message) { }
		public NontreeException(string message, Exception inner) : base(message, inner) { }
		protected NontreeException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

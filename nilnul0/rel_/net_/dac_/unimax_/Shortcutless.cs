﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.unimax_
{
	/// <summary>
	/// </summary>
	///
	//[Obsolete(nameof(dac_.shortcutless_.IUniminimal) + " is more taxonomy.")]
	public interface IShortcutless
		:
		dac_.IUnimax
		,
		dac_.IShortcutless
		//,
		//dac_.shortcutless_.IUniminimal
	{
	}
}

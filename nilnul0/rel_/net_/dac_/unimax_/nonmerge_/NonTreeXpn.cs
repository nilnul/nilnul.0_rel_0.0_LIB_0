﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.unimax_.nonmerge_
{
	/// <summary>
	/// </summary>
	/// <remarks>
	/// </remarks>

	[Serializable]
	//[Obsolete()]
	public class NontreeException :
		Exception
		
		,
		dac_.unimax_.INonmerge
		,
		nilnul._obj.typ.child.calc_.unary_.comple_.IReflec<INonmerge,NontreeException>

	{
		public NontreeException() { }
		public NontreeException(string message) : base(message) { }
		public NontreeException(string message, Exception inner) : base(message, inner) { }
		protected NontreeException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

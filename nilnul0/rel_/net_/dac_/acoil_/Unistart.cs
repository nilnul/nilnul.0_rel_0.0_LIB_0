﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.acoil_
{
	/// <summary>
	/// </summary>
	/// <remarks>
	/// </remarks>
	///
	[Obsolete(
		nameof(dac_.unimin_.IAcoilar)
		+ " is more prime in subtyping."
		
	)]
	public interface IUniminimal:
		dac_.IAcoil
		,
		dac_.IUniminimal
		,
		dac_.unimin_.IAcoilar
	{
	}
}

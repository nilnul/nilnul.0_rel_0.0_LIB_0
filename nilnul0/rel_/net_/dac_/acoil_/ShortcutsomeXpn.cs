﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.acoil_
{
	/// <summary>
	/// if it's acoil, you cannot have any shortcut.
	/// </summary>
	[Serializable]
	public class ShortcutsomeException :
		Exception
		,
		dac_.IAcoil
	{
		public ShortcutsomeException() { }
		public ShortcutsomeException(string message) : base(message) { }
		public ShortcutsomeException(string message, Exception inner) : base(message, inner) { }
		protected ShortcutsomeException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

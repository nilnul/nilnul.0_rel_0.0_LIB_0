﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.polar_
{
	/// <summary>
	/// the end/stop/drain/max/tgt is one and only one.
	/// </summary>
	/// <remarks>
	/// alias:
	///		unifinal
	///		unifinish
	///		uniend
	///		unistop
	///		
	/// </remarks>
	public interface IUnifinal:IPolar
	{
	}
}

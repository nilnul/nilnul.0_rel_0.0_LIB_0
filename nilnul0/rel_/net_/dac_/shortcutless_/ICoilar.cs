﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.shortcutless_
{
	/// <summary>
	///		a->b,b->c, a->d, d->c 
	/// </summary>
	public interface ICoilar
		:dac_.IShortcutless
		,
		dac_.ICoilsome
	{
	}
}

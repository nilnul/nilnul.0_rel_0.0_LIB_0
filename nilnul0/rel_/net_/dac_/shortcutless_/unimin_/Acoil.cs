﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.shortcutless_.unimin_
{
	/// <summary>
	///	eg: a->b,a->c
	/// </summary>
	///

	[Obsolete()]
	public interface IAcoil
		:
	dac_.shortcutless_.IUniminimal
		,
		dac_.acoil_.IUniminimal

	{
	}
}

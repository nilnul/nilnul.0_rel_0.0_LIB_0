﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.shortcutless_.unimin_
{
	/// <summary>
	///		a->b->c, a->d->c 
	/// </summary>
	///
	[Obsolete()]
	public interface ICoilar
		:
		dac_.shortcutless_.IUniminimal
		,
		dac_.ICoilsome
	{
	}
}

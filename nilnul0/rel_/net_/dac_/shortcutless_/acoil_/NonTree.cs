﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.shortcutless_.acoil_
{
	/// <summary>
	/// eg:
	///		a->b->c,d->b->e
	///			there is more than one root.
	/// </summary>
	/// <remarks>
	/// </remarks>
	///
	[Obsolete()]
	public interface INontree:dac_.shortcutless_.IAcoil
		,
		dac_.acoil_.INontree
	{
	}
}

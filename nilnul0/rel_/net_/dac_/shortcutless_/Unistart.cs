﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.shortcutless_
{
	/// <summary>
	/// </summary>
	/// <remarks>
	/// </remarks>
	///
	[Obsolete(nameof(dac_.unimin_.IShortcutless) + " is more prime in subtype " )]
	public interface IUniminimal:
		dac_.IShortcutless,
		dac_.IUniminimal
		,
		dac_.unimin_.IShortcutless
	{
	}
}

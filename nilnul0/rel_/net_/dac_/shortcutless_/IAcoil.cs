﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.shortcutless_
{
	/// <summary>
	///	eg: a->b,a->c
	/// </summary>
	///
	[Obsolete( nameof(dac_.IAcoil) + "is more strict" )]
	public interface IAcoil
		:
		IShortcutless
		,
		dac_.IAcoil
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_
{
	/// <summary>
	/// the stop/drain/tgt is one and only one.
	/// This useful in version control, eg:
	///		if the branches are
	///			nilnul1-a1,
	///			nilnul1-b1
	///		we can then name one as:
	///			nilnul2
	///	Note here,the prefix "nilnul" is important to enable us to hava a new unimax. If without such prefix, we cannot find a unimax for
	///		a1
	///		b1.
	///		
	/// </summary>
	/// <remarks>
	///		
	/// </remarks>
	public interface IUnimax:
		IPolar
		//,
		//net_.IDac
		
		
	{
	}
}

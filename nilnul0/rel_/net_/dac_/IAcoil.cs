﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_
{
	/// <summary>
	/// eg:
	///		a->b, b->c, a->c is cruxed, acyclic, but not nocoil, as a->c can be removed to maintain the connectedness.
	/// </summary>
	public interface IAcoil:
		IDac,
		nilnul._obj.typ.child.calc_.unary_.IComplement<IDac,ICoilsome >

	{
	}
}

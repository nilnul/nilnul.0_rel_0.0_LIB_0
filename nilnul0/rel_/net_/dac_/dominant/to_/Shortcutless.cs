﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dac_.domnant.op_.to_
{
	/// <summary>
	/// remove reflexiveness
	/// then make it shortcutless.
	/// the result is :<see cref="nameof(net_.dac_.IShortcutless)"/>
	/// </summary>
	class Lean:ITo<
		dac_.IShortcutless
	>
	{
	}
}

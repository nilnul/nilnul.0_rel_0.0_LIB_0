﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// alias:
	///		flex
	///		reflex
	///		irreflex4none
	/// </remarks>
	public interface IReflex :

		INet
		,
		_obj.typ.child.calc_.unary_.IComplement<INet, INonreflex>
	#region eg: {} is reflex and irreflex
#if false
		,
		_obj.typ.child.calc_.unary_.IComplement<INet, INonirreflex>

#endif

	#endregion

	{
	}
}

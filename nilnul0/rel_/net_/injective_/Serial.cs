﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.injective_
{
	public interface ISerial
		:IInjective
		,
		rel_.ISerial
	{
	}
}

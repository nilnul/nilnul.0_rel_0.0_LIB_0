﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._tree.unconstained.be_
{
	public class Constrained_ofRef<T>
		:
		nilnul.obj.Box1<
			nilnul.obj.str.be_.Distinct3<T>
		>
		,
		nilnul.obj.BeOfRefI<
			Unconstrained<T>
		>
	{
		public Constrained_ofRef(IEqualityComparer<T> val)
			:
		base(
			new nilnul.obj.str.be_.Distinct3<T>(val)
		)
		{
		}

		public Constrained_ofRef():this(EqualityComparer<T>.Default)
		{

		}

		public bool be(ref Unconstrained<T> obj)
		{
			return boxed.be(obj);

		}

		static public Constrained_ofRef<T> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<Constrained_ofRef<T>>.Instance;
			}
		}

	}
}

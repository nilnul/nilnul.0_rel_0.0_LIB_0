﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._tree.unconstained.be_.constrained
{
	public class Vow<T>
		:
		nilnul.obj.be.Vow4<Unconstrained<T>>
	{
		public Vow(IEqualityComparer<T> be) : base(
			new Constrained<T>(be)
		)
		{
		}

	}
}

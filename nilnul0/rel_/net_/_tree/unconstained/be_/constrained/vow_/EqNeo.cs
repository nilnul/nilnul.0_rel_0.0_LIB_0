﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._tree.unconstained.be_.constrained.vow_
{
	public class EqNeo<T, TEq>
		:
		nilnul.obj.be.vow_.BeDefaulted4<
		 Unconstrained<	T>
			,
be_.constrained_.EqNeo<T,TEq>
			>

		//Vow<T>
		where TEq : IEqualityComparer<T>, new()
	{
		//public EqNeo() : base(
		//	be_.constrained_.EqNeo<T,TEq>.Singleton
		//)
		//{
		//}


		static public EqNeo<T, TEq> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<EqNeo<T, TEq>>.Instance;
			}
		}

	}
}

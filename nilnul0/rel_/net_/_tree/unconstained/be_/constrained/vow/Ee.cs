﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._tree.unconstained.be_.constrained.vow
{
	public class Ee<T>
		:
		nilnul.obj.vow.Ee1<
			Unconstrained<T>
		>
	{
		public Ee( Unconstrained<T> val, IEqualityComparer<T> vow) : base( val, new constrained.Vow<T>(vow) )
		{
		}
	}
}

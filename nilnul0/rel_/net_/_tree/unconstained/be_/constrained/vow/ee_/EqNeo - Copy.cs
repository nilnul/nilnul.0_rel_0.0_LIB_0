﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._tree.unconstained.be_.constrained.vow.ee_
{
	public class EqNeo_byRef<T, TEq>
		:
		nilnul.obj.vow.ee_.VowNeo_byRef<
			Unconstrained< T>
			,
			constrained.vow_.EqNeo_ofRef<T, TEq>
		>
		where TEq : IEqualityComparer<T>, new()
	{
		public EqNeo_byRef(ref Unconstrained<T> val) : base(ref val)
		{
		}
	}
}

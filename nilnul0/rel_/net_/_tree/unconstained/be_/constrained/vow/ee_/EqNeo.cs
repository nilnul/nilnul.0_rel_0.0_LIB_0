﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._tree.unconstained.be_.constrained.vow.ee_
{
	public class EqNeo<T, TEq>
		:
		nilnul.obj.vow.ee_.Defaultable<
			 Unconstrained<T>
			,
			 constrained.vow_.EqNeo<T,TEq>
			
		>
		where TEq : IEqualityComparer<T>, new()
	{
		public EqNeo( Unconstrained<T> val) : base( val)
		{
		}
	}
}

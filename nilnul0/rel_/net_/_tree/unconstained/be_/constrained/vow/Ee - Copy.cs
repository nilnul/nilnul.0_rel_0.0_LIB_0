﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._tree.unconstained.be_.constrained.vow
{
	public class Ee_byRef<T>
		:
		nilnul.obj.vow.Ee_byRef<
			Unconstrained<T>
		>
	{
		public Ee_byRef(
			ref Unconstrained<T> val
			, IEqualityComparer<T> vow) : base(ref val,
				new constrained.Vow_ofRef<T>(vow)
			)
		{
		}
	}
}

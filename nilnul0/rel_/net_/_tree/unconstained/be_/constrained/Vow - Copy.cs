﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._tree.unconstained.be_.constrained
{
	public class Vow_ofRef<T>
		:
		nilnul.obj.be.VowOfRef<Unconstrained<T>>
	{
		public Vow_ofRef( IEqualityComparer<T> val) : base( new Constrained_ofRef<T>(val))
		{
		}

	}
}

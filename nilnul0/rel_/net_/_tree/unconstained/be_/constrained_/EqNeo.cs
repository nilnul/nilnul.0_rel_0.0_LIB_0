﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._tree.unconstained.be_.constrained_
{
	public class EqNeo<T, TEq> :
		Constrained<T>
		where TEq : IEqualityComparer<T>, new()

	{
		public EqNeo() : base(

				 nilnul._obj.typ_.nilable_.unprimable_.singleton_.ByLazy<TEq>.Instance

		)
		{
		}


		static public EqNeo<T, TEq> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<EqNeo<T, TEq>>.Instance;
			}
		}

	}
}

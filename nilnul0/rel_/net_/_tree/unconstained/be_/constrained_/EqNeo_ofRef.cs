﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._tree.unconstained.be_.constrained_
{
	public class EqNeo_ofRef<T, TEq> :
		Constrained_ofRef<T>
		where TEq : IEqualityComparer<T>, new()

	{
		public EqNeo_ofRef() : base(

				 nilnul._obj.typ_.nilable_.unprimable_.singleton_.ByLazy<TEq>.Instance

		)
		{
		}


		static public EqNeo_ofRef<T, TEq> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<EqNeo_ofRef<T, TEq>>.Instance;
			}
		}

	}
}

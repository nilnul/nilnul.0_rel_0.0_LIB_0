﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._tree.unconstained.be_
{
	public class Constrained<T>
		:
		nilnul.obj.Box1<
			nilnul.obj.str.be_.Distinct3<T>
		>
		,
		nilnul.obj.BeI1<
			Unconstrained<T>
		>
	{
		public Constrained(IEqualityComparer<T> val)
			:
		base(
			new nilnul.obj.str.be_.Distinct3<T>(val)
		)
		{
		}

		public Constrained():this(EqualityComparer<T>.Default)
		{

		}

		public bool be(Unconstrained<T> obj)
		{
			return boxed.be(obj);

		}

		static public Constrained<T> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<Constrained<T>>.Instance;
			}
		}

	}
}

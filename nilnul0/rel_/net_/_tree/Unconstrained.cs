﻿using nilnul.rel_.net_.tree.set.be_;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._tree
{

	/// <summary>
	/// there might be merges, including even loops. Constraints shall be applied later to make this a true tree: no merge, thus no loop.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class Unconstrained<T>
		:
		IEnumerable<T>
	{
		internal List<Unconstrained<T>> _children;
		public List<Unconstrained< T>> children
		{
			get
			{
				return _children;
			}
		}

		internal T _root;
		public T root
		{
			get
			{
				return _root;
			}
		}

		public Unconstrained(T root, List< Unconstrained<T>> children)
		{

			_root = root;
			_children = children;
		}
		 public Unconstrained(T root):this(
			root, new List<Unconstrained<T>>() 
		)
		{

		}

		/// <summary>
		/// use this with care, as the added might be joint with other children
		/// </summary>
		/// <param name="item"></param>
		public void addAsChild(Unconstrained<T> item)
		{
			_children.Add(
				item
			);
		}

		/// <summary>
		/// use this with care, as the added might be joint with other children
		/// </summary>
		public void addAsChild(T item)
		{
			addAsChild(
				new Unconstrained<T>(item)
			);
		}


		public IEnumerator<T> GetEnumerator()
		{
			yield return _root;
			foreach (var item in _children)
			{
				foreach (var item1 in item)
				{
					yield return item1;
				}
			}
			//throw new NotImplementedException();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
			//throw new NotImplementedException();
		}
	}
}
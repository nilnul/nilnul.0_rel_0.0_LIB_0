﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._tree
{
	public class KevVals<T>
		: Dictionary<
			T, T
		>
	{
		public KevVals()
		{
		}

		public KevVals(int capacity) : base(capacity)
		{
		}

		public KevVals(IEqualityComparer<T> comparer) : base(comparer)
		{
		}

		public KevVals(IDictionary<T, T> dictionary) : base(dictionary)
		{
		}

		public KevVals(int capacity, IEqualityComparer<T> comparer) : base(capacity, comparer)
		{
		}

		public KevVals(IDictionary<T, T> dictionary, IEqualityComparer<T> comparer) : base(dictionary, comparer)
		{
		}

		protected KevVals(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}

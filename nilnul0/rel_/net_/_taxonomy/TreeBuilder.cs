﻿#nullable enable
using nilnul.rel_.net_._tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._taxonomy
{
	 public class TreeBuilder<T>
		:
		nilnul.obj.Box1<_tree.Unconstrained<T>>
	{
		Func<T, T, bool> _taxonomySup;
		public Func<T, T, bool> taxonomySup
		{
			get
			{
				return _taxonomySup;
			}
		}

		public TreeBuilder(Unconstrained<T> val, Func<T, T, bool> taxonomySup) : base(val)
		{
			_taxonomySup = taxonomySup;
		}
		public TreeBuilder(T el, Func<T, T, bool> taxonomySup):this(new Unconstrained<T>(el),taxonomySup)
		{
		}

		 public _tree.Unconstrained<T>? parent(
	
	T obj2categorize
)
		{

			return _TreeX.ImmediateParent(
				this.boxed
				,
				this._taxonomySup
				,
				obj2categorize
			);

		}


		public _tree.Unconstrained<T>? insertUnder(
			T obj2categorize
		)
		{
			

		}


	}
}

﻿#nullable enable
using nilnul.obj.str;
using nilnul.rel_.net_._tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._taxonomy
{
	static public class _TreeX
	{

		static public _tree.Unconstrained<T>? ImmediateParent<T>(
			 _tree.Unconstrained<T> tree
			,
			Func<T, T, bool> _taxonomySup
			,
			T obj2categorize
		)
		{
			///depth first

			foreach (var item in tree._children)
			{
				var c = ImmediateParent(
					 item
					,
					_taxonomySup
					,
					obj2categorize
				);
				if (c is not null)
				{
					return c;
				}
			}

			return (
				_taxonomySup(
					tree._root
					,
					obj2categorize
				)
			) ? tree:null;

		}

		static public _tree.Unconstrained<T>? InsertUnderRoot<T>(
			 _tree.Unconstrained<T> tree
			,
			Func<T, T, bool> _taxonomySup
			,
			T obj2categorize
		)
		{
			var parent = ImmediateParent(tree, _taxonomySup, obj2categorize);
			if (parent is null)
			{
				return null;
			}
			_Insert_rootAssumeParent(parent, _taxonomySup, obj2categorize);
			return parent;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="tree"></param>
		/// <param name="_taxonomySup"></param>
		/// <param name="tree2categorize">
		/// assumed direct child of the root. Hence, it cannot be in any other child.
		/// </param>
		public static void _Insert_rootAssumeParent<T>(
			Unconstrained<T> tree
			,
			Func<T, T, bool> _taxonomySup
			,
			Unconstrained<T> tree2categorize
		)
		{
			var currentChildrenRefs = tree._children.ToArray();

			foreach (var child in currentChildrenRefs)
			{
			///if any node of current child is covered by the incoming tree, then the root of the current child is covered by the incoming tree
				var parent = _TreeX.ImmediateParent(tree2categorize,_taxonomySup,child._root);
				if (parent is not null)
				{
					tree._children.Remove(child);

					_Insert_rootAssumeParent(
						tree2categorize,_taxonomySup, child
					);
				}
			}
			tree._children.Add(tree2categorize);
		}

		static public void _Insert_rootAssumeParent<T>(
			 _tree.Unconstrained<T> tree
			,
			Func<T, T, bool> _taxonomySup
			,
			T obj2categorize
		)
		{

			var children = tree.children.Where(
				c => _taxonomySup(
					obj2categorize
					,
					c._root
				)
			).ToList();

			children.Each(
				c => tree.children.Remove(

					c
				)
			);

			var asTree =
				new Unconstrained<T>(
					obj2categorize, children
			);
			children.Add(asTree);
		}
	}
}
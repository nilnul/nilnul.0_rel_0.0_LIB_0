﻿using nilnul.obj.str;
using nilnul.rel_.net_._tree;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._taxonomy
{
	[DebuggerDisplay("{" + nameof(GetDebuggerDisplay) + "(),nq}")]
	public class ForestBuilder<T>
		:
		List<Unconstrained<T>>
	{
		Func<T, T, bool> _taxonomySup;
		public Func<T, T, bool> taxonomySup
		{
			get
			{
				return _taxonomySup;
			}
		}

		/// <summary>
		/// merge all child tree,
		/// and then put under a parent node in a tree, or add as an independent tree.
		/// </summary>
		/// <param name="item"></param>
		public void add(T item)
		{
			var children = this.Where(
				c => _taxonomySup(
					item
					,
					c._root
				)
			).ToList();

			children.Each(
				c => this.Remove(

					c
				)
			);
			var asTree =
				new Unconstrained<T>(
					item, children
			);

			foreach (var tree in this)
			{
				/// this might return a subtree.
				var parent = _TreeX.ImmediateParent(tree, _taxonomySup, item);
				if (parent is not null)
				{
					_TreeX._Insert_rootAssumeParent<T>(
						parent, _taxonomySup,asTree
					);
					return; // the item is added to an old tree. 
				}
			}
			//as no old tree can contain this, this is a new tree.
			this.Add(
				asTree
			);

		}
		public ForestBuilder(Func<T, T, bool> _taxonomySup)
		{
			this._taxonomySup = _taxonomySup;
		}


		//		public ForestBuilder(IEnumerable<Unconstrained<T>> collection
		//			,
		//Func<T, T, bool> _taxonomySup
		//			) : base(collection)
		//		{
		//			this._taxonomySup = _taxonomySup;
		//		}



		private string GetDebuggerDisplay()
		{
			return ToString();
		}
	}
}

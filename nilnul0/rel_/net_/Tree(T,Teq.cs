﻿using nilnul.rel_.net_.tree.set.be_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{

	/// <summary>
	/// the root shouldn't be in any children
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TEq"></typeparam>
	public class Tree<T, TEq> : TreeI<T, TEq>
		where TEq : IEqualityComparer<T>, new()

	{
		private Disjoint<T, TEq>.En _children;
		public Disjoint<T, TEq>.En children
		{
			get
			{
				return _children;
				throw new NotImplementedException();
			}
		}

		private T _root;
		public T root
		{
			get
			{
				return _root;
				throw new NotImplementedException();
			}
		}

		 public Tree(T root):this(
			root, new Disjoint<T, TEq>.En() 
		)
		{

		}

		public Tree(T root, Disjoint<T,TEq>.En children)
		{
			nilnul.Assert1.False(
				children.contains(root)	
			);

			_root = root;
			_children = children;
		}


	}


}

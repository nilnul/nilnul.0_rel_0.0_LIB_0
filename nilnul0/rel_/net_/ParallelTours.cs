﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// eg:
	///		single node
	///		plural solitude nodes.
	///		<see cref="net.tour_.nonempty_"/>
	///		or plural <see cref="net.tour_.nonempty_"/> that are disjoint
	/// </summary>
	/// <remarks>
	/// vs:
	///		<see cref="net_.poset_.ILinear"/>
	///			which is transitive;
	///				here this might be not transitive.
	/// </remarks>
	public interface IParallelTours:
		INet
	{

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.sym_.reflex_
{
	public interface ICompatible
		:
		IReflex
		,
		_obj.typ.child.calc_.unary_.IAlias<IReflex, ICompatible>
	{
	}
}

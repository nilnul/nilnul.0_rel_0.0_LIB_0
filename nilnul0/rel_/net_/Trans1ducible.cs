﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// eg:
	///		{a->b,b->c, a->c}
	/// </summary>
	public interface ITrans1ducible : rel_.net_.ITrans, rel_.net_.ITransReducible
	{
	}
}
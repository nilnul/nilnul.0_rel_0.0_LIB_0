﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.antisym_.irreflex_
{
	/// <summary>
	/// </summary>

	public interface IAsym
		:
		//nilnul.obj.IAlias<Irreflex>
		//,
		IIrreflex
		,
		_obj.typ.child.calc_.unary_.IAlias<antisym_.IIrreflex, IAsym>
		,
		_obj.typ.child.calc_.unary_.comple_.IReflec< antisym_.IIrreflex,NonasymException>
	{}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.antisym_.irreflex_
{
	/// <summary>
	/// if some co is symmetric, then it is either reflexive, or irreflexively symmetric;
	///		if it is reflexive, contradict to <see cref="nameof(irreflex_)"/>
	///		if it's irreflexive, but symmetric, conradict to <see cref="nameof(antisym_)"/>
	/// 
	/// </summary>

	[Serializable]
	public class NonasymException : Exception
		,
		antisym_.IIrreflex
	{
		public NonasymException() { }
		public NonasymException(string message) : base(message) { }
		public NonasymException(string message, Exception inner) : base(message, inner) { }
		protected NonasymException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

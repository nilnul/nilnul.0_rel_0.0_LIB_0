﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.ord_
{
	/// <summary>
	/// A is dependent on B; we say 
	///		A is premise/antedent/previous
	///		and B is  consequence/descendent/posterity
	/// </summary>
	public interface DepI
	{
	}
}

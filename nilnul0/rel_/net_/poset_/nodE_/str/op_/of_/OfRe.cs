﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.poset_.nodE_.str.op_.of_
{
	/// <summary>
	/// given a set of T instance, the <see cref="nameof(rel._has.re_.net_.IPoset)"/> determines the steps among them
	/// </summary>
	public class OfRe<T>
		: poset.op_.of_.OfRe<
			IEnumerable<T>
		>
		where T: class
	{
		//private nilnul.obj.SetI2<IEnumerable< T > > _field;

		//public nilnul.obj.SetI2<IEnumerable< T> > field
		//{
		//	get { return _field; }
		//	set { _field = value; }
		//}

		//public IEnumerable<(IEnumerable< T>, IEnumerable< T>)> couples => nilnul.obj.vec.str.op_.binary_._CartesianX.Seq(_field,_field);

		public OfRe(
			rel._has.re_.net_.PosetI<T> val,
			nilnul.obj.SetI2<IEnumerable< T >> field0
		) : base( new rel._has.re_.net_.poset_.elNulable.contrast.onStr_.NulMin<T>(val),field0)
			
		{
			//_field = field0;
		}






	}
}

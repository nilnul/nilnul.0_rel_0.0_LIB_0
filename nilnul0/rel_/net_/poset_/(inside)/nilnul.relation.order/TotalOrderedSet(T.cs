﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.relation.order
{
	public partial class TotalOrderedSet<T>
	{
		private TotalOrderA<T> _order;

		public TotalOrderA<T>	order
		{
			get { return _order; }
			set { _order = value; }
		}
		

		public TotalOrderedSet(TotalOrderA<T> order)
		{
			this.order = order;
		}
					
	}
}

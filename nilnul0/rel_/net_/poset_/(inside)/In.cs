﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel
{
	public abstract class InsideA<T> : InsideI<T>
	{
		public abstract bool inside(T x, T y);

		public bool inc(T x, T y) {
			return inside(y, x);

		}

		public bool eq(T x, T y) {
			return inside(x, y) && inc(x, y);

		}

		public bool ne(T x, T y) {
			return !eq(x, y);

		}

		public bool subProper(T x, T y) {
			return inside(x, y) && ne(x, y);

		}

		public bool supProper(T x, T y) {
			return inc(x, y) && ne(x, y);

		}



	}

	public interface InsideI<T>:InsideI<T,T>
	{
	}


	public interface InsideI<T,T1>
	{
		bool inside(T x, T1 y);
	}
}

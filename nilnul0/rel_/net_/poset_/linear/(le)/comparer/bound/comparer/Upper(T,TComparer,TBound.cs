﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.order.comparer.bound.comparer
{

	public partial class Upper<T, TComparer,TBound>
		: 
		IComparer<TBound>
		
		
		where TComparer : IComparer<T>
		where TBound:Bound<T>
	{
		private TComparer  _elementComparer;
		public TComparer  elementComparer
		{
			get { return _elementComparer; }
			set { _elementComparer = value; }
		}

		private Lower<T,TBound> _lowerBoundComparer;

		public Lower<T,TBound> lowerBoundComparer
		{
			get { return _lowerBoundComparer; }
			set { _lowerBoundComparer = value; }
		}


		public TBound min(TBound a, TBound b)
		{
			return Compare(a, b) < 0 ? a : b;
		}





		public Upper(TComparer elementComparer)
		{
			_elementComparer = elementComparer;

			_lowerBoundComparer = new Lower<T,  TBound>(
				new order.comparer.op.Inverse.CallAsComparer<TComparer,T>(elementComparer)
			);
		}

		public int Compare(TBound x, TBound y)
		{
			return -lowerBoundComparer.Compare(x, y);

		}
	}

	public partial class Upper<T, TBound>
		: 

		Upper<T,IComparer<T>,TBound>

		where TBound:Bound<T>




	{ 

		public Upper(IComparer<T> elementComparer):base(elementComparer)
		{
			
		}

		
	}

	public partial class Upper_elementComparerDefault<T, TComparer,TBound>
		: 
		Upper<T,TComparer,TBound>
		,

		IComparer<TBound>
		
		
		where TComparer : IComparer<T>, new()
		where TBound:Bound<T>
	{

		static public Upper_elementComparerDefault<T, TComparer, TBound> Singleton = SingletonByDefault<Upper_elementComparerDefault<T, TComparer, TBound>>.Instance;
		static public TBound Min(TBound a, TBound b) {
			return Singleton.Compare(a, b) < 0 ? a : b;
		}
		static public TBound Max(TBound a, TBound b) {
			return Singleton.Compare(a, b) > 0 ? a : b;
		}

		public Upper_elementComparerDefault()
			:base(SingletonByDefaultNew<TComparer>.Instance)
		{
		}



	

	}
}

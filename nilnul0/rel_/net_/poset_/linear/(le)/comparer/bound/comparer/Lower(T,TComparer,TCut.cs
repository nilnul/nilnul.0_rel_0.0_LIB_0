﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.order.comparer.bound.comparer
{

	public partial class Lower<T, TComparer,TBound>
		: 
		IComparer<TBound>
		
		
		where TComparer : IComparer<T>
		where TBound:Bound<T>
	{
		private TComparer  _elementComparer;
		public TComparer  elementComparer
		{
			get { return _elementComparer; }
			set { _elementComparer = value; }
		}

		public Lower(TComparer elementComparer)
		{
			_elementComparer = elementComparer;
		}

		public TBound max(TBound x, TBound y) {
			return Compare(x, y) > 0 ? x : y;
		}

		public int Compare(TBound x, TBound y)
		{
			var c = _elementComparer.Compare(x.pinpoint, y.pinpoint);

			if (c == 0)
			{
				if (x.openFalseCloseTrue && !y.openFalseCloseTrue)
				{
					return -1;

				}
				if (!x.openFalseCloseTrue && y.openFalseCloseTrue)
				{
					return 1;
				}

			}
			return c;

		}
	}

	public partial class Lower<T, TBound>
		: 
		Lower<T,IComparer<T>,TBound>
		,

		IComparer<TBound>
		
		
		where TBound:Bound<T>
	{


		public Lower(IComparer<T> comparer)
			:base(comparer)
		{
		}

	}

	public partial class Lower_elementComparerDefault<T, TComparer,TBound>
		: 
		Lower<T,TComparer,TBound>
		,

		IComparer<TBound>
		
		
		where TComparer : IComparer<T>, new()
		where TBound:Bound<T>
	{


		static public Lower_elementComparerDefault<T, TComparer, TBound> Singleton = SingletonByDefaultNew<Lower_elementComparerDefault<T, TComparer, TBound>>.Instance;

		static public TBound Max(TBound a, TBound b) {

			return Singleton.Compare(a, b) > 0 ? a : b;

		}
		static public TBound Min(TBound a, TBound b) {

			return Singleton.Compare(a, b) < 0 ? a : b;

		}

		public Lower_elementComparerDefault()
			:base(SingletonByDefaultNew<TComparer>.Instance)
		{
		}



	

	}


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.order.comparer
{
	/// <summary>
	/// nonempty "bound.Duo".
	/// range is different in that the two ends are of type T, while interval must be defined in extended forms and may have ends of type ExtI such inf, NegInf.
	/// </summary>
	/// 
	[Obsolete("reference bound.duo.be.NonEmpty")]
	public class RangeI
	{


	}
}

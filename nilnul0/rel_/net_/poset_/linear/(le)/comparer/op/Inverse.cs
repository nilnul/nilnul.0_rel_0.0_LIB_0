﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.order.comparer.op
{
	public partial class Inverse
	{
		public partial class CallAsComparer<TComparer,T>
			:IComparer<T>
	where TComparer : IComparer<T>
		{

			private TComparer _comparer;

			public TComparer comparer
			{
				get { return _comparer; }
				set { _comparer = value; }
			}
			public CallAsComparer(TComparer comparer)
			{
				this._comparer = comparer;

			}


			public int Compare(T x, T y)
			{
				return -_comparer.Compare(x, y);
				throw new NotImplementedException();
			}


		}
		public partial class CallAsComparer_comparerDefault<TComparer,T>
			:CallAsComparer<TComparer,T>

	where TComparer : IComparer<T>,new()
		{

			
			public CallAsComparer_comparerDefault()
				:base(SingletonByDefaultNew<TComparer>.Instance)
			{

			}


		


		}

	}
}

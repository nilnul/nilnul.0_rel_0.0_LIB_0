﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.order.comparer.bounded.op
{
	public class Intersect1<T, TComparer, TBound>
		where TComparer : IComparer<T>
		where TBound : order.Bound<T>

	{
		private TComparer _comparer;

		public TComparer comparer
		{
			get { return _comparer; }
			set { _comparer = value; }
		}


		public Intersect1(TComparer comparer)
		{
			_comparer = comparer;

		}
		public order.bound.Pair_TBound<TBound> eval(order.bound.Pair_TBoundI<TBound>  a, order.bound.Pair_TBoundI<TBound> b)
		{

			return new order.bound.Pair_TBound<TBound>(
				new order.comparer.bound.comparer.Lower<T,TComparer,TBound>(comparer).max(a.lower, b.lower)
				,
				new order.comparer.bound.comparer.Upper<T,TComparer,TBound>(comparer).min(a.upper, b.upper)

			);

		}


		static public order.bound.Pair_TBound<TBound> Eval(order.bound.Pair_TBoundI<TBound>  a, order.bound.Pair_TBoundI<TBound> b, TComparer comparer)
		{

			return new order.bound.Pair_TBound<TBound>(
				new order.comparer.bound.comparer.Lower<T,TComparer,TBound>(comparer).max(a.lower, b.lower)
				,
				new order.comparer.bound.comparer.Upper<T,TComparer,TBound>(comparer).min(a.upper, b.upper)

			);

		}



	}

	public class Intersect_comparerDefault<T, TComparer, TBound>
		:Intersect1<T,TComparer,TBound>
		where TComparer : IComparer<T>, new()
		where TBound : order.Bound<T>

	{

		public Intersect_comparerDefault()
			:base(SingletonByDefaultNew<TComparer>.Instance)
		{

		}


		static public order.bound.Pair_TBound<TBound> Eval(order.bound.Pair_TBoundI<TBound>  a, order.bound.Pair_TBoundI<TBound> b)
		{

			return new order.bound.Pair_TBound<TBound>(
				new order.comparer.bound.comparer.Lower<T,TComparer,TBound>(SingletonByDefaultNew<TComparer>.Instance).max(a.lower, b.lower)
				,
				new order.comparer.bound.comparer.Upper<T,TComparer,TBound>(SingletonByDefaultNew<TComparer>.Instance).min(a.upper, b.upper)

			);

		}


			static public order.bound.Pair_TBound<TBound> Eval(order.bound.Pair_TBound<TBound>  a, order.bound.Pair_TBound<TBound> b)
		{

			return new order.bound.Pair_TBound<TBound>(
				new order.comparer.bound.comparer.Lower<T,TComparer,TBound>(SingletonByDefaultNew<TComparer>.Instance).max(a.lower, b.lower)
				,
				new order.comparer.bound.comparer.Upper<T,TComparer,TBound>(SingletonByDefaultNew<TComparer>.Instance).min(a.upper, b.upper)

			);

		}
	



	}

	[Obsolete()]
	
	public class Intersect<T, TComparer, TBound>
		where TComparer : IComparer<T>, new()
		where TBound : order.Bound<T>

	{


		static public Bounded_ComparerSingle<T, TComparer, TBound> Eval(_bounded.Bounded_ComparerSingle<T, TComparer, TBound> a, _bounded.Bounded_ComparerSingle<T, TComparer, TBound> b)
		{

			return new Bounded_ComparerSingle<T, TComparer, TBound>(
				comparer.bound.comparer.Lower_elementComparerDefault<T,TComparer,TBound>.Max(a.lower, b.lower)
				,
				comparer.bound.comparer.Upper_elementComparerDefault<T,TComparer,TBound>.Min(a.upper, b.upper)

			);

		}



	}
}

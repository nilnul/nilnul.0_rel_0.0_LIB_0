﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.order.op
{
	public static class Min
	{
		static public T Eval<T>(IComparer<T> comparer, IEnumerable<T> elements) {

			var min = elements.First();

			for (int i = 1; i < elements.Count(); i++)
			{

				if (comparer.Compare( elements.ElementAt(i), min)<0)
				{
					min = elements.ElementAt(i);
				}
			}

			return min;


		}



	}
}

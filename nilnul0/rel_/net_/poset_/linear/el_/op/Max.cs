﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.order.op
{
	public static class Max
	{

		static public T Eval<T>(IComparer<T> comparer, IEnumerable<T> elements) {

			var max = elements.First();

			for (int i = 1; i < elements.Count(); i++)
			{

				if (comparer.Compare( elements.ElementAt(i), max)>0)
				{
					max = elements.ElementAt(i);
				}
			}

			return max;


		}
		static public T Eval_byComparerInverse<T>(IComparer<T> comparer, IEnumerable<T> elements) {

			return Min.Eval(
				nilnul.order.comparer.solo.op.Inverse.Call(comparer)
				, 
				elements
			);

		}



	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.order.op
{
	public static class MinIndexes
	{
		static public IEnumerable<int> Eval<T>(IComparer<T> comparer, IEnumerable<T> elements) {

			if (elements.Count()==0)
			{
				return new int[0];

			}
			var min = Min.Eval<T>(comparer,elements);

			List<int> r = new List<int>();

			for (int i = 0; i < elements.Count(); i++)
			{
				if (comparer.Compare( elements.ElementAt(i) , min)==0)
				{
					r.Add(i);
				}

			}
			return r;

		}
		static public IEnumerable<int> Eval(IEnumerable<int> elements) {
			return Eval(_comparer.IntComparer.Singleton, elements);
		}
		static public IEnumerable<int> Eval(IEnumerable<double> elements) {
			return Eval(_comparer.DoubleComparer.Singleton, elements);
		}
	}
}

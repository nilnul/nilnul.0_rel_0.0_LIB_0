﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.order.op
{
	public static class MaxIndexes
	{
		static public IEnumerable<int> Eval_byComparerInverse<T>(IComparer<T> comparer, IEnumerable<T> elements) {

			return MinIndexes.Eval<T>(
				nilnul.order.comparer.solo.op.Inverse.Call<T>(
					comparer
				)
				, 
				elements
			);

		}
		static public IEnumerable<int> Eval(IEnumerable<int> elements) {
			return Eval_byComparerInverse(_comparer.IntComparer.Singleton, elements);
		}
		static public IEnumerable<int> Eval(IEnumerable<double> elements) {
			return Eval_byComparerInverse(_comparer.DoubleComparer.Singleton, elements);
		}
	}
}

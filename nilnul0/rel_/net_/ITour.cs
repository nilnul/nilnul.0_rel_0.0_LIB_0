﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	///  empty, single node, or a chain.
	/// </summary>
	/// <remarks>
	/// vs:
	///		<see cref="net_.poset_.ILinear"/>
	///			which is transitive;
	///				here this might be not transitive.
	/// </remarks>
	public interface ITour:
		INet
	{

	}
}

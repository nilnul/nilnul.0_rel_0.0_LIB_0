﻿using nilnul.rel_.net_.tree.set.be_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// a set (children) and an object which is not in set, is called a tree. the object is called the root. the set can be empty. the object and the children are called the nodes of the tree.
	/// two tree are disjoint if their elements/nodes  are disjoint. that is, 
	/// a collection of trees are disjoint if any pair are disjoint.
	/// 
	/// a disjoint collection of trees and an object which is not in any of trees constitue a new tree.
	/// 
	/// 
	/// 
	/// 
	/// 
	/// </summary>
	/// 


	public interface TreeI<TRoot>:_tree_.RootI<TRoot>,_tree_.BlankI

	{


	}

	public interface TreeI_children<T,TChildren> : TreeI<T> , _tree_.Children<TChildren>

	{

	}


	public interface TreeI<T, TEq>
		:TreeI<T>,
		
		_tree_.Children<

			tree.set.be_.Disjoint<T,TEq>.En
		>
		where	TEq:IEqualityComparer<T>,new()
	{

	}


}

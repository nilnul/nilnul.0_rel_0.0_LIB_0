﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// <see cref="nameof(net.be_.Shortcutless)"/>
	///transitivity reducted.
	/// for any path, there is no other simple path to connect the two ends.
	/// 
	/// for any (a,b),(b,c), we cannot find (a,c)
	/// Note different from <see cref="nameof(net_.INontrans)"/>
	/// eg:
	///		a->b, b->c, c->d,  is nontrans, reduced; when appended by "a->d", it becomes not reduced, meanwhile nontrans.
	/// </summary>
	/// <remarks>
	/// alias:
	///		trans reduced
	///		shortcutless.
	///			
	///		
	/// eg:
	///		a->b, b->d, a->c, c->d. There are alternatives; but there are no shortcuts.
	/// </remarks>
	public interface IShortcutless :
		INet

	#region trans reduced might be still trans. eg: {}, {a->b}, or {a->b, c->d}
#if true
		,
		_obj.typ.child.calc_.unary_.IComplement<INet,IShortcutsome>

#endif
	#endregion

	{
	}
}

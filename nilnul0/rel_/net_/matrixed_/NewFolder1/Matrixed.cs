﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nilnul._net_;
using nilnul.obj;

namespace nilnul.net_
{
	[Obsolete()]
	public class Matrixed<T>
		:nilnul.NetI<T>

	{
		private IEnumerable<T> _objs;

		public IEnumerable<T> objs
		{
			get { return _objs; }
			//set { _objs = value; }
		}

		private IEqualityComparer<T> _objEq;

		public IEqualityComparer<T> objEq
		{
			get { return _objEq; }
			set { _objEq = value; }
		}


		private nilnul.rel_.net._mat.Adjace _matrix;
		public nilnul.rel_.net._mat.Adjace matrix
		{
			get { return _matrix; }
			//set { _matrix = value; }
		}

		SetI1<T> _net_.ObjsI<T>.objs
		{
			get
			{
				return new nilnul.obj.Set1<T>(_objEq,  _objs);
				//throw new NotImplementedException();
			}
		}

		public int objsCount { get {
				return _objs.Count();
			} }
		public ObjsI2<Tuple<T, T>> duos
		{
			get {
				return new nilnul.objs_.enumerable_.enum2Arr_.BoxEnumerable<Tuple<T,T>>(
					duoEnumerated
				);
			}
		}
		public IEnumerable<Tuple<T, T>> duoEnumerated
		{
			get
			{
				var objsCount = this.objsCount;
				for (int row = 0; row < objsCount; row++)
				{
					for (int col = 0; col < objsCount; col++)
					{
						if (_matrix[row,col] )
						{
							yield return new Tuple<T,T>(_objs.ElementAt(row), _objs.ElementAt(col));
						}
					}
				}

				//throw new NotImplementedException();
			}
		}

		public Matrixed(
			nilnul.Net<T> net, IEqualityComparer<T> objEq
		)
		{
			var objs=net.objs.toArr();
			var count = objs.Count();

			bool[,] matrix= new bool[count,count];

			var duos = net.duos.toArr();

			for (int i = 0; i < count; i++)
			{
				for (int j = 0; j < count; j++)
				{
					matrix[i, j] = duos.Contains(
						new Tuple<T, T>(objs.ElementAt(i), objs.ElementAt(j))
						,
						new nilnul.obj.duo.eq_.OfCouple<T>(
							objEq
						)
					);
				}
			}

			_objs = objs;

			_matrix= new nilnul.rel_.net._mat.Adjace( matrix);

			_objEq = objEq;
		}
	}
}

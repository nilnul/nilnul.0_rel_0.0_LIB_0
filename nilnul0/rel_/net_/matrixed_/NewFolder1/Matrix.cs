﻿using nilnul.collection.matrix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net
{
	/// <summary>
	/// get a bool.matrix_.square
	/// </summary>
	///

	[Obsolete()]
	 public  class Matrix
	{
		/// <summary>
		/// get the relation matrix
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="relation"></param>
		/// <returns></returns>
		static public bool[,] GetRelationMatrix<T>(
			nilnul.rel_.Net<T> relation	
		) {
			//get the field.

			var field = relation.field;

			var fieldCount=field.Count;

			var matrix=new bool[fieldCount,fieldCount];

			for (int i = 0; i < fieldCount; i++)
			{
				for (int j = 0; j < fieldCount; j++)
				{
					matrix[i,j]=relation.match.has(field.ElementAt(i),field.ElementAt(j));
					
				}
				
			}
			return matrix;
			//throw new NotImplementedException();
		}



		/// <summary>
		/// sum of product.
		/// </summary>
		/// <param name="a_square"></param>
		/// <param name="b_square"></param>
		/// <returns></returns>
		static public bool[,] Multi_sameSizeSquare(bool[,] a_square, bool[,] b_square) {
			var size = a_square.GetLength(0);
			var r=new bool[size,size];
			for (int row = 0; row < size; row++)
			{
				for (int col = 0; col < size; col++)
				{
					var rowI = nilnul.collection.matrix.MatrixX.Row<bool>(a_square, row);
					var colJ = nilnul.collection.matrix.MatrixX.Column(b_square, col);

					r[row, col] = nilnul.str.duo_.sameLen.of_.bit.InnerProduct._Multi_innerProduct_sameSize(rowI, colJ);

					
				}
				
			}
			return r;
			//throw new  NotImplementedException();
		}

	
		
		

		private bool[,] _val;

		public bool[,] val
		{
			get
			{
				return _val;
			}
			set
			{
				if (value == null)
				{
					_val = null;

					return;
				}

				if (value.GetLength(0) != value.GetLength(1))
				{
					throw new ArgumentException("It should be a square ");

				}

				this._val = value;
			}
		}

	

		public Matrix(bool[,] val)
		{
			this.val = val;

		}



		public bool eq(Matrix other)
		{
			return Eq(this, other);


		}


		public bool eq(bool[,] other)
		{

			return this.eq(new Matrix(other));

		}


		public bool Eq(Matrix a, Matrix b)
		{

			return MatrixX.Eq<bool>(a._val, b._val, (c, d) => c == d);

		}

		public bool Eq(bool[,] a,bool[,] b) {

			return Eq(new Matrix(a), new Matrix(b));
		
		}

		public int getLength(int rank)
		{

			if (rank != 0 || rank != 1)
			{
				throw new IndexOutOfRangeException();

			}
			if (_val == null)
			{
				return 0;

			}

			return val.GetLength(rank);
		}

		/// <summary>
		/// the number of nodes. not the total number of elements.
		/// </summary>
		public int length
		{
			get
			{
				if (_val == null)
				{
					return 0;

				}
				return val.GetLength(0);
			}

		}




		public bool this[int row, int col]
		{
			get
			{
				return val[row, col];
			}
			set
			{
				val[row, col] = value;
			}
		}

		public IEnumerable<IEnumerable<bool>> getCols()
		{
			for (int i = 0; i < length; i++)
			{
				yield return getCol(i);

			}

		}

		public IEnumerable<bool> getCol(int col)
		{
			for (int row = 0; row < length; row++)
			{
				yield return val[row, col];

			}
		}

		public void addRows(int rowsAugend, int rowsAddend)
		{

			for (int i = 0; i < length; i++)
			{
				_val[rowsAugend, i] |= _val[rowsAddend, i];

			}

		}

		static public void AddRows(bool[,] matrix_square, int rowsAugend_inBound, int rowsAddend_inBound)
		{
			var len = matrix_square.GetLength(0);

			for (int i = 0; i < len; i++)
			{
				matrix_square[rowsAugend_inBound, i] |= matrix_square[rowsAddend_inBound, i];

			}

		}

		static public void RowOredByOther(bool[,] matrix_square, int rowsAugend_inBound, int rowsAddend_inBound)
		{
			var len = matrix_square.GetLength(0);

			for (int i = 0; i < len; i++)
			{
				matrix_square[rowsAugend_inBound, i] |= matrix_square[rowsAddend_inBound, i];

			}

		}


		static public bool IsOrNot(bool[,] m)
		{

			if (m == null)
			{
				return true;

			}
			if (m.GetLength(0) != m.GetLength(1))
			{
				return false;

			}
			return true;

		}


		static public void Assert(bool[,] m)
		{

			if (m == null)
			{
				return;

			}
			if (m.GetLength(0) != m.GetLength(1))
			{
				throw new Exception("Should be square.");

			}

		}

		public override string ToString()
		{
			return MatrixX.ToString_(_val);
		}

		static public Matrix Clone(Matrix matrix) {
			//matrix.val.Clone();
			return new Matrix((bool[,]) (matrix.val.Clone()));
		
		}





	}
}

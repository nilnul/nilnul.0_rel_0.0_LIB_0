﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// there are multiple items in a poset node.
	/// </summary>
	/// <remarks>
	/// alias:
	///		preference relation
	/// </remarks>
	public interface IWeakOrder:net_.preord_.ITotal
	{
	}
}

﻿using nilnul._rel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul
{


	public interface OrdI<D,TD_Eq, R, TR_Eq>
		:RelI<D,TD_Eq, R,TR_Eq>
		where TD_Eq : IEqualityComparer<D>
		where TR_Eq : IEqualityComparer<R>

	{

	}

	public interface OrdI<T> : RelI<T> {

	}

	public interface OrdI<T,TEq>:OrdI<T,TEq, T,TEq>
		where TEq : IEqualityComparer<T>

	{

	}


	static public class OrdX
	{
		static public bool Eq<T,TEq, TOrd>(
			this TOrd ord, T x, T y	
		)
		where TEq : IEqualityComparer<T>
		where TOrd:OrdI<T,TEq>

		{
			return ord.contains(x, y) && ord.contains(y, x);


		}
		static public bool Eq<T, TOrd>(
			this TOrd ord, T x, T y	
		)
		where TOrd:OrdI<T>

		{
			return ord.contains(x, y) && ord.contains(y, x);


		}

		static public bool Ne<T,TEq, TOrd>(
			this TOrd ord, T x, T y	
		)
		where TEq : IEqualityComparer<T>
		where TOrd:OrdI<T,TEq>

		{
			return !Eq<T,TEq,TOrd>(ord, x, y);


		}
		static public bool Le<T,TEq, TOrd>(
			this TOrd ord, T x, T y	
		)
		where TEq : IEqualityComparer<T>
		where TOrd:OrdI<T,TEq>

		{
			return ord.contains( x, y);


		}
		static public bool Lt<T,TEq, TOrd>(
			this TOrd ord, T x, T y	
		)
		where TEq : IEqualityComparer<T>
		where TOrd:OrdI<T,TEq>

		{
			return Le<T,TEq,TOrd>( ord, x, y) && Ne<T,TEq,TOrd>(ord,x,y);


		}

		static public bool Ge<T,TEq, TOrd>(
			this TOrd ord, T x, T y	
		)
		where TEq : IEqualityComparer<T>
		where TOrd:OrdI<T,TEq>

		{
			return  Le<T,TEq,TOrd>( ord,  y,x) ;


		}


		static public bool Gt<T,TEq, TOrd>(
			this TOrd ord, T x, T y	
		)
		where TEq : IEqualityComparer<T>
		where TOrd:OrdI<T,TEq>

		{
			return  Gt<T,TEq,TOrd>( ord,  y,x) && Ne<T,TEq,TOrd>(ord,x,y);


		}

	}
}

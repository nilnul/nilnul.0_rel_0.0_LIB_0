﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.inside
{
	public class AsEq<T> : IEqualityComparer<T>
	{
		private nilnul.rel.InsideI<T> _inside;

		public nilnul.rel.InsideI<T> inside
		{
			get { return _inside; }
			set { _inside = value; }
		}

		public AsEq(
			nilnul.rel.InsideI<T> inside
			)
		{
			_inside = inside;
		}
		public bool Equals(T x, T y)
		{

			return _inside.inside(x, y) && _inside.inside(y, x);
			throw new NotImplementedException();
		}

		public int GetHashCode(T obj)
		{
			return 0;

			throw new NotImplementedException();
		}
	}
}

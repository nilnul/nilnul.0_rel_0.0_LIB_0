﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.poset.op_.of_
{
	/// <summary>
	/// given a set of T instance, the <see cref="nameof(rel._has.re_.net_.IPoset)"/> determines the steps among them
	/// </summary>
	public class OfRe<T>
		: nilnul.obj.Box1<rel._has.re_.net_.PosetI<T>>
		,
		PosetI<T>
	{
		private nilnul.obj.SetI2<T> _field;

		public nilnul.obj.SetI2<T> field
		{
			get { return _field; }
			set { _field = value; }
		}

		public IEnumerable<(T, T)> couples => nilnul.obj.str.strung.op_.binary_._CartesianX.Seq(_field,_field).Where(
			t=>

			boxed.re(t.Item1,t.Item2)
		);

		public OfRe(rel._has.re_.net_.PosetI<T> val, nilnul.obj.SetI2<T> field0) : base(val)
		{
			_field = field0;
		}


	}
}

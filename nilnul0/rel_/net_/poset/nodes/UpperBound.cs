﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.ord
{
	/// <summary>
	/// get the upperboundS of a subset.
	/// </summary>
	public class UpperBound
	{
		static public HashSet<T> _GetUpperBounds<T>( Ord<T> ord, T _element_inField) {

			return new HashSet<T>(
				ord.avowed.Where( duo=> ord.avowed.elementEq.Equals( duo.Item1, _element_inField )  ).Select(duo1=>duo1.Item2)
				,
				ord.avowed.elementEq
				
			);
		}

		static public HashSet<T> _GetUpperBounds<T>( Ord<T> ord, IEnumerable<T> _elementsStarted_inField) {


			return nilnul.slew_.set.aggregate_.Union._Eval(
				ord.avowed.elementEq
				,
				_elementsStarted_inField.Select(element=>
					_GetUpperBounds(ord,element )
				)
				
			);
		}


	}
}

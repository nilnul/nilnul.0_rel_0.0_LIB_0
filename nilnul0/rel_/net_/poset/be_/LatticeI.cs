﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel_.net_.poset.be_
{
	/// <summary>
	/// 	both meet semilattice and join semilattice
	/// </summary>
	public interface ILattice
		:
		be_.semilattice_.IMeet
		,
		be_.semilattice_.IJoin
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.poset.be_.semilattice_
{
	/// <summary>
	/// every pair of elements has a unique supremum (also called a least upper bound or join)
	/// </summary>
	/// <remarks>
	///
	/// </remarks>
	/// partially ordered, and for any nonempty finite subset, there is a join (least upper bound).
	/// 
	public interface IJoin:ISemiLattice
	{
	}

	/*
	 Join semilattices (which are partially ordered sets) are directed sets as well, but not conversely.
	 */
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.net_.poset.subset
{
	/// <summary>
	/// the maximal elements of a subset of a poset.
	/// </summary>
	/// <remarks>
	/// So this is a set. this set may be:
	///		empty
	///		single
	///		many
	///		or infinite
	///	if the set is single, the element is called greatest/least.
	/// </remarks>
	public interface FrontierI
	{
	}
}

﻿using nilnul.obj.seq_.str.be_;
using nilnul.obj.str;
using nilnul.order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel_.net_.poset.nodes_
{
	static public class _MinmimalX
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="nodes"></param>
		/// <param name="posetLt">lt relation</param>
		/// <returns></returns>
		static public IEnumerable<T> _Minimal_funcAssumeLt<T>
		(
			IEnumerable<T> nodes
			,
			Func<T,T,bool> posetLt
		)
		{
			return nodes.Where(
				m => nodes.None(
					n => posetLt(n, m)

				)
			);

		}
		static public IEnumerable<T> _Minimal_funcAssumeGt<T>
		(
			IEnumerable<T> nodes
			,
			Func<T,T,bool> posetGt
		)
		{
			return _Minimal_funcAssumeLt(
				nodes
				,
				(x,y) => posetGt(y,x)
			);

		}


		static public IEnumerable<T> Minimal<T>
		(
			IEnumerable<T> nodes
			,
			nilnul.rel._has.re_.net_.PosetI<T> posetLe
		)
		{
			var lt = new nilnul.rel._has.re_.net_.poset.Strict<T>(posetLe);

			return nodes.Where(
				m => nodes.None(
					n => lt.re(n, m)

				)
			);

		}

		static public IEnumerable<T> Minimal_ofContrast<T>
		(
			IEnumerable<T> nodes
			,
			nilnul.rel._has.re_.net_.poset_._contraster.ContrastI<T> posetLe
		)
		{
			var lt = new rel._has.re_.net_.poset_._contraster.contrast.Lt<T>(posetLe);
			
			return nodes.Where(
				m => nodes.None(
					n =>lt.lt(n,m)

				)
			);

		}

	

		static public IEnumerable<IEnumerable< T>> Minimal_nulMin<T>
		(
			IEnumerable< IEnumerable< T >> nodes
			,
			nilnul.rel._has.re_.net_.PosetI<T> posetLe
		)
			where T:class
		{
			return Minimal_ofContrast<IEnumerable<T>>(
				nodes,
				new nilnul.rel._has.re_.net_.poset.Contrast< IEnumerable<T> > (
					new rel._has.re_.net_.poset_.elNulable.contrast.onStr_.NulMin<T>(posetLe)
				)
			);

		}

		/// <summary>
		/// on str
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="nodes"></param>
		/// <param name="posetLe"></param>
		/// <returns></returns>
		static public IEnumerable<IEnumerable< T>> Minimal<T>
		(
			IEnumerable< IEnumerable< T >> nodes
			,
			nilnul.rel._has.re_.net_.poset_.ElNulableI<T> posetLe
		)
			where T:class
		{
			return Minimal_ofContrast<IEnumerable<T>>(
				nodes,
				new nilnul.rel._has.re_.net_.poset.Contrast< IEnumerable<T> > (

					new rel._has.re_.net_.poset_.elNulable.contrast.OnStr<T>(posetLe)
				)
			);

		}

		static public IEnumerable<IEnumerable< T>> Minimal_nulMax<T>
		(
			IEnumerable< IEnumerable< T >> nodes
			,
			nilnul.rel._has.re_.net_.PosetI<T> posetLe
		)
			where T:class
		{
			return Minimal(
				nodes,
				

				new rel._has.re_.net_.poset.NulMax<T>(posetLe)
				
			);

		}


	}
}

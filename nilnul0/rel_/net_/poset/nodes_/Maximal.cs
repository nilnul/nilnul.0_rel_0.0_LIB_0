﻿using nilnul.obj.seq_.str.be_;
using nilnul.obj.str;
using nilnul.order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel_.net_.poset.nodes_
{
	static public class _MaximalX
	{
		static public IEnumerable<T> _Maximal_funcAssumeGt<T>
		(
			IEnumerable<T> nodes
			,
			Func<T,T, bool> gt
		)
		{
			return _MinmimalX._Minimal_funcAssumeLt(
				nodes
				,
				 gt
			);
		}

		static public IEnumerable<T> _Maximal_funcAssumeLt<T>
		(
			IEnumerable<T> nodes
			,
			Func<T,T, bool> lt
		)
		{
			return _MinmimalX._Minimal_funcAssumeLt(
				nodes
				,
				(x,y) => lt(y,x)
			);
		}


		static public IEnumerable<T> Maximal<T>
		(
			IEnumerable<T> nodes
			,
			nilnul.rel._has.re_.net_.PosetI<T> posetLe
		)
		{
			return _MinmimalX.Minimal(
				nodes
				,
				new nilnul.rel._has.re_.net_.poset.Converse<T>(posetLe)
			);


		}

		/// <summary>
		/// on str
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="nodes"></param>
		/// <param name="posetLe"></param>
		/// <returns></returns>
		static public IEnumerable<IEnumerable<T>> Maximal<T>
		(
			IEnumerable<IEnumerable<T>> nodes
			,
			nilnul.rel._has.re_.net_.poset_.ElNulableI<T> posetLe
		)
			where T:class
		{
			return _MinmimalX.Minimal(
				nodes
				,
				new nilnul.rel._has.re_.net_.poset_.elNulable.Converse<T>(posetLe)  
			); 

		}

		static public IEnumerable<IEnumerable<T>> Maximal_nulMin<T>
		(
			IEnumerable<IEnumerable<T>> nodes
			,
			nilnul.rel._has.re_.net_.PosetI<T> posetLe
		)
			where T:class
		{
			return Maximal(
				nodes
				,
				new nilnul.rel._has.re_.net_.poset.NulMin<T>(posetLe)  
			); 

		}

		static public IEnumerable<IEnumerable<T>> Maximal_nulMax<T>
		(
			IEnumerable<IEnumerable<T>> nodes
			,
			nilnul.rel._has.re_.net_.PosetI<T> posetLe
		)
			where T:class
		{
			return Maximal(
				nodes
				,
				new nilnul.rel._has.re_.net_.poset.NulMax<T>(posetLe)  
			); 

		}


	
	}
}

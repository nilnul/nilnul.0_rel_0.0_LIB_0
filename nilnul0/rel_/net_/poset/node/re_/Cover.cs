﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.poset.coerce_
{
	/// <summary>
	/// An element ,z, of a partially ordered set  covers another element ,x, provided that there exists no third element, y, in the poset for which x<=y<=z. In this case, z is called an "upper cover" of x and x, a "lower cover" of z. 
	/// This is a transitive refexive reduction. 
	/// The resulted cover is a net. but is it a poset? it's not.
	/// </summary>
	public interface CoverI
	{
	}
}

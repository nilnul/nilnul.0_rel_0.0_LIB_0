﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.order.bound
{


	public partial class Eq<T,TComparer,TBound>
	: IEqualityComparer<TBound>

		where TBound:Bound<T>
	{
		private IEqualityComparer<T> _elementEq;

		public IEqualityComparer<T> elementEq
		{
			get { return _elementEq; }
			set { _elementEq = value; }
		}

		public Eq(IEqualityComparer<T> elementEq)
		{
			this.elementEq = elementEq;

		}





		public bool Equals(TBound x, TBound y)
		{
			return elementEq.Equals(x.pinpoint, y.pinpoint) && x.openFalseCloseTrue == y.openFalseCloseTrue;

			throw new NotImplementedException();
		}

		public int GetHashCode(TBound obj)
		{
			return obj.openFalseCloseTrue.GetHashCode() ^ obj.pinpoint.GetHashCode();
			throw new NotImplementedException();
		}
	}

	public class Eq_comparerDefault<T,TComparer,TBound>
		:Eq<T,TComparer,TBound>
		where TComparer:IEqualityComparer<T>,new()
		where TBound:Bound<T>
	{

		static public Eq_comparerDefault<T, TComparer, TBound> Singleton = SingletonByDefaultNew<Eq_comparerDefault<T, TComparer, TBound>>.Instance;
		public Eq_comparerDefault()
			:base(SingletonByDefaultNew<TComparer>.Instance)
		{

		}

	}

	public class Eq_comparerDefault<T,TBound>
		:Eq<T,EqualityComparer<T>,TBound>
		where TBound:Bound<T>
	{

		static public Eq_comparerDefault<T,  TBound> Singleton = SingletonByDefaultNew<Eq_comparerDefault<T,  TBound>>.Instance;
		public Eq_comparerDefault()
			:base(EqualityComparer<T>.Default)
		{

		}

	}


	



	[Obsolete()]
	public partial class Eq<T, TComparer>
		: Eq<T>

		where TComparer : IEqualityComparer<T>
	{


		public Eq(TComparer comparer)
			: base(comparer)
		{

		}


	}	



		
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.order.bound.duo
{
	public class UpperX
	{
		static public bool Contains<T>(Bound<T> upper, T x, IComparer<T> comparer) {

			var comparedResult = comparer.Compare(upper.pinpoint, x);
			return comparedResult > 0 | (comparedResult == 0 &&  upper.openFalseCloseTrue);

		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.order.bound.duo
{
	public class LowerX
	{
		static public bool Contains<T>(Bound<T> lower, T x, IComparer<T> comparer) {

			var comparedResult = comparer.Compare(lower.pinpoint, x);
			return comparedResult < 0 | (comparedResult == 0 &&  lower.openFalseCloseTrue);

		}
	}
}

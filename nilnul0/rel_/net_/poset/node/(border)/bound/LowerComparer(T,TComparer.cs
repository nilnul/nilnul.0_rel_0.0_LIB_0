﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.order.bound
{

	public partial class LowerComparer<T, TComparer>
		: 

		LowerComparer<T>,
		IComparer<Bound<T>>
		where TComparer : IComparer<T>
	{
		private TComparer  _elementComparer;
		public TComparer  elementComparer
		{
			get { return _elementComparer; }
			set { _elementComparer = value; }
		}

		public LowerComparer(TComparer elementComparer)
			:base(elementComparer)
		{
			_elementComparer = elementComparer;
		}

		public int Compare(Bound<T> x, Bound<T> y)
		{
			var c = _elementComparer.Compare(x.pinpoint, y.pinpoint);

			if (c == 0)
			{
				if (x.openFalseCloseTrue && !y.openFalseCloseTrue)
				{
					return -1;

				}
				if (!x.openFalseCloseTrue && y.openFalseCloseTrue)
				{
					return 1;
				}

			}
			return c;

		}

		
	}
}

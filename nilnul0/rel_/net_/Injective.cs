﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// for any item in domain, there is no more than 1 img. in other words, there is either 1 or 0 tgt in range.
	/// </summary>
	/// <remarks>
	/// alias:
	///		injective
	/// </remarks>
	public interface IInjective:
		
		rel_.IInjective
		,
		INet
	{ }

}

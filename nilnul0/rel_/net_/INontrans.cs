﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// not transitive. Note some local transitive is still possible; different from <see cref="nameof(net_.IShortcutless)"/>
	/// </summary>
	/// <remarks>
	/// vs:
	///		<see cref="nameof(IShortcutless)"/>:
	///			{a->b} is reduced, and trans.  not nontrans
	///			{} is reduced, and trans. not nontrans.
	///			
	/// </remarks>
	public interface INontrans:
		INet
		,
		_obj.typ.child.calc_.unary_.IComplement<INet,net_.ITrans>


	{
	}
}

﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// <see cref="nameof(ICyclic4none)"/>
	/// not any cyclic  , should we make it transitive.
	/// Note: the edge is one way; cannot go agaist the direction.
	/// </summary>
	/// <remarks>
	/// alias:
	///		acyclic
	///		acyclic4all
	///	
	/// </remarks>
	///

	public interface IAcyclic4all : ICyclic4none
		
	{ }

	public interface IAcyclic4all<T> : IAcyclic4all { }
}

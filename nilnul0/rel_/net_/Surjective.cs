﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// if the tgted eq field
	/// </summary>
	/// <remarks>
	/// alias:
	///		injective
	/// </remarks>
	public interface ISurjective:
		INet ,
		rel_.ISurjective{ }

}

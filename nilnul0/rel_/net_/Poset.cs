﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{

	public class Poset<T> :
		rel_.net.be_.poset.vow.Ee<T>
		,
		IPoset

		,
		rel_.NetI<T>
	{
		public Poset(Net1<T> val) : base(val)
		{
		}
		public Poset():this(
			new Net1<T>()
		)
		{

		}

		public SetI2<T> field => this.ee.field;

		public IEnumerable<(T, T)> couples => this.ee.couples;
	}

}

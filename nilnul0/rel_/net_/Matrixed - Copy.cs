﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nilnul._net_;
using nilnul.obj;
using nilnul.rel_;
using nilnul.rel_._net;
using nilnul.rel_._net_;

namespace nilnul.rel_.net_
{
	public class Matrixed<T>
		:nilnul.rel_.NetI<T>
		,
		_net_.WidowsI<T>
		,
		ClonableI< nilnul.rel_.Net1<T>>

	{

		private IEqualityComparer<T> _nodeEq;

		public IEqualityComparer<T> nodeEq
		{
			get { return _nodeEq; }
			set { _nodeEq = value; }
		}

		private IEnumerable<T> _nodes;

		public IEnumerable<T> nodes
		{
			get { return _nodes; }
			//set { _objs = value; }
		}

		private nilnul.rel_.net._mat.Adjace _matrix;
		public nilnul.rel_.net._mat.Adjace matrix
		{
			get { return _matrix; }
			//set { _matrix = value; }
		}

		

		public int objsCount { get {
				return _nodes.Count();
			} }

		public IEnumerable<(T, T)> couples
		{
			get
			{
				var objsCount = this.objsCount;
				for (int row = 0; row < objsCount; row++)
				{
					for (int col = 0; col < objsCount; col++)
					{
						if (_matrix[row,col] )
						{
							yield return (_nodes.ElementAt(row), _nodes.ElementAt(col));
						}
					}
				}

				//throw new NotImplementedException();
			}
		}
		
		public Mate<T> mate => new Mate<T>(
			this._nodeEq,
			couples
		);

		public IEnumerable<T> widows =>	this._nodes.Except(
			mate.field
		);

		public SetI2<T> field => new Set2<T>(
			this._nodeEq
			,
			this._nodes
		);

		public Matrixed(
			IEqualityComparer<T> eq
			,
			IEnumerable<T> nodes
			,
			rel_.net._mat.Adjace matrix
		)
		{
			nilnul.bit.vow_.True1.Vow(

				matrix.width == nodes.Count()
			)
			;
			this._nodeEq = eq;
			this._nodes = nodes;
			this._matrix = matrix;

		}
		public Matrixed(
			IEqualityComparer<T> memberEq,
			IEnumerable<T> field1
			,
			 bool[,] matrix1
		):this(
			memberEq,field1,new net._mat.Adjace(matrix1)
		)
		{
		}

		public Matrixed(SetI2<T> field1, bool[,] matrix1)
			:this(
				 field1.memberEq
				 ,
				 field1
				 
				 ,matrix1
		)
		{
		}


			public Matrixed(
			nilnul.obj.SetI2<T>  nodes
			,
			rel_.net._mat.Adjace matrix
		):this(
			nodes.memberEq
			,
			nodes
			,
			matrix
		)
		{
			

		}
	public Matrixed(
			nilnul.rel_.Net1<T> net//, IEqualityComparer<T> objEq
		)
		{
			var objEq = net.elEq;
			var objs=net.field.ToArray();
			var count = objs.Count();

			bool[,] matrix= new bool[count,count];

			var duos = net.mate.toArr();

			for (int i = 0; i < count; i++)
			{
				for (int j = 0; j < count; j++)
				{
					matrix[i, j] = duos.Contains(
						new Co<T>(objs.ElementAt(i), objs.ElementAt(j))
						,
						new nilnul.obj.co.Eq<T>(
							objEq
						)
					);
				}
			}

			_nodes = objs;

			_matrix= new nilnul.rel_.net._mat.Adjace( matrix);

			_nodeEq = objEq;
		}

		public Net1<T> clone()
		{
			return new Net1<T>(
				this.mate
				,
				this.nodes

			);
		}
	}
}

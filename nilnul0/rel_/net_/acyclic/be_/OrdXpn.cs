﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.acyclic.be_
{
	/// <summary>
	/// 
	///	always return false, cuz:
	///		while irreflexive is implied, then acyclic cannot be in any case an ord.
	///	
	///	But acyclic can be converted to ord, by:
	///		1) transitivity closure.
	///		2) and reflexivity closure
	///	note here transitivity closure doenot necessary imply reflexivity closure.
	///	
	/// </summary>
	class Ord
	{

	}
}

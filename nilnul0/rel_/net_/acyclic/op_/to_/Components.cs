﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.acyclic.op_.to_
{
	/// <summary>
	/// to disjoint <see cref="nameof(cruxed_.IAcyclic)"/> or <see cref="nameof(net_.IDac)"/>.
	/// This cannot be done, if the net is not <seealso cref="nameof(dwelt_.connected_.lean)"/>.
	/// </summary>
	class Compartments
	{
	}
}

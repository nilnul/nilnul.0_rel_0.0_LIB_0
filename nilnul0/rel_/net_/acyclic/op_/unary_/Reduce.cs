﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.acyclic.op_.unary_
{
	/// <summary>
	/// make a transitivity reduction
	/// </summary>
	static  public class _ReduceX
	{
		static public nilnul.rel_.Net1<T> Net_assumeAcyclic<T>(
			nilnul.rel_.Net1<T> net

		) {
			var matrix = nilnul.rel_.net._mat.Adjacency.GetRelationMatrix(net);

			///closure it
			///
			nilnul.rel_.net_.acyclic.op_.unary_._ClosureX._Act(matrix);

			//reduce matrix

			nilnul.rel_.net_.acyclic_.closured._ReduceX.Act_assumeClosure(
				matrix
			);



			///generate net from matrix
			///

			var matrixed = new nilnul.rel_.net_.Matrixed<T>(
				net.field
				,
				matrix
			);

			return matrixed.clone();





		}
		static public (IEnumerable<T> nodes,  IEnumerable<(T,T)> edges) NodesEdges_assumeAcyclic<T>(
			nilnul.rel_.Net1<T> net

		) {
			var matrix = nilnul.rel_.net._mat.Adjacency.GetRelationMatrix(net);

			///closure it
			///
			nilnul.rel_.net_.acyclic.op_.unary_._ClosureX._Act(matrix);

			//reduce matrix

			nilnul.rel_.net_.acyclic_.closured._ReduceX.Act_assumeClosure(
				matrix
			);

			

			///generate net from matrix
			///

			var matrixed = new nilnul.rel_.net_.Matrixed<T>(
				net.field
				,
				matrix
			);

			return (matrixed.nodes, matrixed.couples);

			





		}

	}
}

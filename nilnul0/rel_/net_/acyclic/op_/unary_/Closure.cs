﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.acyclic.op_.unary_
{
	/// <summary>
	/// this is transitivity closure. The result is transitive and asym; so the result is <see cref="nameof(net_.IHier)"/>, or strict poset.
	/// reflexivity closure is not implied, and must be done separately. Note if reflexivity closure is applied, the net is not acyclic any more. So that is not a op which is closed; and that result must be placed under reflexive. <see cref="nameof(net_.reflex.op_.of_.Constrict)"/>
	/// </summary>
	/// <remarks>
	/// 'cuz reflexivity is not closured, so this might be not poset.
	/// </remarks>
	static public class _ClosureX
	{

		static public void _Act(bool[,] matrix)
		{
			var dim = matrix.GetLength(0);

			for (int col = 0; col < dim; col++)
			{
				for (int row = 0; row < dim; row++)
				{
					if (matrix[row, col])
					{
						nilnul.rel_.net._mat.Adjace.OrOtherRow(matrix, row, col);
					}
				}
			}
		}

		static public nilnul.rel_.Net1<T> Net_assumeAcyclic<T>(
	nilnul.rel_.Net1<T> net

)
		{
			var matrix = nilnul.rel_.net._mat.Adjacency.GetRelationMatrix(net);

			///closure it
			///
			_Act(matrix);

			



			///generate net from matrix
			///

			var matrixed = new nilnul.rel_.net_.Matrixed<T>(
				net.field
				,
				matrix
			);

			return matrixed.clone();





		}


	}

	
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dwelt_.connected_.acyclic.op_.unary_
{
	static public class _ClosureX
	{

		static public void _Act(bool[,] matrix)
		{
			var dim = matrix.GetLength(0);

			for (int col = 0; col < dim; col++)
			{
				for (int row = 0; row < dim; row++)
				{
					if (matrix[row, col])
					{
						nilnul.rel_.net._mat.Adjace.OrOtherRow(matrix, row, col);
					}
				}
			}
		}
	}

	
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.acyclic.extrem
{
	/// <summary>
	/// peel off the extrems layer by layer. the net will be altered.
	/// </summary>
	static public class _PeelX
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="_acyclic">will be mutated</param>
		/// <returns></returns>
		static public IEnumerable<T[]> _MinSetEs_assumeAcyclic<T>(
			nilnul.rel_.Net1<T> _acyclic
		)
		{
			/// for the returned is IEnumerable, which shall be readonly, and state remains unchanged during iteration, hence here we shall not change the iterated object. Or else during the execution process, we may inadvertly (for example, by calling the ".Count()" or peeking ahead) changed the iterated object before going forward and seeing something different from what we peek ahead previously
			///

			var _innerState4peeking = new nilnul.rel_.Net1<T>(
				_acyclic
			);

			var mins = acyclic._ExtremX.MinSet_assumeAcyclic(_innerState4peeking).ToArray();
			while (mins.Any())
			{
				yield return mins;
				_innerState4peeking.delNodes(mins);
				mins = acyclic._ExtremX.MinSet_assumeAcyclic(_innerState4peeking).ToArray();
			}
		}

		static public IEnumerable<T[]> _MinSetEs<T>(
			nilnul.rel_.net_.Acyclic4all<T> _acyclic
		)
		{
			return _MinSetEs_assumeAcyclic(
				_acyclic.ee
			);
		}


		static public IEnumerable<T[]> _MaxSetEs_assumeAcyclic<T>(
			nilnul.rel_.Net1<T> _acyclic
		)
		{
			var conversed = nilnul.rel_.net.op_.unary_._ConverseX.Converse(_acyclic);
			return _MinSetEs_assumeAcyclic(conversed);

			//var maxs = acyclic._ExtremX.MaxSet_assumeAcyclic(_acyclic).ToArray();
			//while (maxs.Any())
			//{
			//	yield return maxs;
			//	_acyclic.delNodes(maxs);
			//	maxs = acyclic._ExtremX.MaxSet_assumeAcyclic(_acyclic).ToArray();
			//}
		}



	}
}

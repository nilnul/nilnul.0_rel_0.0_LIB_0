﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.acyclic
{
	/// <summary>
	/// maximum or minimum
	/// </summary>
	static public class _ExtremX
	{
		static public IEnumerable<T> MinSet_assumeAcyclic<T>(nilnul.rel_.Net1<T> _acyclic) {
			return _acyclic.field.Where(
				f=> !_acyclic.mate.range.Contains(f)
			);
		}

		static public IEnumerable<T> MaxSet_assumeAcyclic<T>(nilnul.rel_.Net1<T> _acyclic) {
			return _acyclic.field.Where(
				f=> !_acyclic.mate.domain.Contains(f)
			);
		}

	}
}

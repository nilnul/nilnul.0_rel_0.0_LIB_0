﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.acyclic
{
	/// <summary>
	/// also known as topological order, in which a total order is arbitrated
	/// </summary>
	public interface ITraverse:op_.ITo
		,
		op_.to_.ILinear
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dwelt_
{

	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// alias:
	///		integral,
	///		monolithic
	/// </remarks>
	public interface IConnected:IDwelt { }
	public class Connected<TEl>
		: nilnul.rel_.net_.dwelt.be_.connected.vow.Ee<TEl>

	{
		public Connected(Dwelt<TEl> val) : base(val)
		{
		}

		public Connected(Net<TEl> net):this(new Dwelt<TEl>(net) )
		{
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// eg:
	///		{a->b, b->c, c->d,   a->d}
	/// </summary>
	public interface INontrans1ducible : rel_.net_.INontrans, rel_.net_.IShortcutsome
	{
	}
}
﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.complete.op_
{
	public class OfSet<T>
		: nilnul.obj.Box1<nilnul.obj.SetI2<T>>
		,
		NetI<T>
	{
		public OfSet(SetI2<T> val) : base(val)
		{
		}

		public SetI2<T> field =>boxed;

		public IEnumerable<(T, T)> couples => nilnul.obj.str.strung.op_.binary_._CartesianX.Seq(
			boxed,boxed
		);
	}
}

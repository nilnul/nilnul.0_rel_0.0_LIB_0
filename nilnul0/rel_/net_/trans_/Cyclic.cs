﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.trans_
{
	/// <summary>
	/// eg:
	///		 a->a
	/// </summary>
	public interface ICyclic
		:
		
		ITrans
		,
		_obj.typ.child.calc_.unary_.IComplement<ITrans, IAcyclic>
		,
		_obj.typ.child.calc_.unary_.IAlias<ITrans,INonirreflex>
		,
		net_.ICyclic4some
		

	
	
	{
	}
}

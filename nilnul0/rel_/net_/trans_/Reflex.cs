﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.trans_
{
	/// <summary>
	/// 
	/// </summary>
	/// 
	///

	[Obsolete(nameof( net_.reflex_.ITrans) + " is prime taxonomy")]
	public interface IReflex
		:
		
		ITrans
		,
		net_.reflex_.ITrans
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.trans_.acyclic_
{

	[Serializable]
	public class NonirreflexException : Exception
		,
		IAcyclic
	{
		public NonirreflexException() { }
		public NonirreflexException(string message) : base(message) { }
		public NonirreflexException(string message, Exception inner) : base(message, inner) { }
		protected NonirreflexException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

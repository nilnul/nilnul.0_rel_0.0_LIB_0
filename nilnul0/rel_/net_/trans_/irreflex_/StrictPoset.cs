﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.trans_.irreflex_
{
	/// <summary>
	/// </summary>
	public interface IStrictPoset:
		nilnul._obj.typ.child.calc_.unary_.IAlias<IIrreflex, IStrictPoset>
		,
		IIrreflex
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.trans_.irreflex_
{
	/// <summary>
	/// 
	/// </summary>
	public interface IAcyclic
		:
		_obj.typ.child.calc_.unary_.comple_.IReflec<IIrreflex,CyclicException>
		,
		IIrreflex
	{
	}
}

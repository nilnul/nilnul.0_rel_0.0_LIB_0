﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.trans_.irreflex_
{
	/// <summary>
	/// 
	/// </summary>
	public interface IAsym
		:
		nilnul.obj.IAlias<IIrreflex>	//if a co is symmetric, by transitivity, the el is reflex, resulting a contradiction.
		,
		IIrreflex
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.trans_
{
	/// <summary>
	/// </summary>
	public interface IStrictPoset:
		nilnul.obj.IAlias<trans_.irreflex_.IStrictPoset>
		,
		ITrans
	{
	}
}

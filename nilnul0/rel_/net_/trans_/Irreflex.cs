﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.trans_
{
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// strict preord
	/// </remarks>
	public interface IIrreflex
		:
		_obj.typ.child.calc_.unary_.IAlias<ITrans, IAsym> //if som co is symmetric, then by trans, one el is reflex, contracting. On the other hand, for trans_.Asym, if som co is reflex, by trans, the co is symmetric, resulting contradiction, so it's Irreflex.

		,
		_obj.typ.child.calc_.unary_.IAlias<ITrans, IAcyclic>
		,
		ITrans
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.trans_
{
	/// <summary>
	/// eg:
	///		a->a
	/// </summary>
	public interface ISym:
		nilnul._obj.typ.child.calc_.unary_.IComplement<ITrans, trans_.IAsym>
		,
		ITrans
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.trans_
{
	/// <summary>
	/// eg:
	///		a->b, b->c, a->c
	/// </summary>
	/// <remarks>
	/// strict preord
	/// </remarks>
	public interface IAcyclic
		:
		
		ITrans
		,
		_obj.typ.child.calc_.unary_.IAlias<ITrans, IIrreflex>
		,
		_obj.typ.child.calc_.unary_.IAlias<ITrans, IAsym>

		,
		net_.IAcyclic4all
		

	
	
	{
	}
}

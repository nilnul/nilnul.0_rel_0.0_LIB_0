﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.trans_.asym_
{
	/// <summary>
	/// if it's cyclic, then there is cycle. then there is reflexivity and symmetry.
	/// </summary>

	[Serializable]
	public class CyclicException : Exception
	{
		public CyclicException() { }
		public CyclicException(string message) : base(message) { }
		public CyclicException(string message, Exception inner) : base(message, inner) { }
		protected CyclicException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

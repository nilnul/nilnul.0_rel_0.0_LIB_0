﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.trans_.asym_
{
	/// <summary>
	///  a transitive relation is
	///		asymmetric if and only if it is irreflexive
	/// </summary>
	public interface Irreflex:
		nilnul.obj.IAlias<IAsym>
		,
		IAsym
	{
	}
}

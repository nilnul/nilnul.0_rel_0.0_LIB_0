﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dwelt_.connected
{
	//extern alias obj;
	public interface BeI<T>
		:
		nilnul.obj.BeI1< nilnul.rel_.net_.dwelt_.Connected<T>>

	{

	}

	public class Be<T> :
		nilnul.obj.Be1<
nilnul.rel_.net_.dwelt_.Connected<T>
		>,

		BeI<T>
	{
		public Be(System.Func<Connected<T>, bool> func) : base(func)
		{
		}
	}



}

﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dwelt_.connected.vow.ee_
{
	public class VowDefaulted<T, TVow> :
		Ee<T>
		where TVow: VowI2<Connected<T>> ,new()
	{
		public VowDefaulted(Connected<T> val) : base(val, nilnul.obj_.Singleton<TVow>.Instance)
		{
		}
	}
}

﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dwelt_.connected.vow
{
	public class Ee<T>
		: nilnul.obj.vow.Ee1<rel_.net_.dwelt_.Connected<T>>
	{
		public Ee(net_.dwelt_.Connected<T> val, VowI2<Connected<T>> vow) : base(val, vow)
		{
		}
	}
}

﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dwelt_.connected.be
{
	public class Vow<T> : nilnul.obj.be.Vow4<nilnul.rel_.net_.dwelt_.Connected<T>>
		,
		rel_.net_.dwelt_.connected.VowI<T>
	{
		public Vow(BeI1<Connected<T>> be) : base(be)
		{
		}
	}
}

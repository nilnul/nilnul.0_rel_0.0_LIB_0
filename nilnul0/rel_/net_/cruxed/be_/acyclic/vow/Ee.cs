﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dwelt_.connected.be_.acyclic.vow
{
	public class Ee<TEl>
		: nilnul.rel_.net_.dwelt_.connected.vow.ee_.VowDefaulted<TEl, acyclic.Vow<TEl>>
	{
		public Ee(Connected<TEl> val) : base(val)
		{
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.cruxed_
{
	/// <summary>
	/// reflexive, symmetric
	/// </summary>
	public interface ICompatible
		: net_.ICruxed
		,
		net_.ICompatible

	{	}



}

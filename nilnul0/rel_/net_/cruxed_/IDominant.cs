﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.cruxed_
{
	/// <summary>
	/// 
	/// dwelt,
	/// irreflexive,transitive,
	///		hence acyclic
	/// connected
	/// </summary>
	/// <remarks>
	/// aka:
	///		strict partial order
	/// </remarks>
	///
	[Obsolete(nameof(net_.dac_.IDominant))]
	public interface IDominant
		:net_.ICruxed
		,
		net_.IDominant
	{
	}
}

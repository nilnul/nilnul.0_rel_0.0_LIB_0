﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.cruxed_.poset.op_.to_
{
	/// <summary>
	/// remove reflexiveness
	/// the result is :<see cref="nameof(net_.cruxed_.IDominant)"/>
	/// </summary>
	class Irreflex
		:ITo<cruxed_.IDominant>
	{
	}
}

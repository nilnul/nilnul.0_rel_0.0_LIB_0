﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.cruxed_
{
	/// <summary>
	/// <see cref="nameof(net_.IDac)"/>
	/// dwelt, connected, acyclic
	/// </summary>
	/// <remarks>
	/// alias:
	///		acyclic
	///		dag
	///			directed acyclic graph
	///			dwelt, acyclic, gross/genesis/germinat
	///		dac
	///			swelt acyclic, connected
	///		
	/// </remarks>
	///
	[Obsolete(nameof(net_.IDac))]
	public interface IAcyclic
		:ICruxed,
		net_.IAcyclic4all
	{
	}
}

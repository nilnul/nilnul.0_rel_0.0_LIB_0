﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.cruxed_.acoil_
{
	/// <summary>
	/// 
	/// </summary>

	[Serializable]
	public class PosetException : Exception
	{
		public PosetException() { }
		public PosetException(string message="poset is reflexive, thus not acoil") : base(message) { }
		public PosetException(string message, Exception inner) : base(message, inner) { }
		protected PosetException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

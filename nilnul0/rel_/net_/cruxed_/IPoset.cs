﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.cruxed_
{
	/// <summary>
	/// dwelt,
	/// reflexive, symmetric, transitive,
	/// connected
	/// </summary>
	public interface IPoset
		:net_.ICruxed
		,
		net_.IPoset
	{
	}
}

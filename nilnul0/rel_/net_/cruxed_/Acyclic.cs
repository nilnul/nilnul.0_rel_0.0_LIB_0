﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.dwelt_.connected_
{
	/// <summary>
	/// 
	/// </summary>
	

	public class Acyclic<TEl>
		: nilnul.rel_.net_.dwelt_.connected.be_.acyclic.vow.Ee<TEl>

	{
		public Acyclic(Connected<TEl> val) : base(val)
		{
		}
		public Acyclic(rel_.Net<TEl> net):this(new Connected<TEl>(net) )
		{

		}
	}
}

﻿using System.Collections.Generic;

namespace nilnul.rel_.net_
{
	public class Acyclic4all<TEl>
		:
		//nilnul.rel_.net.vow.ee_.VowDefault1<
		//	TEl, nilnul.rel_.net.be_.acyclic.Vow<TEl>
		//>
		nilnul.rel_.net.be_.acyclic.vow.Ee<TEl>
		,
		IAcyclic4all

	{
		public Acyclic4all(Net1<TEl> val) : base(val)
		{
		}
		public Acyclic4all(IEnumerable<TEl> vs, (TEl, TEl)[] ps, IEqualityComparer<TEl> eq)
			:this(new Net1<TEl>(vs,eq,ps))
		{

		}
		public Acyclic4all(IEnumerable<TEl> vs, (TEl, TEl)[] ps)
			:this(new Net1<TEl>(vs,EqualityComparer<TEl>.Default,ps))
		{

		}
		public Acyclic4all():this(new Net1<TEl>())
		{

		}


	}
}

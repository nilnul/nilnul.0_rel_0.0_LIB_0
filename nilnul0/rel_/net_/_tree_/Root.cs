﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._tree_
{
	/// <summary>
	///  the root of tree
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface RootI< out T>
	{
		T root { get; }
	}


}

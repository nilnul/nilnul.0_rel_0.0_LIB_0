﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._tree_
{
	/// <summary>
	/// a net that is:
	///		not empty (must have one or more)
	///		acyclic
	///		connected (one way or the other)
	///		directed (always from root to child)
	/// </summary>
	public interface BlankI
	{

	}


}

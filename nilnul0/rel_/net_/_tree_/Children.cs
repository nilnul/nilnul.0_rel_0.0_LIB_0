﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_._tree_
{
	/// <summary>
	/// a set of trees
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface Children< out T>
	{
		T children { get; }
	}


}

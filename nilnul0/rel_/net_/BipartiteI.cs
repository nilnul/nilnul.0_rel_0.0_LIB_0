﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.net_
{
	/// <summary>
	/// can be partitioned into sets and :
	/// no connections exists within a set
	/// each connection must be from one set to the other.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface BipartiteI<T>:nilnul.NetI<T>
	{

	}
}

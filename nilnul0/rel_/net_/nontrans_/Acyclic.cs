﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.nontrans_
{
	/// <summary>
	///  eg:
	///		a->b
	/// </summary>
	public interface IAcyclic
		:
		INontrans
		,
		net_.IAcyclic4all
		,
		_obj.typ.child.calc_.unary_.IComplement<INontrans, ICyclic>
		
	{}
}

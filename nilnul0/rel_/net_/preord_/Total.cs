﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.preord_
{
	/// <summary>
	/// reflex, trans, total
	/// </summary>
	public interface ITotal:
		net_.IPreord
		,
		net_.ITotal
	{

	}
}

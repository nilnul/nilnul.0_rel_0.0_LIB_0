﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// eg:
	///		{ a->b, b->c }
	/// </summary>
	public interface INontrans1duced : rel_.net_.INontrans, rel_.net_.IShortcutless
	{
	}
}
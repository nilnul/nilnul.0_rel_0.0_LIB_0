﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.nonreflex_
{
	/// <summary>
	/// eg:
	///		a->b
	/// </summary>
	public interface IAcyclic
		:
		INonreflex
		,
		nilnul._obj.typ.child.calc_.unary_.IComplement<INonreflex, ICyclic>
		,
		net_.IAcyclic4all
	{}
}

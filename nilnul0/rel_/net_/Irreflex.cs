﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_
{
	/// <summary>
	/// none of the cos is reflexive
	/// </summary>
	/// <remarks>
	/// alias:
	///		reflex4none
	/// </remarks>
	public interface IIrreflex:INet
	{
	}
}

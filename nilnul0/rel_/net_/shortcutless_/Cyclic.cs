﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net_.shortcutless_
{
	/// <summary>
	/// can still be cyclic.
	///	eg:
	///		a->a, this cannot be reduced (as it will not restore by transitivity closure )
	///		a->b, b->a. This cannot be reduced.
	///		a->b, b->c, c->a. This cannot be reduced.
	/// </summary>
	class Cyclic
	{
	}
}

﻿namespace nilnul.rel_
{
	/// <summary>
	/// category the rel according to its mate.
	/// </summary>
	public interface IByMate:IRel { }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.op_
{
	public interface IUnary<T>
		:nilnul.obj.op_.UnaryI<
			nilnul.rel_.Net1<T>
		>
		,
		nilnul.rel_.net.OpI<T>
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.op_.unary_
{
	/// <summary>
	/// the origin co is called adjacency. By transitive closureness, the resulted co is called path.
	/// </summary>
	/// <remarks>
	/// alias:
	///		transitivity closure.
	///		chain
	///		path
	///		
	/// </remarks>
	public interface IChain
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.net.convert_
{
	/// <summary>
	/// if two nodes have a two-way edge (a--> and b-->a), then we regard the two nodes as one and merge them. 
	/// We carry out this operation recursively till no two nodes have a two-way edge.
	/// 
	/// </summary>
	/// <remarks>
	///		This is infact a class/cluster operation. The resultant net 's nodes are clusters.
	/// </remarks>
	public class MergeMutuals
	{
	}
}

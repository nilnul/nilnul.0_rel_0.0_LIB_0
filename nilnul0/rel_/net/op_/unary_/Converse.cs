﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.op_.unary_
{
	static public class _ConverseX
	{

		static public rel_.Net1<T> Converse<T>(this nilnul.rel_.Net1<T>  operand) {

			return new Net1<T>(
				operand.field
				,
				_net.match.op_.unary_._ConverseX.Converse(operand.mate)
			);
		}
	}
}

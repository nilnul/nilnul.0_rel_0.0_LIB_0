﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.op_.binary_
{
	static public class _UnionX
	{
		static public void UnionWith<T>(
			rel_.Net1<T> net
			,
			IEnumerable<(T,T) > edges
			,
			IEnumerable<T> someOfField
		) {

			net.addRange(edges, someOfField);

		}
		static public rel_.Net1<T> Union<T>(
			rel_.Net1<T> net
			,
			IEnumerable<(T,T) > edges
			,
			IEnumerable<T> someOfField
		) {

			var r= new rel_.Net1<T>(net.elEq,net.mate,net.widows);

			r.addRange(edges, someOfField);
			return r;


		}


	}
}

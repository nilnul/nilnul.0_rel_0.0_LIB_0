﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.step.str_
{
	/// <summary>
	///  every neighboring edge pair is <see cref="nameof(step.co.be_.Chained))"/>
	///		The path can be empty.
	/// </summary>
	public interface IChain
	{
	}
}

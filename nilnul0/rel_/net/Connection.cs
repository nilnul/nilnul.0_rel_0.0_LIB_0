﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net
{
	/// <summary>
	/// a str of verges .
	///		for each consecutive pair of nodes, there is a co, or a conversed co connecting the two.
	/// </summary>
	/// <remarks>
	/// take the net as undirected
	/// This is not a str of nodes, but a str of steps. So a single node is not a connection; empty nodes is a connection.
	/// </remarks>
	public interface IConnection
		
	{

	}
}

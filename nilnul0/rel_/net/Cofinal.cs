﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.net
{
	public class Cofinal
	{
		static public bool Eval<T,TEq>(
			_net.NetI_EqDefault<T,TEq> net, Set_eqDefault<T,TEq> set
		)
			where TEq :IEqualityComparer<T>,new()
		{
			return net.nodes.IsSupersetOf(set) && net.nodes.All(
				v=>set.Any(
					s=>
					net.edges.contains(v, s)
				)
			);

		}
	}
}

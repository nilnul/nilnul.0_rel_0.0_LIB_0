﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net
{
	public class Tagged<T, TTag>
		: nilnul.obj.Box_pub<
			nilnul.rel_.Net1<T>
			>
	{

		private _tagged.Tags<T, TTag> _tags;
		public _tagged.Tags<T, TTag> tags
		{
			get { return _tags; }
			set { _tags = value; }
		}

		/// <summary>
		/// tags will use the elEq of net
		/// </summary>
		/// <param name="val"></param>
		/// <param name="tags"></param>
		public Tagged(
			Net1<T> val
			,
			_tagged.Tags<T, TTag> tags
		)
			:
			base(val


				)
		{
			if (val.elEq == tags.Comparer)
			{
				_tags = tags;
			}
			else
			{
				///make the two elEq the same.
				_tags = new _tagged.Tags<T, TTag>(

					tags
					,
					val.elEq
				);

			}
		}
		/// <summary>
		/// tags will use the elEq of net
		/// </summary>
		/// <param name="val"></param>
		/// <param name="tags"></param>
		public Tagged(
			Net1<T> val
			,
			IEnumerable<((T,T),TTag)> tags
		)
			:
			base(val


				)
		{
			
				///make the two elEq the same.
				_tags = new _tagged.Tags<T, TTag>(

					tags
					,
					val.elEq
				);

			
		}
		public Tagged(
			IEqualityComparer<T> eq
		):this(
			new Net1<T>(
				eq
			)
			,
			new _tagged.Tags<T, TTag>(eq)
		)
		{

		}
	}
}

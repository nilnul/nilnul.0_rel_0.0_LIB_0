﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.node.re_
{
	/// <summary>
	/// if there is a connection connecting the two node. from one point to another; or vice versan. Note: the direction of each step is insignificant.
	/// </summary>
	public interface IConnected
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.node.re_.connected_.nonself_
{
	/// <summary>
	/// connected by one step. the direction of the step is insignificant.
	/// </summary>
	/// <remarks>
	/// alias:
	///		adjacent
	///		edge
	///		arc
	/// </remarks>
	public class Direct
	{
	}
}

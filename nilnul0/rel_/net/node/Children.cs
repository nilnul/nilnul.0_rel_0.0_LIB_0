﻿using nilnul._net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nilnul.duo.be;

namespace nilnul.rel.net.node
{
	/// <summary>
	/// the direct next node.
	/// </summary>
	static public class _ChildrenX
	{


		static public Set_eqDefault<T, TEq> Children<T, TEq>(this NetI_EqDefault<T, TEq> net, T parent)
		where TEq : IEqualityComparer<T>, new()

		{
			//IEqualityComparer<T> Eq = SingletonByDefault<TEq>.Instance;
			return nilnul.rel.RangeX.Range<T, TEq>(
					net.edges.Where(edge => edge.StartWith<T, TEq>(parent))
			);
		}

	}
}

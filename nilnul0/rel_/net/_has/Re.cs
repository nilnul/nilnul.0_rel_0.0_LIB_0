﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net._has
{
	public interface IRe : rel._has.IRe { }
	public interface IRe<T> : rel._has.IRe<T, T>,IRe { }
	public interface ReI<T>: nilnul.rel._has.ReI<T,T>,IRe<T>
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.net._traverse
{

	[Serializable]
	public class CyclicException : Exception
	{
		public CyclicException() { }
		public CyclicException(string message) : base(message) { }
		public CyclicException(string message, Exception inner) : base(message, inner) { }
		protected CyclicException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
	
}

﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.vow
{
	[Obsolete()]
	public class Ee<T>
		: nilnul.obj.vow.Ee1<rel_.Net<T>>
	{
		public Ee(Net<T> val, VowI2<Net<T>> vow) : base(val, vow)
		{
		}
	}
}

﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.vow.ee_
{
	public class VowDefault1<T, TVow> :
		Ee1<T>
		where TVow: VowI2<Net1<T>> ,new()
	{
		public VowDefault1(Net1<T> val) : base(val, nilnul.obj_.Singleton<TVow>.Instance)
		{
		}
	}
}

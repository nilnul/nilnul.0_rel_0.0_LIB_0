﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.net
{
	/// <summary>
	/// topologically sort the objs of the net
	/// </summary>
	static public class _TraverseX
	{
		static public IEnumerable<T> Traverse<T>(nilnul.Net<T> net) {
			return Traverse(net.objs.toArr().ToList(), net.duos.toArr().ToList(), net.objs.elementEq);
		}

		static public IEnumerable<T> Traverse<T>(List<T> objs, List<Tuple<T, T>> duos, IEqualityComparer<T> objEq)
		{
			var roots = nilnul.net._RootsX.Roots(objs, duos, objEq);

			var r = new List<T>();


			while (roots.Any())
			{

				//remove root

				objs.RemoveAll(x => roots.Contains(x, objEq));

				duos.RemoveAll(
					duo => roots.Any(
						root => objEq.Equals(duo.Item1, root)
					)
				);

				r.AddRange(roots);
				roots = nilnul.net._RootsX.Roots(objs, duos, objEq);

			}

			if (objs.Any())
			{
				throw new _traverse.CyclicException( new nilnul.Net<T>(objs, duos,objEq).ToString() );
			}

			return r;

		}
	}
}

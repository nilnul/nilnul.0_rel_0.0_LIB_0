﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.be
{
	[Obsolete()]
	public class Vow<T> : nilnul.obj.be.Vow4<nilnul.rel_.Net<T>>
		,
		rel_.net.VowI<T>
	{
		public Vow(nilnul.obj.BeI1<Net<T>> be) : base(be)
		{
		}
	}
}

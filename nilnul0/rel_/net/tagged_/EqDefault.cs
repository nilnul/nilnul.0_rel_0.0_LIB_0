﻿using nilnul.rel_.net._tagged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.tagged_
{
	public class EqDefault<TEl, TTag, TElEq>
		: Tagged<TEl, TTag>
		where TElEq : IEqualityComparer<TEl>, new()
	{
		static public IEqualityComparer<TEl> ElEq = nilnul.obj_.Singleton<TElEq>.Instance;
		public EqDefault(net_.EqDefault1<TEl,TElEq> val, _tagged.tags_.NodeEqDefault<TEl, TTag, TElEq> tags)
			:
			base(
				val

				,

				tags
			)
		{
		}

		public EqDefault(
			IEnumerable<TEl> elements
			,
			IEnumerable<(TEl,TEl)> edges
			,
			IEnumerable<((TEl,TEl),TTag)> tags
		):this(
			new net_.EqDefault1<TEl, TElEq>(
				elements,edges
			)
			,
			new _tagged.tags_.NodeEqDefault<TEl, TTag, TElEq>(tags)
		)
		{

		}

		public EqDefault():
			base(
				ElEq
		)
		{

		}
	}

	
}

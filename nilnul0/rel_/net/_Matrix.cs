﻿using nilnul.collection.matrix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net
{
	/// <summary>
	/// get a bool.matrix_.square
	/// </summary>
	/// 
	 static public  class _MatrixX
	{
		/// <summary>
		/// get the relation matrix
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="relation"></param>
		/// <returns></returns>
		static public bool[,] Matrix<T>(
			nilnul.rel_.Net1<T> relation	
		) {
			//get the field.

			var field = relation.field;

			var fieldCount=field.Count();

			var matrix=new bool[fieldCount,fieldCount];

			for (int i = 0; i < fieldCount; i++)
			{
				for (int j = 0; j < fieldCount; j++)
				{
					matrix[i,j]=relation.mate.has(field.ElementAt(i),field.ElementAt(j));
					
				}
				
			}
			return matrix;
			//throw new NotImplementedException();
		}


					




	}
}

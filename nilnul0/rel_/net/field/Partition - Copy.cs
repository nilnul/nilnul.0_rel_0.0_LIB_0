﻿using nilnul.collection.set;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.net
{
	/// <summary>
	/// partiton the net into components, each of which is connected
	/// </summary>
	///
	[Obsolete(nameof(rel_.net.field._PartitionX))]
	static public class _PartitionX
	{

		static public nilnul.set.Family<T, TEq> Partition<T, TEq>(
			nilnul.Net<T, TEq> net
			
		)
		where TEq : IEqualityComparer<T>, new()
		{
			var equivalencePartition = new nilnul.set.Family<T,TEq>(
				net.objs.toArr().Select(
					c => new nilnul.obj.Set<T, TEq>(new[] { c })
				)
			);

			foreach (var pair in net.duos.toArr())
			{
				var set = nilnul.set.family.be_.disjoint.vow.En<T, TEq>.ContainerOf(equivalencePartition, pair.Item1);

				var setSecond = nilnul.set.family.be_.disjoint.vow.En<T, TEq>.ContainerOf(equivalencePartition, pair.Item2);

				if (!set.SetEquals(setSecond))
				{
					//merge the two sets.
					equivalencePartition.Remove(setSecond);
					set.UnionWith(setSecond);
				}
			}

			return equivalencePartition;

		}




	}
}

﻿using nilnul.collection.set;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel_.net.field
{
	/// <summary>
	/// partiton the net into components, each of which is connected
	/// </summary>
	static public class _PartitionX
	{
		static public nilnul.set.Family<T> Partition<T>(
			this nilnul.rel_.Net<T> net

		)
		{
			var equivalencePartition = new nilnul.set.Family<T>(
net.match.objEq,
				net.field.Select(
					c => new nilnul.obj.Set1<T>(net.match.objEq, new[] { c })
				)
			);

			foreach (var pair in net.match)
			{

				/// nodes partition that cntains given pair's item1.
				var set = nilnul.set.family.be_.disjoint.vow._EeX.ContainerOf(
					net.match.objEq
					,
					equivalencePartition,
					pair.component
				);

				var setSecond = nilnul.set.family.be_.disjoint.vow._EeX.ContainerOf(
					net.match.objEq
					,

					equivalencePartition, pair.component1
				);

				if (!
					nilnul.obj.set._eq_._OfSeqX.Eq(set,setSecond, net.match.objEq)
					//set.SetEquals(setSecond)
					)
				{
					//merge the two sets.
					equivalencePartition.Remove(setSecond);

					set.UnionWith(setSecond);
				}
			}
			return equivalencePartition;
		}
		static public nilnul.set.family_.EqDefaulted<T, TEq> Partition<T, TEq>(
			this nilnul.rel_.net_.EqDefaulted<T, TEq> net

		)
		where TEq : IEqualityComparer<T>, new()
		{
			var equivalencePartition = new nilnul.set.family_.EqDefaulted<T, TEq>(
				net.field.Select(
					c => new nilnul.obj.set_.EqDefaulted<T, TEq>(new[] { c })
				)
			);

			foreach (var pair in net.match)
			{

				/// nodes partition that cntains given pair's item1.
				var set = nilnul.set.family.be_.disjoint.vow._EeX.ContainerOf<T,TEq>(
					equivalencePartition, pair.component
				);

				var setSecond = nilnul.set.family.be_.disjoint.vow._EeX.ContainerOf<T,TEq>(
					equivalencePartition, pair.component1
				);

				if (!set.SetEquals(setSecond))
				{
					//merge the two sets.
					equivalencePartition.Remove(setSecond);
					set.UnionWith(setSecond);
				}
			}
			return equivalencePartition;
		}
	}
}

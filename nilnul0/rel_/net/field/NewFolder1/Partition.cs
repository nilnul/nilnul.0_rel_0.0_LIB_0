﻿using nilnul.collection.set;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel.net.op
{
	[Obsolete()]
	public partial class Partition
	{
		

		static public nilnul.collection.set.s.Set<T, TEq> PartitionOfVortex<T, TEq>(Net_eqDefault<T, TEq> net)
		where TEq : IEqualityComparer<T>, new()
		{



			//var equivalencePartition = new collection.set.s.Disjoint<T,TEq>.Noun( new  nilnul.collection.set.s.Set<T,TEq>(
			//	relation.field().Select(c => new Set<T,TEq>(new[] { c }))
			//));
			var equivalencePartition = new nilnul.collection.set.s.Set<T, TEq>(
				net.nodes.Select(c => new nilnul.collection.set.Set<T, TEq>(new[] { c }))
			);

			foreach (var pair in net.edges)
			{
				var set = nilnul.collection.set.s.Disjoint<T, TEq>.Noun.ContainerOf(equivalencePartition, pair.Item1);
				var setSecond = nilnul.collection.set.s.Disjoint<T, TEq>.Noun.ContainerOf(equivalencePartition, pair.Item2);

				if (!set.SetEquals(setSecond))
				{
					//merge the two sets.
					equivalencePartition.Remove(setSecond);
					set.UnionWith(setSecond);
				}
			}

			return equivalencePartition;

		}




	}
}

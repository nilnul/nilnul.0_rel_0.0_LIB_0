﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net
{
	/// <summary>
	/// there is a co in the net connecting the two points.
	/// </summary>
	/// <remarks>
	/// alias:
	///		edge
	///		arc
	///		grad
	///		step
	///		hop
	///		charge
	///		link
	///		carry
	///		stride
	///		adjacency
	///			conflicting with <seealso cref="nameof(be_.Adjacent)"/>
	/// </remarks>
	public interface StepI<T>: nilnul.obj.CoI2<T>
	{
	}
}

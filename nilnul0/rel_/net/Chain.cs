﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net
{
	/// <summary>
	/// for a net,
	///		a str of steps,
	///		when scrolled with buffer sized two:
	///			that the end of prev duo is the begin of the next duo.
	/// The path can be empty
	/// </summary>
	/// <remarks>
	/// alias:
	///		path
	///		chain
	///		walk
	///		trail
	///	Note: this is a str of steps, not a str of nodes. So a single node is not chain, but empty nodes is a chain.
	/// </remarks>
	public interface IChain
		:step.str_.IChain
	{

	}
}

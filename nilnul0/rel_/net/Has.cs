﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_._net
{
	public interface IHas : _rel.IHas { }
	public interface IHas<T> : _rel.IHas<T, T>,IHas { }
	public interface HasI<T>: nilnul._rel.HasI<T,T>,IHas<T>
	{
	}
}

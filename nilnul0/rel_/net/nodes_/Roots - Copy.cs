﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.net
{
	[Obsolete(nameof(rel_.net._StartersX))]
	static public class _RootsX
	{
		static public IEnumerable<T> Roots<T>(nilnul.NetI<T> net) {
			return net.objs.toArr().Where(
				obj => !net.duos.toArr().Any(
					duo => net.objs.elementEq.Equals(duo.Item2, obj)
				)

			).ToList();

		}
		static public IEnumerable<T> Roots<T>(List<T> objs, List<Tuple<T, T>> duos, IEqualityComparer<T> objEq) {
			return  objs.Where(
			   obj => duos.Any(
				   duo => objEq.Equals(duo.Item2, obj)
			   )

		   );
		}


	}
}

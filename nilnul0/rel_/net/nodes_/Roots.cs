﻿//extern alias obj;

using nilnul.obj;
using /*obj::*/nilnul.obj.seq;
using /*obj::*/nilnul.obj.seq.be_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net
{
	/// <summary>
	/// in-degree is zero
	/// </summary>
	static public class _StartersX
	{
		
		static public IEnumerable<T> KeepStarters<T>(
			List<T> nodes2filter
			,
			List<Tuple<T, T>> match,
			IEqualityComparer<T> objEq
			) {
			return  nodes2filter.Where(
			   obj => match.None(
				   edge => objEq.Equals( obj, edge.Item2)
			   )

		   );
		}

		static public IEnumerable<T> KeepStarters<T>(List<T> objs, List<(T, T)> duos, IEqualityComparer<T> objEq) {
			return  objs.Where(
			   obj => duos.None(
				   duo => objEq.Equals( obj, duo.Item2)
			   )

		   );
		}

		public static IEnumerable<T> KeepStarters<T>(HashSet<T> objs, List<(T, T)> duos, IEqualityComparer<T> objEq)
		{
			return  objs.Where(
			   obj => duos.None(
				   duo => objEq.Equals( obj, duo.Item2)
			   )

		   );

		}

		internal static IEnumerable<T>KeepStarters<T>(IEnumerable<T> objs, IEnumerable<CoI2<T>> duos, IEqualityComparer<T> objEq)
		{
			return  objs.Where(
			   obj => duos.None(
				   duo => objEq.Equals( obj, duo.component1)
			   )

		   );


		}
	}
}

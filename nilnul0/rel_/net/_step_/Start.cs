﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net._step_
{
	public interface StartI<T>
	{
		T start { get; }
	}
}

﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net._tagged
{
	public class Tags<TNode, TTag>
		:

Dictionary<(TNode, TNode), TTag>


	{

		public Tags(IDictionary<(TNode, TNode), TTag> dict) : base(dict)
		{
		}

		public Tags(Dictionary<(TNode, TNode), TTag> dict) : base(dict)
		{
		}




		public Tags(IDictionary<(TNode, TNode), TTag> dict, IEqualityComparer<TNode> equalityComparer) : this(
			new Dictionary<(TNode, TNode), TTag>(new nilnul.obj.co.Eq<TNode>(equalityComparer)) { }
		)
		{
			dict.Each(
				kv => this.Add(kv.Key, kv.Value)
			);
		}

		//public  dict
		//	= new obj.Dict<(TNode, TNode), string, obj.co.eq_.Defaulted<TNode, TNodeEq>>();
		public Tags(IEqualityComparer<TNode> equalityComparer) : this(
			new Dictionary<(TNode, TNode), TTag>(
				new nilnul.obj.co.Eq<TNode>(equalityComparer)
			)
		)
		{

		}

		public Tags(Tags<TNode, TTag> tags, IEqualityComparer<TNode> elEq)
			: this((Dictionary<(TNode, TNode), TTag>)tags, elEq)
		{
		}

		public Tags(
			IEnumerable<((TNode, TNode), TTag)> tags
			,
			IEqualityComparer<TNode> elEq
		) : base(

			new nilnul.obj.co.Eq<TNode>(elEq)


		)
		{
			tags.Each(
				x => {
					if (!base.ContainsKey(x.Item1))
					{
						base.Add(x.Item1, x.Item2);
					}
				}
			);
		}

		public Tags(IEqualityComparer<TNode> instance, IEnumerable<((TNode, TNode), TTag)> tags):this(tags,instance)
		{
		}

		public Tags(
			IEqualityComparer<TNode> instance
			,
			IEnumerable<KeyValuePair<(TNode, TNode), TTag>> tags
		):this(instance, tags.Select(kv=> (kv.Key,kv.Value)))
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="arc"></param>
		/// <returns> default if no such key</returns>
		public TTag getTag((TNode, TNode) arc)
		{
			if (this.ContainsKey(arc))
			{
				return this[arc];
			}
			return default(TTag);
		}
	}
}

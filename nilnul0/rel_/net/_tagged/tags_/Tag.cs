﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net._tagged.tags_
{
	public class NodeEqDefault<TNode, TTag, TNodeEq>
		:
		Tags<TNode,TTag>

		where TNodeEq : IEqualityComparer<TNode>, new()
	{
		//public  dict
		//	= new obj.Dict<(TNode, TNode), string, obj.co.eq_.Defaulted<TNode, TNodeEq>>();
		public NodeEqDefault():base( nilnul.obj_.Singleton<TNodeEq>.Instance)
		{
		}

		public NodeEqDefault(
			IEnumerable<((TNode,TNode),TTag) > tags
		):base(nilnul.obj_.Singleton<TNodeEq>.Instance,tags)
		{

		}

		public NodeEqDefault(
			IEnumerable<KeyValuePair<(TNode,TNode),TTag> > tags
		):base(nilnul.obj_.Singleton<TNodeEq>.Instance,tags)
		{

		}

		

		
	}
}

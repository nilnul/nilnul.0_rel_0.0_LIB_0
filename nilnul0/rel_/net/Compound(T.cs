﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nilnul.obj;
using nilnul.rel_._net;

namespace nilnul.rel_.net
{
	/// <summary>
	/// multiple arcs, with different tag,  exist between node.Co. We can regard this as a str of rel compounded together.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TTag"></typeparam>
	public class Compound<T,TTag> : _compound_.BlankI
		,_compound_.MatchI<T,TTag>
		,
		_rel_.SrcInClassI<T>
		,
		_rel_.DstInClassI<T>
		,
		_rel_.FieldInClassI<T>
	{
		/// <summary>
		/// assume eq is same; 
		/// </summary>
		/// <param name="nodes"></param>
		/// <param name="duos"></param>
		private Compound(
			
			 nilnul.obj.Set1<T> nodes
			,
			_compound.Match<T,TTag> duos
		)
		{

			_widows = nodes;
			_match  = duos;
			pak();
		}

		public Compound(obj.co_.tagged.Eq<T, TTag> objEq, IEnumerable<T> objs, IEnumerable< nilnul.obj.co_.Tagged<T,TTag> > duos )
			:this(
				new nilnul.obj.Set1<T>(objEq.boxed.Item1,objs) 
				 ,
				new _compound.Match<T,TTag>(
					objEq,
					duos
				)
			)
		{
			
		}

		public Compound(obj.co_.tagged.Eq<T, TTag> objEq, List<T> objs, List<(T, T,TTag)> duos )
			:this(
				objEq 
				 ,
				objs,

				duos.Select(x=> new obj.co_.Tagged<T, TTag>(x.Item1,x.Item2,x.Item3 ))
			)
		{
			
		}

		public void pak() {
			_widows.ExceptWith(
				_match.field
				
			);
		}



		private _compound.Match<T,TTag> _match;
		public _compound.Match<T,TTag> match => _match;
		

		public Set1<T> _widows;
		public Set1<T> widows
		{
			get
			{
				return _widows;
				//throw new NotImplementedException();
			}
		}
		public Set1<T> field => new Set1<T>( this._match.coTaggedEq.boxed.Item1,  _widows.Concat(_match.field) );

		public Set1<T> src => field;

		public Set1<T> dst => field;


		public override string ToString()
		{
			return $"{_widows}+{_match}";
		}
	}
}

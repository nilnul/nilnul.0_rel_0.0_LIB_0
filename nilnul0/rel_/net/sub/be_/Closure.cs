﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.sub.be_
{
	/// <summary>
	/// all related are included. those not included are not related by any node in the subnet.
	/// </summary>
	/// <remarks>
	/// alias:
	///		exhausted
	///			exhaustive
	///		maximal
	///		closure
	/// </remarks>
	interface IClosure
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nilnul.set.rel;

namespace nilnul.rel.net.duo.be
{
	static public class _DisjointX
	{
		static public bool Disjoint<T,TEq>(
			_net.NetI_EqDefault<T,TEq> net
			,
			_net.NetI_EqDefault<T,TEq> net1

			)
			where TEq:IEqualityComparer<T>,new()
		{

			return nilnul.set.co._DisjointX.Disjoint(net.nodes, net1.nodes);

		}
	}
}

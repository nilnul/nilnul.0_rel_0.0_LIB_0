﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net
{
	/// <summary>
	/// a subnet is a net that:
	///		the nodes of which are the subset of the nodes of the parent net
	///		the mate of which are the subset of the mate of the parent net.
	/// </summary>
	interface ISub
	{
	}
}

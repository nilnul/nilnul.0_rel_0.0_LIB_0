﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net
{
	/// <summary>
	/// any operation that returns net, including operation that takes input of type that is not net
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface OpI<T>: nilnul.obj.OpI3<nilnul.rel_.Net1<T>>
	{
	}
}

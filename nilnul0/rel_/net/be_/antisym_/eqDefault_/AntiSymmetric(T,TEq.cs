﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.relation.be
{

	[Obsolete()]

	public class AntiSymmetric<T,TEq>
		: nilnul.bit.Be<Relation2<T,TEq>>
		where TEq :IEqualityComparer<T>,new()
	{
		static public bool Eval(
			Relation2<T,TEq> relation
			)
		{

			foreach (var item in relation)
			{
				if (nilnul.relation.pair.Reflexive.Not(item,relation.eq))
				{
					if (relation.Contains(item.toInverse()))
					{
						return false;
						
					}
					
				}

			}

			return true;
			throw new NotImplementedException();


		}
		public AntiSymmetric()
			: base(Eval)
		{

		}

	}
}

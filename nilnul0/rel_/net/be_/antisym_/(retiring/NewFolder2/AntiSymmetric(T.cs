﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel.be_
{
	//extern alias obj;

	[Obsolete()]

	public class AntiSymmetric<T>
		:
		//obj::
		nilnul.obj.be_.FroFunc<nilnul.Rel1<T>>
	{
		static public bool Be(
			nilnul.Rel1<T> relation
			)
		{

			foreach (var item in relation)
			{
				if ( nilnul.obj.duo.be_.Reflexive.Not(item,relation.elementEq))
				{
					if (
						relation.Contains(
							nilnul.obj.duo.convert_.Converse.Eval<T>( item)
						)
					)
					{
						return false;
						
					}
					
				}

			}

			return true;
			//throw new NotImplementedException();


		}
		public AntiSymmetric()
			: base(Be)
		{

		}

	}
}

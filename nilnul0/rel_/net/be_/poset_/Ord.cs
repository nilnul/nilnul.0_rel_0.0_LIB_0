﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel.be_
{
	//extern alias obj;
	public class Ord
	{
		static public bool Be<T>(nilnul.Rel1<T> relation)
		{
			return Preorder.Be(relation) && nilnul.rel.be_.AntiSymmetric<T>.Be(relation);
		}


	}
	public class Ord<T>
		: nilnul.rel._be_.OfRelClass<T>
	{
		public bool be(nilnul.Rel1<T> relation)
		{
			return Ord.Be<T>(relation);
		}




		static public readonly Ord<T> Singleton = SingletonByDefault<Ord<T>>.Instance;

		public class En : nilnul.obj.be.En_beDefaultable<nilnul.Rel1<T>, Ord<T>>
		{
			public En(Rel1<T> val) : base(val)
			{

			}
		}

	}
}

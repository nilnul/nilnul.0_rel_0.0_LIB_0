﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel.be_
{
	//extern alias obj;
	/// <summary>
	/// 预序
	/// </summary>
	///
	[Obsolete(nameof(nilnul.rel_.net.be_._PreordX))]
	public class Preorder
	{

		static public bool Be<T>(nilnul.Rel1<T> relation)
		{
			return Reflexive.Be(relation) && Transitive.Be(relation);
		}


	}
	[Obsolete(nameof(nilnul.rel_.net.be_._PreordX))]
	public class Preorder<T>
		: nilnul.rel._be_.OfRelClass<T>
	{
		public bool be(nilnul.Rel1<T> relation)
		{
			return Reflexive.Be(relation) && Transitive.Be(relation);
		}




		static public readonly Preorder<T> Singleton = SingletonByDefault<Preorder<T>>.Instance;

		public class En : nilnul.obj.be.En_beDefaultable<nilnul.Rel1<T>, Preorder<T>>
		{
			public En(Rel1<T> val) : base(val)
			{

			}
		}

	}
}

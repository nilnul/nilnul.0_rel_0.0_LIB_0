﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.be_
{
	/// <summary>
	/// Formally, an adjacency relation is any relation which is irreflexive and symmetric.
	///The set  of edges of a loopless graph, being a set of unordered pairs of elements of, constitutes an adjacency relation on.
	/// </summary>
	/// <remarks>
	/// defined as Interface, it can be implemented using different methods such that :
	///		1) more efficient
	///		2) case by case
	/// </remarks>
	public interface IAdjacent
		:
		irreflex_.ISym
	{
		

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using nilnul._rel_;

namespace nilnul.rel_.net.be_
{
	/// <summary>
	/// reflexive iff for every a∈ A,aRa
	/// </summary>
	static public class _ReflexiveX
		
	{
		static public bool Be<T>(
			nilnul.rel_.Net<T> relation
		)
		{

			foreach (var item in relation.field)
			{
				if (
					relation.match.hasNot(item,item)

					
				)
				{
					return false;

				}

			}
			return true;
		}
	}

	public class Reflex<T>
		:
		nilnul.rel_.net.BeI1<T>
	{
		public bool be(Net1<T> relation)
		{

			foreach (var item in relation.field)
			{
				if (
					relation.mate.hasNot(item, item)

				)
				{
					return false;

				}

			}
			return true;


		}


		static public Reflex<T> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<Reflex<T>>.Instance;
			}
		}

	}
	
}
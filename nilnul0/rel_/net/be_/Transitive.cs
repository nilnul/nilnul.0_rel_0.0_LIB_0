﻿using nilnul.relation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.be_
{
	static public  class _TransitiveX
	{

		 static public bool Be<T>(Relation2<T> relation)
		 {

			 var matrix = nilnul.relation.op.unary.Matrix.Eval(relation);
			 return Be(matrix);


		 }


		 static public bool Be(bool[,] _relationMatrix) {

			 var length = _relationMatrix.GetLength(0);

			 for (int row = 0; row < length; row++)
			 {
				 for (int col = 0; col < length; col++)
				 {

					if (nilnul.bit.combine_.Gt.Eval(

						_relationMatrix[row, col]
						,
						nilnul.bit.str.duo_.sameLen.be_.Imply._Eval_twoStrSameLen(
							nilnul.matrix.Extensions.Row(_relationMatrix, col),
							nilnul.matrix.Extensions.Row(_relationMatrix, row)
						)
					))
					{
						return false;

					}

				 }

			 }
			 return true;
		

		 
		 
		 }

		static public bool Be(Matrix rm) {

			return Be(rm.val);

		
		
		}

		static public bool Be<T>(nilnul.rel_.Net<T> relation)
		{

			var matrix = Matrix.GetRelationMatrix(relation);
			return Be(matrix);


		}


		public static bool Be<T>(Net1<T> relation)
		{
			var matrix = net._MatrixX.Matrix(relation);
			return Be(matrix);

			//throw new NotImplementedException();
		}
	}
	public class Trans<T> : net.BeI1<T>
	{
		public bool be(Net1<T> obj)
		{
			return _TransitiveX.Be(obj);
			//throw new NotImplementedException();
		}


		static public Trans<T> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<Trans<T>>.Instance;
			}
		}

	}

}

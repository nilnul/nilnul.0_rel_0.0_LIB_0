﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be_
{
	/// <summary>
	/// directed and acyclic. 
	/// because it's acyclic, so the directed here means end-directed (if you draw arrow rightward, it's right-directed); so we have a unique destination while the source may be many.
	/// C# subtyping is DAG and the end -type is object.
	/// </summary>
	public class Dag<T> : BeI1<T>
	{
		/// <summary>
		/// acyclic and directed. 
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public bool be(RelI1<T> obj)
		{
			throw new NotImplementedException();
		}
	}
}

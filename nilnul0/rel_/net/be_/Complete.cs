﻿//extern alias obj;

using /*obj::*/nilnul.obj.str.be_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.be_
{
	/// <summary>
	/// </summary>
	public class Complete<T>
		:nilnul.rel_.net.BeI1<T>
	{
		/// <summary>
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public bool be(nilnul.rel_.Net1<T> obj)
			
		{
			
			return nilnul.set._PermutateX.Permutate1(obj.fieldAsClass, 2).All(
				p2Instance=>obj.mate.Contains(
					new nilnul.obj.Co<T>(
						p2Instance.ee.First()
						,
						p2Instance.ee.Last()
					)
					
				)
			);


		}


		static public Complete<T> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<Complete<T>>.Instance;
			}
		}




	}

}

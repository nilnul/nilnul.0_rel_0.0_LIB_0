using System;
using System.Collections.Generic;
using System.Linq;

using nilnul.relation;


namespace nilnul.func
{


	/// <summary>
	/// Input n(n=0,1,2,...) objects
	/// Output 1 objects. Sometimes output no objects;this is a special case and we can regard it as out a special object named void.
	/// Function has a method call, which return the result.
	/// </summary>
	public class Func<T>
		: nilnul.relation.Relation2<T>
	{

		static public bool Be_byMatrix(nilnul.relation.Relation2<T> relation)
		{
			var matrix = nilnul.relation.op.unary.Matrix.Eval(relation);
			var rows = nilnul.collection.matrix.MatrixX.Rows(matrix);
			foreach (var row in rows)
			{
				if (row.Sum(c => 1) > 1)
				{
					return false;
				};

			}
			return true;


			throw new NotImplementedException();
		}

		static public bool Be_byEnumDomain(
			Relation2<T> relation
		)
		{
			foreach (var item in relation.domain())
			{
				if (relation.Select(c => relation.eq.Equals(c.second, item)).Count() > 1)
				{
					return false;
				}
			}
			return true;
			throw new NotImplementedException();
		}
		public Func(IEqualityComparer<T> elementEqComparer)
			:base(elementEqComparer)
		{

		}
		public Func(IEnumerable<Pair2<T>> pairs, IEqualityComparer<T> elementEqComparer)
			:base(pairs,elementEqComparer)
		{

		}
	}
}

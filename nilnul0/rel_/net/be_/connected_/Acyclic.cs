﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.be_.connected_
{
	public class Acyclic<T> : nilnul.rel_.net.BeI<T>
	{
		public bool be(Net<T> obj)
		{
			return Connected<T>.Singleton.be(obj) &&
				net.be_.Acyclic<T>.Singleton.be(obj)
				;
		}

		static public Acyclic<T> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<Acyclic<T>>.Instance;
			}
		}

	}
}

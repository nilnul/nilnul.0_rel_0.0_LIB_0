﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.be_
{
	/// <summary>
	/// connected and acyclic. 
	/// because it's acyclic, so the directed here means end-directed (if you draw arrow rightward, it's right-directed); so we have a unique destination while the source may be many.
	/// C# subtyping is DAG and the end -type is object.
	/// </summary>
	/// <remarks>
	/// Dag is not necessaryly Ord; Dag is reduced Ord.
	/// </remarks>
	public class Dag<TEl>
		:
		rel_.net.BeI<TEl>
	{
		public bool be(Net<TEl> obj)
		{
			return connected_.Acyclic<TEl>.Singleton.be(obj);
			throw new NotImplementedException();
		}

		static public Dag<TEl> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<Dag<TEl>>.Instance;
			}
		}

	}

	
}

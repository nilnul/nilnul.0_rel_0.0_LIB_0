﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.net.be_
{
	/// <summary>
	/// directed and acyclic. 
	/// because it's acyclic, so the directed here means end-directed (if you draw arrow rightward, it's right-directed); so we have a unique destination while the source may be many.
	/// C# subtyping is DAG and the end -type is object.
	/// </summary>
	///
	[Obsolete(nameof(rel_.net.be_.Connected_eqDefaulted<T,TEq>))]
	public class Connected<T,TEq> 
		where TEq:IEqualityComparer<T>,new()
	{
		/// <summary>
		/// acyclic and directed. 
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public bool be(Net<T,TEq> obj)
			
		{
			return nilnul.net._PartitionX.Partition(obj).Count <= 1;

			//throw new NotImplementedException();
		}

		static public Connected<T,TEq> Singleton
		{
			get
			{
				return nilnul.obj.SingletonByDefault<Connected<T,TEq>>.Instance;
			}
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.be_
{
	/// <summary>
	/// for any plural simple chain: <see cref="nameof(chain_.simple_.plural.be_.Shortcutless)"/>, there is no one-step chain connecting from the start to the end.
	/// </summary>
	/// <remarks>
	/// Note:
	///		a cyclic net can also be shortcutless. eg: a->b->c->a
	///		a reflexive net can also be shortcutless. eg: a->a
	///	claim: if a net is shortcutless, then its reflexive closure is also shortcutless.
	///		proof:
	///		the simple chain from the reflexive closure is also a simple chain from the original net.
	///		'cuz the chain is simple, hence the endpoints are not reflexive; hence if there were direct edge in the closure, then there will be such edge in the original; but the original has no such edge, resulting a contradiction.
	///		
	///claim: if a reflexive closure is shortcutless, then the original is shortcutless.
	///		proof:
	///		the simple chain from the original is also a simple chain from the closure.
	///		'cuz the chain is simple, hence the endpoints are not reflexive; hence if there were direct edge in the origin, then there will be such edge in the closure; but the closure has no such edge, resulting a contradiction.
	///
	/// Theorem: node reflexive or not wouldnot affect the shortcutlessness.
	///		
	/// </remarks>
	class Shortcutless
	{
	}
}

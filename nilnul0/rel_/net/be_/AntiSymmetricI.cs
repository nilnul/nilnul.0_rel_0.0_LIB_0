﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel_.net.be_
{
	/// <summary>
	/// definition: for any x,y, if xRy and yRx,then x=y. 
	/// 
	/// 
	/// </summary>
	/// <remarks>
	/// alias:antisym
	/// </remarks>
	public  interface AntiSymmetricI<T> :
		nilnul.rel_.net.BeI1<T>
	{
	}
}

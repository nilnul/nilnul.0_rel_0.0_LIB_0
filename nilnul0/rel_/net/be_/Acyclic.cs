﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.be_
{
	/// <summary>
	/// no chain is circle.
	/// directed and acyclic. 
	/// because it's acyclic, so the directed here means end-directed (if you draw arrow rightward, it's right-directed); so we have a unique destination while the source may be many.
	/// C# subtyping is DAG and the end -type is object.
	/// </summary>
	public class Acyclic<T> : nilnul.rel_.net.BeI<T>
		,
		nilnul.rel_.net.BeI1<T>
	{
		/// <summary>
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public bool be(nilnul.rel_.Net<T> obj)
		{

			return acyclic_.ByLoop<T>.Singleton.be(obj);

		}

		public bool be(Net1<T> obj)
		{
			return acyclic_.ByLoop<T>.Singleton.be(obj);

		}

		static public Acyclic<T> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<Acyclic<T>>.Instance;
			}
		}

	}
}

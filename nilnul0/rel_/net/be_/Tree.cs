﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nilnul._net;
using nilnul;
using nilnul.str.be;
using nilnul.duo.be;
using nilnul.str;

namespace nilnul.rel_.net.be_
{
	/// <summary>
	/// acyclic and connected
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TEq"></typeparam>
	public class Tree<T, TEq> : nilnul.BeI<nilnul._net.NetI_EqDefault<T, TEq>>
		where TEq:IEqualityComparer<T>,new()
	{


		public bool be(NetI_EqDefault<T, TEq> obj)
		{

			var eq = SingletonByDefault<TEq>.Instance;

			var nodes = obj.nodes;
			var roots=nodes.Where(
				node=>obj.edges.None(
					edge=>
					edge.EndWith<T,TEq>(node)
				)
			);

			if (roots.Count()!=1)
			{
				return false;
			}


			var visiting = new Queue<T>();
			visiting.Enqueue(roots.First());

			var visited = new Set_eqDefault<T, TEq>();

			while (visiting.Count>0)
			{
				var current = visiting.Dequeue();

				if (visited.Contains(current))
				{
					return false;
				}
				else
				{
					visited.Add(current);
				}


				foreach (
					var item in nilnul.rel.net.node._ChildrenX.Children(obj,current)
				)
				{
						visiting.Enqueue(item);
				}
			}

			if (
				nodes.Any(node=> visited.NotContain_eqDefault<T,TEq>(node))
			)
			{
				return false;


			}

			return true;
			

		


			throw new NotImplementedException();
		}
	}
}

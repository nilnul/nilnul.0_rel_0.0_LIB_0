﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.be_._shortcutless
{
	/// <summary>
	/// given plural n, eg: 4.
	///		for any simple chain of le <var>n</var> edges, there is no shortcut.
	///	eg:
	///		net: a->b,b->c, c->d, a->d
	///			it's <see cref="nameof(IntransitiveIfLePlural)"/>(2), but no <see cref="nameof(IntransitiveIfLePlural)"/>(3)
	/// </summary>
	class IntransitiveIfLePlural
	{
	}
}

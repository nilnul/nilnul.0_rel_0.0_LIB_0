﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.relation.be
{
	public partial class StrictOrder
	{
		public bool Be<T,TEq>(Relation2<T,TEq> relation)
			where TEq :IEqualityComparer<T>,new()
		{
			return Irreflexive<T,TEq>.Be(relation) &&Transitive_AntiSymmetric.Be(relation);
		}
	}
}

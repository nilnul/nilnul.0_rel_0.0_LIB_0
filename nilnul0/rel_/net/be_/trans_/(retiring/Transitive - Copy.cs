﻿using nilnul.relation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be_
{
	[Obsolete(nameof(nilnul.rel_.net.be_._TransitiveX))]
	 public  class Transitive
	{

		 static public bool Be<T>(nilnul.Rel1<T> relation)
		 {

			 var matrix = RelMatrix.GetRelationMatrix(relation);
			 return Be(matrix);

			 throw new NotImplementedException();

		 }
		 static public bool Be<T>(Relation2<T> relation)
		 {

			 var matrix = nilnul.relation.op.unary.Matrix.Eval(relation);
			 return Be(matrix);

			 throw new NotImplementedException();

		 }


		 static public bool Be(bool[,] _relationMatrix) {

			 var length = _relationMatrix.GetLength(0);

			 for (int row = 0; row < length; row++)
			 {
				 for (int col = 0; col < length; col++)
				 {

					if (nilnul.bit.combine_.Gt.Eval(

						_relationMatrix[row, col]
						,
						nilnul.bit.str.duo_.sameLen.be_.Imply._Eval_twoStrSameLen(
							nilnul.matrix.Extensions.Row(_relationMatrix, col),
							nilnul.matrix.Extensions.Row(_relationMatrix, row)
						)
					))
					{
						return false;

					}

				 }

			 }
			 return true;
		

		 
		 
		 }

		static public bool Be(RelMatrix rm) {

			return Be(rm.val);

		
		
		}
	}
}

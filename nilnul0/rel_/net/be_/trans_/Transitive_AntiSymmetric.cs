﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.relation.be
{
	[Obsolete()]
	public class Transitive_AntiSymmetric
	{
		static public bool Be<T,TEq>
			(Relation2<T,TEq> relation) 
			where TEq :IEqualityComparer<T>,new()
		
			{
 
			return Transitive.Be(relation) && AntiSymmetric<T,TEq>.Eval(relation);
		
		}
	}
}

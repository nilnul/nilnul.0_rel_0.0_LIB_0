﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.be_
{
	/// <summary>
	/// </summary>
	public class Connected<T>
		:nilnul.rel_.net.BeI<T>
	{
		/// <summary>
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public bool be(nilnul.rel_.Net<T> obj)
			
		{
			return _ConnectedX.IsConnected(obj);

		}


		static public Connected<T> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<Connected<T>>.Instance;
			}
		}




	}
	/// <summary>
	/// </summary>
	public class Connected_eqDefaulted<T,TEq>
		:Connected<T>

		where TEq:IEqualityComparer<T>,new()
	{
		/// <summary>
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public bool be(nilnul.rel_.net_.EqDefaulted<T,TEq> obj)
			
		{
			return base.be(obj);

		}

		static public Connected_eqDefaulted<T,TEq> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<Connected_eqDefaulted<T,TEq>>.Instance;
			}
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.be_.acyclic.vow
{
	public class Ee<TEl>
		: nilnul.rel_.net.vow.ee_.VowDefault1<TEl, acyclic.Vow<TEl>>
	{
		public Ee(Net1<TEl> val) : base(val)
		{
		}
	}
}

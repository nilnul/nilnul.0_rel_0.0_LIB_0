﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.net.be_.acyclic_
{
	/// <summary>
	/// directed and acyclic. 
	/// because it's acyclic, so the directed here means end-directed (if you draw arrow rightward, it's right-directed); so we have a unique destination while the source may be many.
	/// C# subtyping is DAG and the end -type is object.
	/// </summary>
	public class ByRecur<T> : nilnul.net.BeI<T>
	{
		/// <summary>
		/// </summary>
		/// <param name="net"></param>
		/// <returns></returns>
		public bool be(NetI<T> net)
		{

			return _be(
				net.objs.toArr().ToList(),
				net.duos.toArr().ToList(),
				net.objs.elementEq
			);


			throw new NotImplementedException();
		}

		public bool be(nilnul.Net<T> net) {
			return _be(
				net.objs.toArr().ToList(),
				net.duos.toArr().ToList(),
				net.objs.elementEq
			);
		

		}

		private bool _be(List<T> objs, List<Tuple<T,T>> duos, IEqualityComparer<T> objEq ) {
			if (objs.Count==0)
			{
				return true;
			}


			//get the objs that ingree is zero.

			var  roots = nilnul.net._RootsX.Roots(objs, duos, objEq);

			//remove root

			objs.RemoveAll(x=> roots.Contains(x, objEq) );

			duos.RemoveAll(
				duo =>roots.Any( 
					r=> objEq.Equals( duo.Item1, r)  
				)
			);

			return _be(objs,duos, objEq);
		

		}



		static public ByRecur<T> Singleton
		{
			get
			{
				return nilnul.obj.SingletonByDefault<ByRecur<T>>.Instance;
			}
		}

	}
}

﻿//extern alias obj;

using nilnul.obj;
using/* obj::*/nilnul.obj.seq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.be_.acyclic_
{
	/// <summary>
	/// empty is Acyclic
	/// </summary>
	public class ByLoop<T> : nilnul.rel_.net.BeI<T>
	{
		private bool _be(List<T> field, List<(T, T)> edges, IEqualityComparer<T> objEq)
		{
			

			var starters = nilnul.rel_.net._StartersX.KeepStarters(field,edges,objEq);


			while (starters.Any())
			{

				//remove root
				field.RemoveAll(x => starters.Contains(x, objEq));

				/// remove edges of root 
				edges.RemoveAll(
					edge => starters.Any(
						r => objEq.Equals(edge.Item1, r)
					)
				);

				starters = nilnul.rel_.net._StartersX.KeepStarters(field, edges, objEq);

			}
			return edges.None();

		}

		private bool _be(SetI2<T> field, IEnumerable<CoI2<T>> edges, IEqualityComparer<T> objEq)
		{

			return _be(field.ToList(),
				edges.Select(x=> (x.component,x.component1)).ToList()
				, objEq
			);
		
		}
		private bool _be(HashSet<T> nodes, List<(T, T)> edges, IEqualityComparer<T> objEq)
		{
			

			var starters = nilnul.rel_.net._StartersX.KeepStarters(nodes,edges,objEq);


			while (starters.Any())
			{

				//remove root
				nodes.ExceptWith(starters);

				/// remove edges of root 
				edges.RemoveAll(
					edge => starters.Any(
						r => objEq.Equals( r, edge.Item1)
					)
				);

				starters = nilnul.rel_.net._StartersX.KeepStarters(nodes, edges, objEq);

			}
			return edges.None();

		}
		private bool _be(Set1<T> nodes, List<CoI2<T>> edges, IEqualityComparer<T> objEq)
		{

			///get all minimums
			var starters = nilnul.rel_.net._StartersX.KeepStarters(nodes,edges,objEq)
				.ToArray();	//decouple from nodes, which will be changed in enumeration of starters.


			while (starters.Any())
			{

				//remove minimus
				nodes.ExceptWith(starters);

				/// remove edges of root 
				edges.RemoveAll(
					edge => starters.Any(
						starter => objEq.Equals( starter, edge.component)
					)
				);

				starters = nilnul.rel_.net._StartersX.KeepStarters(nodes, edges, objEq).ToArray();

			}
			return edges.None();

		}

		/// <summary>
		/// </summary>
		/// <param name="net"></param>
		/// <returns></returns>
		public bool be(nilnul.rel_.Net<T> net)
		{
			return _be(
				net.field,
				net.match.ToList(),
				net.match.objEq
			);
			//throw new NotImplementedException();
		}

		public bool be(nilnul.rel_.Net1<T> net)
		{
			return _be(
				net.field,
				net.mate.ToList(),
				net.mate.elEq
			);
			//throw new NotImplementedException();
		}

		

		static public ByLoop<T> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<ByLoop<T>>.Instance;
			}
		}

	}
}

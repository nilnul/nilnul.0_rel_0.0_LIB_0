﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.be_
{
	/// <summary>
	/// has node
	/// </summary>
	public class Dwelt<T> : nilnul.rel_.net.BeI<T>
	{
		/// <summary>
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public bool be(nilnul.rel_.Net<T> obj)
		{
			return obj.field.Any();
		}

		static public Dwelt<T> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<Dwelt<T>>.Instance;
			}
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.be_.poset.vow
{
	public class Ee<T>
		: nilnul.rel_.net.vow.ee_.VowDefault1<
			T,
			Vow<T>
		>
	{
		public Ee(Net1<T> val) : base(val)
		{
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.be_
{
	static public class _UnconnectedX
	{

		static public bool Be<T,TEq>(nilnul.rel_.net_.EqDefaulted<T,TEq> net)
		where TEq:IEqualityComparer<T>,new()

		{

			return !_ConnectedX.IsConnected(net);

		}

	}

}

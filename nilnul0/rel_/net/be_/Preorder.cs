﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel_.net.be_
{
	public interface IPreord
		:be_.reflex_.ITrans
	{ }
	//extern alias obj;
	/// <summary>
	/// 预序
	/// </summary>
	static public class _PreordX
	{

		static public bool Be<T>(nilnul.rel_.Net<T> relation)
		{
			/// may be circular
			return _ReflexiveX.Be(relation) && _TransitiveX.Be(relation);
		}
	}
	public class Preord<T>
		: nilnul.rel_.net.Be<T>
		,
		nilnul.rel_.net.BeI1<T>
	{


		public Preord() : base(_PreordX.Be)
		{
		}

		public bool be(Net1<T> obj)
		{
			return Reflex<T>.Singleton.be(obj) && Trans<T>.Singleton.be(obj);

		}


		static public Preord<T> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<Preord<T>>.Instance;
			}
		}

	}
}

﻿using nilnul.net.be._tarjan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace nilnul.rel.net.be._cyclic
{


	public class Tarjan<T, TEq> : nilnul.BeI<nilnul.rel.Net_eqDefault<T, TEq>>
		where TEq:IEqualityComparer<T>,new()
	{
		static public Tarjan<T, TEq> Singleton = SingletonByDefault<Tarjan<T, TEq>>.Instance;

		static public TEq ElementEq = SingletonByDefault<TEq>.Instance;
		public bool be(nilnul.rel.Net_eqDefault<T, TEq> obj)
		{
			return detectCycle(obj).All(a => a.IsAcyclic);
			throw new NotImplementedException();
		}

		private StronglyConnectedComponentList<T> stronglyConnectedComponents;
		private Stack<Vertex<T>> stack;
		private int index;


		public StronglyConnectedComponentList<T> detectCycle(nilnul.rel.Net_eqDefault<T, TEq> net) {
			List<Vertex<T>> graph = new List<Vertex<T>>();

			foreach (var item in net.nodes)
			{
				graph.Add(
					new Vertex<T>(item)	
				);
			}

			setDependencies(graph, net);


			stronglyConnectedComponents = new StronglyConnectedComponentList<T>();
			index = 0;
			stack = new Stack<Vertex<T>>();

			foreach (var v in graph)
			{
				if (v.Index < 0)
				{
					StrongConnect(v);
				}
			}
			return stronglyConnectedComponents;






		}
		public void setDependencies(List<Vertex<T>> vs,Net_eqDefault<T,TEq> net) {

			foreach (var item in vs)
			{
				setDependencies(item, net,vs);




			}

		}

		public void setDependencies(Vertex<T> v,Net_eqDefault<T,TEq> net,List<Vertex<T>> vs) {

			v.Dependencies=net.edges.Where(a => ElementEq.Equals( v.Value,a.Item1))
				.SelectMany(
						b=>  vs.Where(c=> ElementEq.Equals(c.Value,  b.Item2)
					)
				)
				.ToList()
			;

		}


		/// <summary>
		/// Calculates the sets of strongly connected vertices.
		/// </summary>
		/// <param name="graph">Graph to detect cycles within.</param>
		/// <returns>Set of strongly connected components (sets of vertices)</returns>
		public StronglyConnectedComponentList<T> detectCycle(IEnumerable<Vertex<T>> graph)
		{
			stronglyConnectedComponents = new StronglyConnectedComponentList<T>();
			index = 0;
			stack = new Stack<Vertex<T>>();
			foreach (var v in graph)
			{
				if (v.Index < 0)
				{
					StrongConnect(v);
				}
			}
			return stronglyConnectedComponents;
		}



		private void StrongConnect(Vertex<T> v)
		{
			v.Index = index;
			v.LowLink = index;
			index++;
			stack.Push(v);

			foreach (Vertex<T> w in v.Dependencies)
			{
				if (w.Index < 0)
				{
					StrongConnect(w);
					v.LowLink = Math.Min(v.LowLink, w.LowLink);
				}
				else if (stack.Contains(w))
				{
					v.LowLink = Math.Min(v.LowLink, w.Index);
				}
			}

			if (v.LowLink == v.Index)
			{
				var scc = new StronglyConnectedComponent<T>();
				Vertex<T> w;
				do
				{
					w = stack.Pop();
					scc.Add(w);
				} while (v != w);
				stronglyConnectedComponents.Add(scc);
			}

		}


	}
}

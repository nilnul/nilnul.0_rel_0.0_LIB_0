﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.net.be._tarjan
{
    public class Vertex<T>
    {

		internal int Index { get; set; }

		internal int LowLink { get; set; }

		public T Value { get; set; }

		public ICollection<Vertex<T>> Dependencies { get; set; }



		public Vertex()
        {
            this.Index = -1;
            this.Dependencies = new List<Vertex<T>>();
        }

        public Vertex(T value)
            : this()
        {
            this.Value = value;
        }

        public Vertex(IEnumerable<Vertex<T>> dependencies)
        {
            this.Index = -1;
            this.Dependencies = dependencies.ToList();
        }



        public Vertex(T value, IEnumerable<Vertex<T>> dependencies)
            : this(dependencies)
        {
            this.Value = value;
        }

    }
}

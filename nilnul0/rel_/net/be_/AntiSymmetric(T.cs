﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel_.net.be_
{

	public class AntiSymmetric<T>
		: nilnul.rel_.net.BeI1<T>
	{
		public bool be(Net1<T> relation)
		{
			var coEq = new nilnul.obj.co.be_.Reflex<T>(relation.elEq);

			foreach (var item in relation.mate)
			{
				if (
					!coEq.be(item)

				)
				{

				
					if (relation.mate.has( item.component1,item.component ))
					{
						return false;
						
					}
					
				}

			}

			return true;


		}


		static public AntiSymmetric<T> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<AntiSymmetric<T>>.Instance;
			}
		}

	}
}

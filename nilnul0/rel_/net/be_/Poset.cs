﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel_.net.be_
{
	/// <summary>
	/// reflexive, antisymmetric, transitive.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class Poset<T>
		: nilnul.rel_.net.BeI1<T>
		,
		be_.preord_.IAntisymmetric
	{
		public bool be(Net1<T> obj)
		{
			return Preord<T>.Singleton.be(obj) && AntiSymmetric<T>.Singleton.be(obj);

		}


		static public Poset<T> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<Poset<T>>.Instance;
			}
		}

	}
}

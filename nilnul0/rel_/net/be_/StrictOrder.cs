﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel_.net.be_
{
	/// <summary>
	/// irreflexive, antisymmetric, transitive
	/// </summary>
	/// <remarks>
	/// alias:
	///		trans1asm
	/// </remarks>
	public interface IStrictOrder:be_.trans_.IAsym
	{
		
	}
}

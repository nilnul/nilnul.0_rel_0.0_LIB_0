﻿using nilnul.collection.set;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nilnul.rel_.net.be_
{
	/// <summary>
	/// partiton the net into components, all of them are connected.
	/// Note: empty net is defined as connected
	/// </summary>
	static public class _ConnectedX
	{
		static public bool IsConnected<T>(
			this nilnul.rel_.Net<T> net
		)
		{
			if (net.field.Any())
			{
				var firstNode = net.field.First();
				var set = new HashSet<T>(net.match.objEq) { firstNode };
				///find all nodes connected
				///
				///increase the set
				///


				var connected = net.field.Where(
					node => set.Any(
						current => net.match.has(current, node)
						||
						net.match.has(node, current)
					)
				);

				while (connected.Any())
				{
					set.AddRange(connected);

					connected = net.field.Where(
										node => set.Any(
											current => net.match.has(current, node)
											||
											net.match.has(node, current)
										)
									);
				}
				if (net.field.Any(n=> !set.Contains(n) ))
				{
					return false;
				}
				return true;
			}
			else
			{
				return true;
			}

		
		}
		[Obsolete("use parentType instead",true)]
		static private bool IsConnected<T, TEq>(
			this nilnul.rel_.net_.EqDefaulted<T, TEq> net

		)
		where TEq : IEqualityComparer<T>, new()
		{
			return IsConnected((rel_.Net<T>)net);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.be_
{
	/// <summary>
	/// for a plural simple chain: <see cref="nameof(chain_.simple_.plural)"/>, there is no one-step chain connecting from the start to the end.
	/// </summary>
	/// <remarks>
	/// for all plural simple chain,
	///		if there exists one chain that is shortcutsome, then the net is shortcutsome.
	///		if there is no chain that is shortcutsome, then the net is shortcutless.
	///	So <see cref="nameof(Shortcutsome)"/> is dual to <see cref="nameof(Shortcutless)"/>
	/// </remarks>
	class Shortcutsome
	{
	}
}

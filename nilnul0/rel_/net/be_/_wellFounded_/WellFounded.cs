﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using nilnul.collection.set;
using nilnul._net;

namespace nilnul.net.be
{

	/// <summary>
	/// 
	/// </summary>
	public class WellFounded
	{

		/// <summary>
		/// a collection of dags are WellFounded.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TEq"></typeparam>
		/// <param name="rel"></param>
		/// <returns></returns>

		static public bool Eval<T,TEq>(nilnul.Rel1<T,TEq> rel)
			where TEq : IEqualityComparer<T>

		{
			return Eval(
				new nilnul._net.Net<T,TEq>(rel)
			);

			throw new NotImplementedException();


		}

		static public bool Eval<T,TEq>(nilnul._net.Net<T,TEq> net)
			where TEq : IEqualityComparer<T>
		{
			return
				nilnul.set.family.ctor_._PowX.ToHashSets<T>(	net.nodes)
				.Where(a=>a.Count>0)
				.All(
					subset=> subset.Any(
						minimal=> subset.All(
							x=> net.edges.notContains(x,minimal)   
						) 
					)
				);

				


			throw new NotImplementedException();


		}

	}

	public class WellFounded<T, TEq, TSet, TDuo, TRel,TNet>
		: nilnul.BeI<TNet>

		where TEq : IEqualityComparer<T>,new()
		where TSet:nilnul.Set_eqDefault<T,TEq>
		where TDuo : Tuple<T, T>
		where TRel : nilnul.Rel_elementEqDefault<T,TEq,TDuo>
		where TNet:nilnul.rel.Net_eqDefault<T,TEq,TSet,TDuo,TRel>
	{

		static public readonly WellFounded<T, TEq, TSet, TDuo, TRel, TNet> Singleton = SingletonByDefault<WellFounded<T, TEq, TSet, TDuo, TRel, TNet>>.Instance;



		public bool be(TNet obj)
		{
			


			return nilnul.set.family.ctor_._PowX.ToHashSets<T>(	obj.nodes)
				.Where(a=>a.Count>0)
				.All(
					subset=> subset.Any(
						minimal=> subset.All(
							x=> obj.edges.notContains(x,minimal)   
						) 
					)
				);

				

			


			throw new NotImplementedException();
		}



		
	}
}
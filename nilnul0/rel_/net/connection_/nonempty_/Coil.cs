﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net.connection_.nonempty_
{
	/// <summary>
	/// the start point and the end point of the connection is the same.
	/// </summary>
	/// <remarks>
	/// a cirle formed by connection.
	/// 
	/// 卷。a cirle or a 回流
	/// 
	/// alias:
	///		coil
	///			there might be some recoil
	///		confluent
	///		convoluted
	/// </remarks>
	/// 
	public interface ICoil
	{
	}
}

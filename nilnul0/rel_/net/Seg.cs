﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net
{
	/// <summary>
	/// a pair of node, where there exists a step from start to end, or from end to start. The direciton of the step is in significant.
	/// </summary>
	/// <remarks>
	/// alias:
	///		seg
	///		neighbor
	///		pair
	///		span
	///		edge
	/// </remarks>
	public interface ISeg
	{
	}
}

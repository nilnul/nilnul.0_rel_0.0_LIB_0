﻿using nilnul.obj;
using nilnul.obj.co_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.net._compound
{
	public class Match<TNode,TTag>
		: nilnul.obj.Set1<nilnul.obj.co_.Tagged<TNode,TTag>>
	{
		private obj.co_.tagged.Eq<TNode,TTag> _coTaggedEq;

		public obj.co_.tagged.Eq<TNode, TTag> coTaggedEq
		{
			get { return _coTaggedEq; }
			set { _coTaggedEq = value; }
		}

		public nilnul.obj.Set1<TNode> field
		{
			get
			{
				return new Set1<TNode>(
					_coTaggedEq.boxed.Item1
					,
					domain.Concat(range)
				);
			}
		}

		public nilnul.obj.Set1<TNode> domain
		{
			get
			{
				return new Set1<TNode>(
					_coTaggedEq.boxed.Item1
					,
					this.Select(x => x.component)
				);

			}
		}

		public nilnul.obj.Set1<TNode> range
		{
			get
			{
				return new Set1<TNode>(
					_coTaggedEq.boxed.Item1
					,
					this.Select(x => x.component1)
				);

			}
		}


	

	


		public Match(obj.co_.tagged.Eq<TNode, TTag> eq, IEnumerable<obj.co_.Tagged<TNode,TTag>> vars) : base((eq), vars)
		{
			this._coTaggedEq = eq;
		}

		public Match(obj.co_.tagged.Eq<TNode, TTag> eq) : this(eq, new obj.co_.Tagged<TNode,TTag>[0])
		{
		}

		public Match(IEqualityComparer<TNode> eq, IEqualityComparer<TTag> tagEq) : this(
			new obj.co_.tagged.Eq<TNode,TTag>(eq, tagEq)
		)
		{

		}

		public Match(IEqualityComparer<TNode> elementEq, IEqualityComparer<TTag> tagEq,  IEnumerable<obj.co_.Tagged<TNode, TTag>> vars)
			:
			this(
				new obj.co_.tagged.Eq<TNode, TTag>(elementEq,tagEq)
				,
				vars
			)
		{
		}

		
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._net_
{
	[Obsolete(nameof(NodesI<T>))]
	public interface VerticesI<T>
	{
		nilnul.obj.SetI<T> vertices { get; }
	}
}

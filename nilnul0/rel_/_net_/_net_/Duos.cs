﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._net_
{
	public interface DuosI<T>
	{
		/// <summary>
		/// </summary>
		/// <returns></returns>
		/// <remarks>
		/// this justifies the existance of nilnul.Objs for
		///	1) this collection is not a set as we shall not have the elementEq (elementEq shall be derived from src.elementEq and dst.elementEq)
		///	2) this collection is not str as we don't need this to arrange sequentially.
		///	3) it's not necessariely a sortie as we don't need this to be set nor do we need this to be a str
		///	4) this is in fact a part ready to be assemblied to a set. there are something else meant to be part of a str, a sortie.
		/// </remarks>
		nilnul.ObjsI2<Tuple<T, T>> duos { get; }

	}

	
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._net_
{
	public interface NetI<T>

		:VerticesI<T>
		,EdgesI<T>
	{
	}
}

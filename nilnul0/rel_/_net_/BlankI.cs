﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_._net_
{
	/// <summary>
	/// the domain and the range are the same set; the elements of this set are of the same given type T
	/// </summary>
	public interface BlankI:nilnul._rel_.BlankI
	{
	}
	public interface BlankI<T> : BlankI
	{

	}
}

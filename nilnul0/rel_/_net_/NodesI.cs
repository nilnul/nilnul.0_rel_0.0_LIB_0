﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_._net_
{
	/// <summary>
	/// another name for vertices
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface NodesI<T>
	{
		IEnumerable<T> nodes { get; }
	}
}

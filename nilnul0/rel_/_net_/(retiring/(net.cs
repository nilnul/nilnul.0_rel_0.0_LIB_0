﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._net
{
	[Obsolete()]
	public interface NodesI<TSet>
	{
		TSet nodes { get; }

	}


	[Obsolete()]
	public interface EdgesI<TRel> {
		TRel edges { get; }
	}


	/// <summary>
	/// a rel and some extra nodes that are in, or not in,  rel.
	/// 
	/// sounds good name indicating associating with set.
	/// </summary>
	[Obsolete()]
	public interface NetI {

	}

	[Obsolete()]
	public interface NetI<T> :NetI{

	}




	[Obsolete()]
	public interface NetI< TSet, TRel>
	:
		NodesI<TSet>
		,
		EdgesI<TRel>
		,NetI
		

	{
		

	}

	[Obsolete()]
	public interface NetI_EqDefault<T, TEq>
		:NodesI<Set_eqDefault<T,TEq>>, EdgesI<Rel_elementEqDefault_heritSameGeneric<T,TEq>>
		where TEq:IEqualityComparer<T>,new()
	{



	}

	[Obsolete()]
	public interface NetI1<T, TEq>
		:
		
		NodesI<Set_eqDefault<T,TEq>>
		, 
		EdgesI<Rel_elementEqDefault_heritSameGeneric<T,TEq>>

		where TEq:IEqualityComparer<T>,new()
	{


	}


	[Obsolete()]
	public class NetA<TSet,TRel>:NetI<TSet,TRel>
	{
		private TSet _nodes;

		public TSet nodes
		{
			get { return _nodes; }
			private set { _nodes = value; }
		}

		private TRel _edges;

		public TRel edges
		{
			get { return _edges; }
			private set { _edges = value; }
		}


		public NetA(TSet set, TRel rel)
		{

		}
	}


	[Obsolete()]
	public interface NetI<T, TEq, TSet, TRel>
		:NetI<TSet,TRel>,NetI<T>

		where TSet:nilnul.Set<T,TEq>
		where TRel:nilnul.Rel1<T,TEq>
		where TEq:IEqualityComparer<T>

	{


	}

	[Obsolete()]
	public class Net<T, TEq, TSet, TRel>
		: 
		NetA<TSet,TRel>
		,
		NetI<T, TEq, TSet, TRel>

		where TSet : nilnul.Set<T, TEq>
		where TRel : nilnul.Rel1<T, TEq>
		where TEq : IEqualityComparer<T>


	{
		

		public Net(TSet set, TRel rel):base(set, rel)
		{

			nilnul.Assert1.True( Object.ReferenceEquals( set.elementEq, rel.elementEq));



		}
	}



	[Obsolete()]
	public class Net<T,TEq>

		:Net<T,TEq, nilnul.Set<T,TEq>, nilnul.Rel1<T,TEq> >
		where TEq : IEqualityComparer<T>


	{

		public Net(Rel1<T, TEq> rel)
			:this(nilnul.rel.FieldX.Field<T,TEq>( rel), rel)
		{
			

		}

		public Net(
			nilnul.Set<T,TEq> set
			,
			nilnul.Rel1<T,TEq> rel
		):base(set, rel)
		{

		}

	}
}

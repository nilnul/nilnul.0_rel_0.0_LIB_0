﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_._net_
{
	public interface CouplesI<T>
	{
		/// <summary>
		/// when taken in by other callers, this is assume distinct
		/// </summary>
		IEnumerable<(T,T)>  couples { get; }
	}
}

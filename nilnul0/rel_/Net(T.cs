﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nilnul.obj;
using nilnul.rel_._net;
using nilnul.rel_._net_;

namespace nilnul.rel_
{
	/// <summary>
	///  a name for Rel{T,T}, unambigous even without typeParam
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface INet<T> :INet{

	}
	public interface NetI<T>
:
		INet<T>
		,
		_net_.FieldI<T>
		,
		_net_.CouplesI<T>
		
	{

	}
	public class Net1<T>
		:
		_net_.BlankI<T>
		, _net_.MateI<T>
		
		//,
		//_rel_.FieldInClassI<T>
		,
		NetI<T>
		,
		ClonableI<Net1<T>>
	{
		public IEqualityComparer<T> elEq { get {
				return _widows.memberEq;

				//return _mate.elEq;
			}
		}
		private _net.Mate<T> _mate;
		public Mate<T> mate => _mate;

		/// <summary>
		/// 
		/// </summary>
		///
		[Obsolete("this shall be field. as in some process such as delNodes, we delete some edges in match, but the nodes shall still be kept whileas the nodes are not in this 'window', but only in filed. using field will make computation robust, err-less-prone.")]

		public Set1<T> _widows;
		public Set1<T> widows
		{
			get
			{
				return _widows;
				//throw new NotImplementedException();
			}
		}

		public Net1(SetI2<T> field1, IEnumerable<(T, T)> ts)


		{
			this._widows = new Set1<T>(field1.memberEq, field1);
			this._mate = new Mate<T>(field1.memberEq, ts);
			this.leanWidow();

		}

		public Net1(SetI2<T> field1, IEnumerable<CoI2<T>> ts):this(
			field1,ts.Select(a=>(a.component,a.component1))
		)
		{
		}


		public Net1(_net.Mate<T> mate0, IEnumerable<T> extraNodes)
		{
			this._mate = mate0;
			this._widows = new Set1<T>(
				mate0.elEq
				,
				extraNodes.Except(mate0.field, mate0.elEq)
			);
		}

		public Net1(_net.Mate_heritRelMate<T> mate0, IEnumerable<T> extraNodes)
			:this( (_net.Mate<T>)mate0,extraNodes )
		{
			
		}
		/// <summary>
		/// </summary>
		/// <param name="nodes"></param>
		/// <param name="duos"></param>
		///
		[Obsolete("set eq maybe not the same")]
		private Net1(


			Mate<T> duos, nilnul.obj.Set1<T> nodes

		)
			:
			this(duos, nodes.AsEnumerable())        
		{


		}
		public Net1(_net.Mate<T> mate0):this(mate0, Enumerable.Empty<T>())
		{
			
		}

		public Net1(_net.Mate_heritRelMate<T> mate0):this(mate0, Enumerable.Empty<T>())
		{
			
		}

		public Net1(
			IEnumerable<T> objs
			,
			IEqualityComparer<T> eq,

			IEnumerable<(T, T)> duos



			)
			: this(
				 new Set2<T>(objs,eq)
				 ,
				 duos
			)
		{


		}

		public Net1(IEqualityComparer<T> eq, IEnumerable<Tuple<T, T>> duos, IEnumerable<T> objs)
			: this(
				 new Mate<T>(eq, duos)
				 ,
				 objs
			)
		{


		}

		public Net1(IEqualityComparer<T> eq, IEnumerable<nilnul.obj.CoI2<T>> duos, IEnumerable<T> objs)
			: this(
				 new Mate<T>(eq, duos)
				 ,
				 objs
			)

		{



		}
		public Net1(IEqualityComparer<T> eq, List<(T, T)> duos, List<T> objs)

			: this(
				 new Mate<T>(eq, duos)
				 ,
				 objs
			)
		{

		}

		public Net1(IEqualityComparer<T> eq, IEnumerable<(T, T)> duos, IEnumerable<T> objs)
			: this(
				 new Mate<T>(eq, duos)
				 ,
				 objs
			)

		{

		}

		public Net1(IEqualityComparer<T> eq, IEnumerable<T> objs, IEnumerable<(T, T)> duos)
			: this(
				 new Mate<T>(eq, duos)
				 ,
				 objs
			)

		{

		}

		public Net1(IEnumerable<T> objs, IEnumerable<(T, T)> duos )
			: this(
				EqualityComparer<T>.Default
				 ,
				 objs
				  ,duos
			)

		{


		}


		public Net1(IEqualityComparer<T> eq, List<Tuple<T, T>> duos, List<T> objs)
			: this(
				 new Mate<T>(eq, duos)
				 ,
				 objs
			)

		{

		}

		public Net1(IEqualityComparer<T> eq, IEnumerable<(T, T)> duos)
			: this(
				 new Mate<T>(eq, duos)
				 
			)

		{


		}
		public Net1(IEqualityComparer<T> eq, IEnumerable<T> duos)
			: this(
				eq,
				Enumerable.Empty<(T,T)>(),
				duos
				 
			)

		{


		}




		public Net1(IEqualityComparer<T> eq, IEnumerable<Tuple<T, T>> duos):this(eq,duos, Enumerable.Empty<T>())
		{
			
		}


		public Net1(IEqualityComparer<T> eq) : this(
			eq, new nilnul.obj.CoI2<T>[0], new T[0]
		)
		{

		}

		public Net1() : this(EqualityComparer<T>.Default)
		{

		}

		public Net1(Net1<T> acyclic):this(
			acyclic.elEq,
			acyclic.field,acyclic.couples
		)
		{
		}




		/// <summary>
		/// generate a new set
		/// </summary>
		public SetI2<T> field 
			{
			get {
				var matField = _mate.field1;
				matField.UnionWith(_widows);
				return matField;
			}
		}
		/// <summary>
		/// generate a new set
		/// </summary>
		public Set2<T> fieldAsClass 
			{
			get {
				var matField = _mate.field1;
				matField.UnionWith(_widows);
				return matField;
			}
		}


		public IEnumerable<T> nodes => field;


		public IEnumerable<(T, T)> couples => mate.Select(x=> (x.component,x.component1));


		public override string ToString()
		{
			return $"{_widows}+{_mate}";
		}
		public void add(T node)
		{
			if (!this.mate.field.Contains(node))
			{
				///true if added; false if already there
				_widows.Add(node);

			}
		}

		public void addRange(IEnumerable< T> nodes)
		{
			nodes.Each(
				n => add(n)
			);

		}


		public void add(T node, T node1)
		{
			_mate.add(node, node1);
			_widows.remove(
				new T[] { node,node1}
			);
		}

		public void add((T , T )node1)
		{
			add(node1.Item1, node1.Item2);
			
		}

		public void addRange(IEnumerable< (T,T) > nodes)
		{
			nodes.Each(
				n => add(n)
			);

		}

		public void leanWidow() {
			_widows.RemoveWhere(
				 this.mate.field.Contains
			);
		}

		public void addRange(IEnumerable< (T,T) > nodes, IEnumerable<T> individuals)
		{
			_mate.addRange(nodes);
			_widows.add(
				individuals
			);

			leanWidow();


		}
		public void delNodes(IEnumerable<T> nodes)
		{
			//make this a filed
			_widows.UnionWith(_mate.field1);
			_widows.remove(nodes);
			_mate.delNodes(nodes);


			_widows.ExceptWith(
				_mate.field1
			);
			/// note: this will remove related nodes; but these nodes shall be put in the field, for example, widow
			///

			//			_mate.delNodes(nodes);

		}

		public Net1<T> clone()
		{
			return new rel_.Net1<T>(elEq, mate, widows);

		}
	}
}

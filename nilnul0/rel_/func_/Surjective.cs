﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.func_
{
	/// <summary>
	/// monandry mate; total monandry rel.
	/// alias:
	///		dict = func_.surjective
	/// </summary>
	/// <typeparam name="TSrc"></typeparam>
	/// <typeparam name="TTgt"></typeparam>
	public class Surjective<TSrc,TTgt>
		:Dictionary<TSrc,TTgt>
	{

	}
}

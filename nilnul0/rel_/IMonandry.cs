﻿namespace nilnul.rel_
{
	/// <summary>
	/// the mate is <see cref="_rel.mate_.IMonandry"/>
	/// </summary>
	public interface IMonandry : rel_.IByMate { }
}

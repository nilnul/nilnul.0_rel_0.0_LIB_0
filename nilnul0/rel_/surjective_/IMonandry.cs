﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.surjective_
{
	/// <summary>
	/// the mate of this, a <see cref="ISurjective"/>, is <see cref="_rel.mate_.IMonandry"/>
	/// a rel that is both surjective and monandry.
	/// in otherwords, a key can have one value, or null. same value can correspond with various keys.
	/// eg:
	///		a dict, entry of which can have null value (but not null key).
	///			cuz for null-values keyVal, the key is regarded as a famale widow (having no male couterpart)
	///				note the difference with <seealso cref="nameof(_rel.mate_.IDict)"/> is whether value can be null
	/// </summary>
	/// alias:
	///		<see cref="Dictionary{TKey, TValue}"/>
	public interface IMonandry
		:
		rel_.ISurjective
		,
		rel_.IMonandry
	{
	}
}

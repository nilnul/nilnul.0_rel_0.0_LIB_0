﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_
{
	public interface IFunc
		:rel_.injective_.ISerial
	{ }
	/// <summary>
	/// monandry mate; total monandry rel. Note: there are no or some widowTgtS, so it's not the same as Dict that has no widowTgtS
	/// alias:
	///		monandry*total rel
	/// </summary>
	/// <typeparam name="TSrc"></typeparam>
	/// <typeparam name="TTgt"></typeparam>
	/// <remarks>
	/// monandry that is total
	/// </remarks>
	public class Func<TSrc, TTgt>
		: nilnul.rel.be.en_.BeDefaulted<TSrc, TTgt, nilnul.rel.be_.Func<TSrc, TTgt>>
	{
		public Func(Rel3<TSrc, TTgt> obj) : base(obj)
		{
		}
	}
}

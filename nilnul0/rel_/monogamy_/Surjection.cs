﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.monandry_.monogyny_
{
	/// <summary>
	/// keyWidows are appended. total or partial. This is also monogamy
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	/// <typeparam name="TVal"></typeparam>
	public class Surjection<TKey, TVal>
		: nilnul.obj.Box<_rel.mate_.monandry_.dict_.Bijective<TKey, TVal>>
	{
		private HashSet<TKey> _widows;

		public HashSet<TKey> widows
		{
			get { return _widows; }
		}


		public _rel.mate_.monandry_.dict_.Bijective<TKey, TVal> mate
		{
			get { return boxed; }
		}

		public Surjection(_rel.mate_.monandry_.dict_.Bijective<TKey, TVal> val, IEnumerable<TKey> extraKeys
			//
			)
			: base(val)
		{
			_widows = new HashSet<TKey>(
				extraKeys.Except(
					val.ee.Keys,
					val.ee.Comparer
				), val.ee.Comparer
			);
		}

		public Surjection(_rel.mate_.monandry_.dict_.Bijective<TKey, TVal> val):this(val, new TKey[0])
		{

		}
		public Surjection(Dictionary<TKey, TVal> val,  IEqualityComparer<TVal> valEq)
			:
			this(
				new _rel.mate_.monandry_.dict_.Bijective<TKey, TVal>(val,valEq)
			)
		{

		}


	}
}

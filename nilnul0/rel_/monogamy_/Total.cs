﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.monandry_.monogyny_
{
	/// <summary>
	/// no widow keys. but we might have widowValues.
	///
	/// <see cref="nameof(rel_.func_.Bijective)"/>
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	/// <typeparam name="TVal"></typeparam>
	public class Total<TKey, TVal>
		: nilnul.obj.Box<_rel.mate_.monandry_.dict_.Bijective<TKey, TVal>>
	{
		private HashSet<TVal> _widows;

		public HashSet<TVal> widows
		{
			get { return _widows; }
		}


		public _rel.mate_.monandry_.dict_.Bijective<TKey, TVal> mate
		{
			get { return boxed; }
		}

		public Total(_rel.mate_.monandry_.dict_.Bijective<TKey, TVal> val, IEnumerable<TVal> extraKeys
			//
			)
			: base(val)
		{
			_widows = new HashSet<TVal>(
				extraKeys.Except(
					val.ee.Values,
					val.valEq
				),
				val.valEq
			);
		}

		public Total(_rel.mate_.monandry_.dict_.Bijective<TKey, TVal> val):this(val, new TVal[0])
		{

		}
		public Total(Dictionary<TKey, TVal> val,  IEqualityComparer<TVal> valEq)
			:
			this(
				new _rel.mate_.monandry_.dict_.Bijective<TKey, TVal>(val,valEq)
			)
		{

		}


	}
}

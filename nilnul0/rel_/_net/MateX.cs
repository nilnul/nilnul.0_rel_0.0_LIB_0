﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_._net
{
	static public class _MateX
	{
		static public IEnumerable<T> Srcs<T>(
			IEnumerable<(T,T)> cos

		) {
			return cos.Select(co=>co.Item1);
		}

		static public HashSet<T> Domain<T>(
			Mate_heritRelMate<T> cos

		) {
			return new HashSet<T>( Srcs<T>( cos), cos.srcElEq);
		}


		static public IEnumerable<T> Tgts<T>(
			IEnumerable<(T,T)> cos

		) {
			return cos.Select(co=>co.Item2);
		}

		static public HashSet<T> Range<T>(
			Mate_heritRelMate<T> cos

		) {
			return new HashSet<T>( Tgts<T>( cos), cos.srcElEq);
		}
		static public IEnumerable<T> Objs<T>(
			IEnumerable<(T,T)> cos

		) {
			return Srcs(cos).Concat(Tgts(cos));
		}

	

		static public HashSet<T> Field<T>(
			Mate_heritRelMate<T> cos

		) {
			return new HashSet<T>( Objs<T>( cos), cos.srcElEq);
		}


	}
}

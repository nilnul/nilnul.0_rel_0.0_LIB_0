﻿//extern alias obj;

using nilnul.obj;
using/* obj::*/nilnul.obj.seq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_._net.mate
{
	/// <summary>
	/// </summary>
	static public class _StartersX
	{
		public static IEnumerable<T> Starters<T>( IEnumerable<(T, T)> edges, IEqualityComparer<T> objEq)
		{
			return  _MateX.Srcs(edges).Except(
				_MateX.Tgts(edges), objEq
			);



		}


	

	}
}

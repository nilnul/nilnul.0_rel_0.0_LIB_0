﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nilnul.obj;

namespace nilnul._rel._path.duos.be
{
	//extern alias obj;
	public class Connected<T>
		: /*obj::*/nilnul.obj.BeI1<IEnumerable<nilnul.obj.Duo<T>>>
	{
		private IEqualityComparer<T> _elementEq;

		public IEqualityComparer<T> elementEq
		{
			get { return _elementEq; }
			//set { _elementEq = value; }
		}


		public Connected(
			IEqualityComparer<T> elementEq
			
		)
		{
			_elementEq = elementEq;

		}
		public bool be(IEnumerable<nilnul.obj.Duo<T>> obj)
		{
			for (int i = 1; i < obj.Count(); i++)
			{
				if (
					!_elementEq.Equals( 
						obj.ElementAt(i).Item1
						,
						obj.ElementAt(i).Item2
					)
				)
				{
					return false;

				}

			}
			return true;
			//throw new NotImplementedException();
		}


		


		public class Aver
			:
			nilnul.obj.be.Aver<IEnumerable<nilnul.obj.Duo<T>>>
		{
			public Aver(IEqualityComparer<T> elementEq):base(
				new Connected<T>(elementEq)
			)
			{

			}

			
		}

		public class En
			:

			nilnul.obj.be.En<IEnumerable<nilnul.obj.Duo<T>>>
		{
			public En(IEqualityComparer<T> elementEq, IEnumerable<nilnul.obj.Duo<T>> val) : base(new Connected<T>(elementEq), val)
			{
			}
		}
	}
}

﻿//extern alias obj;

using nilnul.obj;
using/* obj::*/nilnul.obj.seq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_._net.mate.be_
{
	/// <summary>
	/// empty is Acyclic
	/// </summary>
	static public class _AcyclicX 
	{
		public static  bool Be<T>( List<(T, T)> edges, IEqualityComparer<T> objEq)
		{
			var starters = mate._StartersX.Starters(edges,objEq);

			while (starters.Any())
			{

				/// remove edges of root 
				edges.RemoveAll(
					edge => starters.Any(
						dangleTip => objEq.Equals(edge.Item1, dangleTip)
					)
				);

				starters = mate._StartersX.Starters(edges,objEq);
				;

			}
			return edges.None();

		}

		public static bool Be<T>(Mate_heritRelMate<T> edges)
		{
			return Be(
				edges.ToList()
				,
				edges.srcElEq
			);
		}


	

	}
}

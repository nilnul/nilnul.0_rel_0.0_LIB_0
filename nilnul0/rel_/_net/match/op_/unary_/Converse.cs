﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_._net.match.op_.unary_
{
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// alias:
	///  A relation may be represented by a logical matrix
	///  Then the converse relation is represented by its transpose matrix:
	/// </remarks>
	static public class _ConverseX
	{

		static public _net.Mate<T> Converse<T>(_net.Mate<T>  operand) {

			return new Mate<T>(
				operand.elEq
				,
				operand.Select(c=> (c.component1,c.component))
			);
		}
	}
}

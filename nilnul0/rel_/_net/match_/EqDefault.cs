﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_._net.mate_
{
	public class EqDefault<T, TEq>
		:
		_net.Mate_heritRelMate<T>
		where TEq : IEqualityComparer<T>, new()
	{


		public EqDefault() : base(nilnul.obj_.Singleton<TEq>.Instance)
		{
		}

		public EqDefault(

			IEnumerable<(T, T)> duos
		) :
		base(
nilnul.obj_.Singleton<TEq>.Instance

				, duos)
		{
		}

		public EqDefault(IEnumerable<Tuple<T, T>> duos) : base(

nilnul.obj_.Singleton<TEq>.Instance
			, duos)
		{
		}
	}
}

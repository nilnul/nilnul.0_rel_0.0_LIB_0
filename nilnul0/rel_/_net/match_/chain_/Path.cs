﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel
{
	/// <summary>
	/// a str of head to toe of duos
	/// </summary>
	///
	[Obsolete()]
	public class Path<T>
	{
		private IEnumerable<nilnul.obj.Duo<T>> _duos;

		public IEnumerable<nilnul.obj.Duo<T>> duos
		{
			get { return _duos; }
			//set { _duos = value; }
		}

		public Path()
		{

		}

		public int length { get { return _duos.Count(); } }

	}
}

﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_._net.match_
{
	public class EqDefaulted<T, TEq> : Match<T>
		where TEq : IEqualityComparer<T>, new()
	{
		public EqDefaulted(HashSet<CoI2<T>> hashSet) : base(nilnul.obj_.Singleton<TEq>.Instance, hashSet)
		{
		}

		public EqDefaulted() : base(
nilnul.obj_.Singleton<TEq>.Instance
			)
		{
		}

		

		public EqDefaulted(CoI2<T>[] vars ) : base( nilnul.obj_.Singleton<TEq>.Instance, vars)
		{
		}

	
		public EqDefaulted(IEnumerable<CoI2<T>> vars) : base(nilnul.obj_.Singleton<TEq>.Instance,vars )
		{
		}

		
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._net.duos
{
	static public class _DomainX
	{
		static public nilnul.obj.Set1<T> Domain<T>( this IEnumerable<Tuple<T,T>> duos, IEqualityComparer<T> objEq) {
			return new obj.Set1<T>(
				objEq
				,
				duos.Select(x=>x.Item1)
			);
		}

		static public IEnumerable<T> Domain<T>( this IEnumerable<Tuple<T,T>> duos 			) {
			return duos.Select(x=>x.Item1);
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._net.duos
{
	static public class _FieldX
	{
		static public nilnul.obj.Set1<T> Field<T>(this IEnumerable<Tuple<T,T>> duos, IEqualityComparer<T> objEq) {
			return new obj.Set1<T>(
				objEq
				,
				Field(duos)
				
			);
		}

		static public IEnumerable<T> Field<T>(this IEnumerable<Tuple<T,T>> duos) {
			return _DomainX.Domain(duos).Concat( _RangeX.Range(duos));
		}

	}
}

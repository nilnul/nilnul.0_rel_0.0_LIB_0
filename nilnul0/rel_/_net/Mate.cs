﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_._net
{
	/// <summary>
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class Mate1<T>
		: nilnul.obj.Set2<(T,T)>
	{
		private IEqualityComparer<T> _elEq;

		public IEqualityComparer<T> elEq
		{
			get { return _elEq; }
			set { _elEq = value; }
		}

		

		/// <summary>
		/// generate a new set
		/// </summary>
		public nilnul.obj.Set2<T> field
		{
			get
			{
				var domain = this.domain;
				domain.UnionWith(range);
				return domain;
				//return new Set1<T>(
				//	_objEq
				//	,
				//	domain.Concat(range)
				//);
			}
		}

		public nilnul.obj.Set2<T> domain
		{
			get
			{
				return new Set2<T>(
					this.Select(x => x.Item1)
					,
					_elEq
				);

			}
		}


		public nilnul.obj.Set2<T> range
		{
			get
			{
				return new Set2<T>(
					
					this.Select(x => x.Item2),
					_elEq
				);

			}
		}

		public Mate1(IEqualityComparer<T> objEq1, IEnumerable<(T, T)> duos):base(
			new nilnul.obj.co.Eq<T>(
				objEq1
			), duos
		)
		{
		}

		public Mate1(IEqualityComparer<T> eq, IEnumerable<CoI2<T>> vars) : this(eq, vars.Select(x=> (x.component,x.component1)))
		{
			_elEq = eq;
		}

		public Mate1(IEqualityComparer<T> eq, IEnumerable<Co<T>> vars) : this(eq, vars.Cast<CoI2<T>>())
		{
			_elEq = eq;
		}


		public Mate1(IEqualityComparer<T> instance, HashSet<CoI2<T>> hashSet) : this(
			(instance), (IEnumerable<CoI2<T>>)hashSet
		)
		{
		}

		public Mate1(IEqualityComparer<T> instance, CoI2<T>[] vars) : this(instance, (IEnumerable<CoI2<T>>)vars)
		{
		}

		public Mate1(IEqualityComparer<T> objEq, List<Tuple<T, T>> duos) :
			this((objEq), duos.Select(x => new nilnul.obj.Co<T>(x)))
		{
		}
		public Mate1(IEqualityComparer<T> objEq, List<(T, T)> duos) :
			this((objEq), duos.Select(x => new nilnul.obj.Co<T>(x)))
		{
		}
		public Mate1(IEqualityComparer<T> eq, IEnumerable<Tuple<T, T>> duos)
			:this(
			eq, duos.Select( x=> new nilnul.obj.Co<T>(x) )
		)
		{
		}
		public Mate1(_rel.Mate<T, T> duos)
			:this(
			duos.srcElEq, duos.Select( x=> new nilnul.obj.Co<T>(x) )
		)
		{
		}
		public Mate1(Mate_heritRelMate<T> x):this((_rel.Mate<T, T>)x)
		{
		}
		public Mate1(IEqualityComparer<T> eq) : this(
			(eq), new nilnul.obj.CoI2<T>[0]
		)
		{

		}







		public void add((T, T) p)
		{
			Add(
				(p.Item1, p.Item2)
			);
		}

		public void addRange( IEnumerable<(T, T)> p)
		{
			p.Each(
				x=>add(x)
			);
		}


		public void add(T node, T node1)
		{
			add((node, node1));
		}

		public void delNodes(IEnumerable<T> nodes) {
			this.RemoveWhere(
				x => nodes.Contains(x.Item1, this.elEq)
				||
				nodes.Contains(x.Item2, this.elEq)
			);
		}

		public bool has(T x, T y) {
			return base.Contains(
				(x,y)
			);
		}

		public bool hasNot(T x, T y) {
			return !has(x,y);
		}

	}
}

﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_._net
{
	

	public class Mate_heritRelMate<T> : _rel.Mate<T, T>
	{
		public Mate_heritRelMate(IEqualityComparer<T> srcEq0) : base(srcEq0, srcEq0)
		{
		}

		public Mate_heritRelMate():this(EqualityComparer<T>.Default)
		{

		}

		public Mate_heritRelMate(IEqualityComparer<T> srcEq0,  IEnumerable<(T, T)> duos) : base(srcEq0, srcEq0, duos)
		{
		}

		public Mate_heritRelMate(IEqualityComparer<T> srcEq0,  IEnumerable<Tuple<T, T>> duos) : base(srcEq0, srcEq0, duos)
		{
		}

		static public implicit operator Mate<T>(Mate_heritRelMate<T> x) {
			return new Mate<T>(x);
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_._net
{
	/// <summary>
	/// implmented by a set of chains such as to save storage in some cases.
	/// eg:
	///		a->b->c, a->d->c
	///		is more compact than:
	///		a->b,b->c,a->d,a->c
	/// </summary>
	public interface IMatch_byPaths
		:IMatch
	{

	}
}

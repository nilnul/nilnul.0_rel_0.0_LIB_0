﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_._net
{
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// nomenclature:
	///		vs map
	///			<seealso cref="nameof(nilnul._rel.Mate)"/>
	///			map is used in _rel.Map
	///		vs matrix,
	///			(mate as net) looks like matrix
	///	alias:
	///		mate
	///		match
	///		map
	///		mat
	///		matrix
	/// </remarks>
	public interface IMatch
	{
	}

	/// <summary>
	/// 'cuz this is 
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface IMatch<T>:IMatch,
		_rel.IMate<T,T>		///if we still uses the name "IMate<typeparam name="T"></typeparam>", then we herit nilnul._rel.IMate<T,T>. That would be wrong, don't do this, as this is contravening out rule: for Marker interfaces, more generice ones herit less generic type parameterized type. So we need to change the name from Mate to Match

	{ }
}

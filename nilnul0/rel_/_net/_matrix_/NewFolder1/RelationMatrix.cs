﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.relation
{
	public partial class RelationMatrix
	{
		private bool[,] _val;

		public bool[,] val
		{
			get
			{
				return _val;
			}
			set
			{
				if (value == null)
				{
					_val = null;

					return;
				}

				if (value.GetLength(0) != value.GetLength(1))
				{
					throw new ArgumentException("It should be a square ");

				}

				this._val = value;
			}
		}



	

		public RelationMatrix(bool[,] val)
		{
			this.val = val;

		}



		public bool eq(RelationMatrix other)
		{
			return Eq(this, other);


		}


		public bool eq(bool[,] other)
		{

			return this.eq(new RelationMatrix(other));

		}


		public bool Eq(RelationMatrix a, RelationMatrix b)
		{

			return nilnul.collection.Equal.Eval(a._val, b._val, (c, d) => c == d);

		}

		public bool Eq(bool[,] a,bool[,] b) {

			return Eq(new RelationMatrix(a), new RelationMatrix(b));
		
		}

		public int getLength(int rank)
		{

			if (rank != 0 || rank != 1)
			{
				throw new IndexOutOfRangeException();

			}
			if (_val == null)
			{
				return 0;

			}

			return val.GetLength(rank);
		}

		/// <summary>
		/// the number of nodes. not the total number of elements.
		/// </summary>
		public int length
		{
			get
			{
				if (_val == null)
				{
					return 0;

				}
				return val.GetLength(0);
			}

		}




		public bool this[int row, int col]
		{
			get
			{
				return val[row, col];
			}
			set
			{
				val[row, col] = value;
			}
		}

		public IEnumerable<IEnumerable<bool>> getCols()
		{
			for (int i = 0; i < length; i++)
			{
				yield return getCol(i);

			}

		}

		public IEnumerable<bool> getCol(int col)
		{
			for (int row = 0; row < length; row++)
			{
				yield return val[row, col];

			}
		}

		public void addRows(int rowsAugend, int rowsAddend)
		{

			for (int i = 0; i < length; i++)
			{
				_val[rowsAugend, i] |= _val[rowsAddend, i];

			}

		}

		static public bool IsOrNot(bool[,] m)
		{

			if (m == null)
			{
				return true;

			}
			if (m.GetLength(0) != m.GetLength(1))
			{
				return false;

			}
			return true;

		}


		static public void Assert(bool[,] m)
		{

			if (m == null)
			{
				return;

			}
			if (m.GetLength(0) != m.GetLength(1))
			{
				throw new Exception("Should be square.");

			}

		}

		public override string ToString()
		{
			return CollectionTostrX.Tostr(_val);
		}

		static public RelationMatrix Clone(RelationMatrix matrix) {
			//matrix.val.Clone();
			return new RelationMatrix((bool[,]) (matrix.val.Clone()));
		
		}





	}
}

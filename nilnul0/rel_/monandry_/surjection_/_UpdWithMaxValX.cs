﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.dict_
{
	
	/// <summary>
	/// updating with max val
	/// </summary>
	static public class _UpdWithMaxValX
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TKey"></typeparam>
		/// <typeparam name="TVal"></typeparam>
		/// <param name="dict1"></param>
		/// <param name="newEntries"></param>
		/// <param name="comparerOfNulable">comparer of nulable</param>
		static public void UpdWithMaxVal_compAssumbleOnNulable<TKey,TVal>(
			this Dictionary<TKey, TVal> dict1
			,
			IEnumerable<(TKey,TVal)> newEntries
			,
			IComparer<TVal> comparerOfNulable)
			where TVal:class
		{
			
			var re =  new nilnul.obj.nulable.comp.Re<TVal>(
				comparerOfNulable
			);

			newEntries.Each(
				kv=> {
					if (dict1.ContainsKey(kv.Item1))
					{
						if (re.gt( kv.Item2, dict1[kv.Item1] ))
						{
							dict1[kv.Item1] = kv.Item2;
						}
						//else do nothing
					}
					else
					{
						dict1.AddEntry(kv);
					}
				}
			);
		}

		static public void UpdWithMaxVal_compAssumbleOnNulable<TKey, TVal>(
			this Dictionary<TKey, TVal> dict1
			,
			IEnumerable<(TKey, TVal)> newEntries
			
		)
			where TVal : class
		{
			UpdWithMaxVal_compAssumbleOnNulable(
				dict1
				,
				newEntries
				,
				Comparer<TVal>.Default
			);
		}
			/// <summary>
			/// 
			/// </summary>
			/// <typeparam name="TKey"></typeparam>
			/// <typeparam name="TVal"></typeparam>
			/// <param name="dict1"></param>
			/// <param name="newEntries"></param>
			/// <param name="comparer"> of nonnuls </param>
			static public void UpdWithMaxVal_compAssumeOnNonnul2NulMax<TKey,TVal>(
				this Dictionary<TKey, TVal> dict1
				,
				IEnumerable<(TKey,TVal)> newEntries
				,
				IComparer<TVal> comparer
			)
			where TVal:class
		{
			UpdWithMaxVal_compAssumbleOnNulable(
				dict1,newEntries,new nilnul.obj.nulable.comp_.NulMax<TVal>(comparer)
			);
			
			
		}

		static public void UpdWithMaxVal_compAssumeOnNonnul2NulMax<TKey, TVal>(
			this Dictionary<TKey, TVal> dict1
			,
			IEnumerable<(TKey, TVal)> newEntries
			
		)
			where TVal:class
		{
			UpdWithMaxVal_compAssumeOnNonnul2NulMax(
				dict1, newEntries, Comparer<TVal>.Default);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TKey"></typeparam>
		/// <typeparam name="TVal"></typeparam>
		/// <param name="dict1"></param>
		/// <param name="newEntries"></param>
		/// <param name="comparer"> of nonnuls </param>
		static public void UpdWithMaxVal_compAssumeOnNonnul2NulMin<TKey,TVal>(this Dictionary<TKey, TVal> dict1, IEnumerable<(TKey,TVal)> newEntries,IComparer<TVal> comparer)
			where TVal:class
		{
			UpdWithMaxVal_compAssumbleOnNulable(
				dict1,
				newEntries,
				new nilnul.obj.nulable.comp_.NulMin<TVal>(comparer)
			);
			
		}

		static public void UpdWithMaxVal_compAssumeOnNonnul2NulMin<TKey,TVal>(this Dictionary<TKey, TVal> dict1, IEnumerable<(TKey,TVal)> newEntries)
			where TVal:class
		{
			UpdWithMaxVal_compAssumeOnNonnul2NulMin(
				dict1,
				newEntries,
				Comparer<TVal>.Default
			);
			
		}


		static public Dictionary<TKey,TVal> DictOfMax_compAssumeOnNonnul2NulMax<TKey,TVal>(
			this IEnumerable<(TKey,TVal)> dict,
			IComparer<TVal> comparer)
			where TVal:class
		{
			var r = new Dictionary<TKey, TVal>();
			UpdWithMaxVal_compAssumeOnNonnul2NulMax(r,dict,comparer);
			return r;
		}

		static public Dictionary<TKey,TVal> DictOfMax_compAssumeOnNonnul2NulMax<TKey,TVal>(
			this IEnumerable<(TKey,TVal)> dict
			)
			where TVal:class
		{
			
			return DictOfMax_compAssumeOnNonnul2NulMax(dict,Comparer<TVal>.Default);
		}



		static public Dictionary<TKey,TVal> DictOfMax_compAssumeOnNonnul2NulMin<TKey,TVal>(
			this IEnumerable<(TKey,TVal)> dict,
			IComparer<TVal> comparer)
			where TVal:class
		{
			var r = new Dictionary<TKey, TVal>();
			UpdWithMaxVal_compAssumeOnNonnul2NulMin(r,dict,comparer);
			return r;
		}

		static public Dictionary<TKey,TVal> DictOfMax_compAssumeOnNonnul2NulMin<TKey,TVal>(
			this IEnumerable<(TKey,TVal)> dict
			
		)
			where TVal:class
		{
			return DictOfMax_compAssumeOnNonnul2NulMin(dict,Comparer<TVal>.Default);
		}



	}
}

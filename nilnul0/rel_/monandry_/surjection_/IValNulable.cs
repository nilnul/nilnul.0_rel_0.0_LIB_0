﻿using System.Collections.Generic;

namespace nilnul.rel_.surjective_.monandry_
{
	/// <summary>
	/// when the val is a reference type, this can be implemented as <see cref="Dictionary{TKey, TValue}"/>
	/// </summary>
	public interface IValNulable { }
}

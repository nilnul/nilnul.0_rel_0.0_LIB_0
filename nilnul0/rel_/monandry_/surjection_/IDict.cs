﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.surjective_.monandry_
{
	/// <summary>
	/// 
	/// </summary>
	///
	[Obsolete(nameof(surjective_.IMonandry))]
	public interface IDict
		:
		nilnul.obj.IAlias< IMonandry>
		,
		IMonandry
	{
	}
}

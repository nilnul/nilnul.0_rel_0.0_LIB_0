﻿namespace nilnul.rel_
{
	/// <summary>
	/// val is nulable.and hence a relation, not a mate.
	///  if the val is null, it's regarded as if we don't have the corresponding key, 
	/// <seealso cref="nameof(nilnul._rel.mate_.IDict)"/>, which val is non-nulable
	/// </summary>
	public interface IDict
		:
		nilnul.rel_.surjective_.monandry_.IDict
		//nilnul.rel_.surjective_.IMonandry
	{
	}
}

﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_
{

	/// <summary>
	/// monandry
	/// keys are distinct
	/// </summary>
	/// 
	static public class _DictX
	{

		static public void AddEntry<TKey,TVal>(this Dictionary<TKey,TVal> dict, KeyValuePair<TKey,TVal> keyValues) {
			dict.Add(keyValues.Key,keyValues.Value);
		}

		static public void AddEntry<TKey,TVal>(this Dictionary<TKey,TVal> dict, (TKey,TVal) keyValues) {
			dict.Add(keyValues.Item1,keyValues.Item2);
		}

		static public void AddRange<TKey,TVal>(this Dictionary<TKey,TVal> dict, IEnumerable<KeyValuePair<TKey,TVal>> keyValues) {
			keyValues.Each(
				x=>dict.AddEntry(x)
			);
		}




	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.monandry_
{
	/// <summary>
	/// keyWidows are appended. partial or total, with a dict.
	/// The inverse of this would be a function which is total
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	/// <typeparam name="TVal"></typeparam>
	///

	[Obsolete("some nul value shall be regarded as willow")]
	public class Surjection<TKey, TVal>
		: nilnul.obj.Box<Dictionary<TKey, TVal>>
	{
		private HashSet<TKey> _widows;

		public HashSet<TKey> widows
		{
			get { return _widows; }
		}

		public Dictionary<TKey, TVal> mate
		{
			get { return boxed; }
		}

		public Surjection(Dictionary<TKey, TVal> val, IEnumerable<TKey> extraKeys) : base(val)
		{
			_widows = new HashSet<TKey>(
				extraKeys.Except(val.Keys, val.Comparer), val.Comparer
			);
		}

		public Surjection(Dictionary<TKey, TVal> val):this(val, new TKey[0])
		{

		}


	}
}

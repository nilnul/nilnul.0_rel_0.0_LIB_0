﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_.monandry_
{
	/// <summary>
	/// keyWidows are appended; rangeWidows are appended. total or partial. This is also monogamy
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	/// <typeparam name="TVal"></typeparam>
	public class Monogyny<TKey, TVal>
		:
		nilnul.rel.be.en_.BeDefaulted<TKey, TVal, rel.be_.monandry_.Monogyny<TKey, TVal>>
	{
		public Monogyny(Rel3<TKey, TVal> obj) : base(obj)
		{
		}
	}
}

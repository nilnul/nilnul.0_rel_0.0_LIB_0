﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_
{
	/// <summary>
	/// </summary>
	/// <typeparam name="TSrc"></typeparam>
	/// <typeparam name="TTgt"></typeparam>
	/// <remarks>
	/// </remarks>
	public class Monogamy<TSrc, TTgt>
		: nilnul.rel.be.en_.BeDefaulted<TSrc, TTgt, nilnul.rel.be_.Monogamy<TSrc, TTgt>>
	{
		public Monogamy(Rel3<TSrc, TTgt> obj) : base(obj)
		{
		}
	}
}

﻿namespace nilnul.rel_
{
	/// <summary>
	/// a set with coupling.
	/// hence the domain and the range is the same; hence the src and the tgt come from the same type.
	/// </summary>
	/// <remarks>
	/// nomenclature:
	///		"net" is "set", replacing the "s" (single) with "n"(node), where "n" means "not only node/singleS".
	/// </remarks>
	public interface INet
		:IRel
	{

	}
}

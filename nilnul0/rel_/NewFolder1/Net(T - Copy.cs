﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nilnul.obj;
using nilnul.rel_._net;

namespace nilnul.rel_
{
	[Obsolete("lazy widow is not semantically clear-cut, or performant")]
	public class Net<T> : _net_.BlankI<T>
		,_net_.MatchI<T>
		,
		_rel_.SrcInClassI<T>
		,
		_rel_.DstInClassI<T>
		,
		_rel_.FieldInClassI<T>
	{
		public Net(IEqualityComparer<T> eq, IEnumerable<T> objs, IEnumerable<Tuple<T, T>> duos) 
		{
			_match = new _net.Match<T>(
					eq,
					duos
				);
			_nodes = new nilnul.obj.Set1<T>(eq, objs);


		}
		public Net(IEqualityComparer<T> eq, IEnumerable<T> objs, IEnumerable<nilnul.obj.CoI2<T>> duos) 
		{
			_nodes = new nilnul.obj.Set1<T>(eq, objs);
			_match = new _net.Match<T>(
					eq,
					duos
				);


		}
		public Net(IEqualityComparer<T> objEq, IEnumerable<T> objs, IEnumerable<(T, T)> duos )
			
		{
			_nodes = new nilnul.obj.Set1<T>(objEq, objs);
			_match = new _net.Match<T>(
					objEq,
					duos
				);
			
		}

		public Net(IEqualityComparer<T> objEq, List<T> objs, List<Tuple<T, T>> duos )
			:this(
				objEq
				 ,
				
					(IEnumerable<T>)objs,

					(IEnumerable<Tuple<T,T>>)duos
				
			)
		{
			
		}

		public Net(IEqualityComparer<T> objEq, List<T> objs, List<(T, T)> duos )
			:this(
				objEq
				 ,
				
					(IEnumerable<T>)objs,

					(IEnumerable<Tuple<T,T>>)duos
				
			)


		{
			
		}
		/// <summary>
		/// assume eq is same; 
		/// </summary>
		/// <param name="nodes"></param>
		/// <param name="duos"></param>
		private Net(
			
			 nilnul.obj.Set1<T> nodes
			,
			Match<T> duos
		):this(nodes.Comparer, nodes, duos)
		{

			
		}

		public Net(IEqualityComparer<T> eq):this(
			eq, new T[0], new nilnul.obj.CoI2<T>[0]
		)
		{

		}

		public Net():this(EqualityComparer<T>.Default)
		{

		}

		public void pak() {
			_nodes.ExceptWith(
				_match.field
				
			);
		}

		public Set1<T> widows {
			get {
				return new Set1<T>(match.objEq, _nodes.Except(_match.field, match.objEq ) );
			}
		}

		public void add(T node) {
			_nodes.Add(node);
		}

		public void add(T node, T node1) {
			_match.add( node, node1);
		}

		private _net.Match<T> _match;
		public Match<T> match => _match;
		

		public Set1<T> _nodes;
		public Set1<T> nodes
		{
			get
			{
				return _nodes;
				//throw new NotImplementedException();
			}
		}



		public Set1<T> src => field;

		public Set1<T> dst => field;

		/// <summary>
		/// generate a new field
		/// </summary>
		public Set1<T> field => new Set1<T>( this._match.objEq,  _nodes.Concat(_match.field) );

		public override string ToString()
		{
			return $"{_nodes}+{_match}";
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel_
{
	/// <summary>
	/// the mate of this is <see cref="_rel.mate_.IMonandry"/>
	/// for any item in domain, there is no more than 1 img. in other words, there is either 1 or 0 tgt in range.
	/// </summary>
	/// <remarks>
	/// alias:
	///		injective
	/// </remarks>
	public interface IInjective:IRel { }

}

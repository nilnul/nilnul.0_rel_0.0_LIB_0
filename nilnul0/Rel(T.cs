﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nilnul._rel;
using nilnul.obj;
using nilnul.rel_._net;
//#nullable enable;
namespace nilnul
{
	/// <summary>
	/// a finite <see cref="nameof(nilnul.SetI)"/> of <see cref="nameof(nilnul.obj.CoI)"/>
	/// </summary>
	public interface IRel { }



	public interface IRel<T, T1>:IRel { }
	/// <summary>
	/// alias:
	///		lookup
	/// </summary>
	/// <typeparam name="TDomainEl"></typeparam>
	/// <typeparam name="TRangeEl"></typeparam>
	public class Rel3<TDomainEl,TRangeEl>
		:
		_rel_.BlankI
		,
		_rel_.MateI<TDomainEl,TRangeEl>
		
		,
		_rel_.DomainInClassI<TDomainEl>
		,
		_rel_.RangeInClassI<TRangeEl>

	{
		private _rel.Mate<TDomainEl,TRangeEl> _mate;
		public _rel.Mate<TDomainEl,TRangeEl> mate => _mate;

		public Set1<TDomainEl> _domainWidows;
		public Set1<TDomainEl> domainWidows
		{
			get
			{
				return _domainWidows;
				//throw new NotImplementedException();
			}
		}

		public Set1<TRangeEl> _rangeWidows;
		public Set1<TRangeEl> rangeWidows
		{
			get
			{
				return _rangeWidows;
				//throw new NotImplementedException();
			}
		}
		/// <summary>
		/// generate a new set
		/// </summary>
		public Set1<TDomainEl> domain
		{
			get
			{

				var src = this._mate.src;
				src.UnionWith(_domainWidows);
				return src;
			}
		}

		/// <summary>
		/// generate a new set
		/// </summary>
		public Set1<TRangeEl> range
		{
			get
			{

				var tgt = this._mate.tgt;
				tgt.UnionWith(_rangeWidows);
				return tgt;
			}
		}

		public Rel3(_rel.Mate<TDomainEl,TRangeEl> mate0, IEnumerable<TDomainEl> extraSrcs, IEnumerable<TRangeEl> extraDsts)
		{
			this._mate = mate0;

			this._domainWidows = new Set1<TDomainEl>(
				mate0.srcElEq
				,
				extraSrcs.Except(mate0.src, mate0.srcElEq)
			);
			this._rangeWidows = new Set1<TRangeEl>(
				mate0.tgtElEq
				,
				extraDsts.Except(mate0.tgt, mate0.tgtElEq)
			);


		}
		
		public Rel3(IEqualityComparer<TDomainEl> eq, IEqualityComparer<TRangeEl> dstEq1,IEnumerable<Tuple<TDomainEl, TRangeEl>> duos, IEnumerable<TDomainEl> objs, IEnumerable<TRangeEl> extraDsts)
			: this(
				 new Mate<TDomainEl,TRangeEl>(eq,dstEq1, duos)
				 ,
				 objs
				  ,extraDsts
			)
		{



		}
	


		public Rel3(IEqualityComparer<TDomainEl> eq, IEqualityComparer<TRangeEl> dstEq1 ) : this(
			new Mate<TDomainEl, TRangeEl>(eq,dstEq1) , new TDomainEl[0], new TRangeEl[0]
		)
		{

		}

		public Rel3() : this(EqualityComparer<TDomainEl>.Default,EqualityComparer<TRangeEl>.Default)
		{

		}

		

		public override string ToString()
		{
			return $"{_mate}+{_domainWidows}+{_rangeWidows}";
		}
		public void addDomain(TDomainEl node)
		{
			if (!this.mate.src.Contains(node))
			{
				///true if added; false if already there
				_domainWidows.Add(node);
			}
		}

		public void addRange(TRangeEl node)
		{
			if (!this.mate.tgt.Contains(node))
			{
				///true if added; false if already there
				_rangeWidows.Add(node);
			}
		}


		public void add(TDomainEl node, TRangeEl node1)
		{
			_mate.add(node, node1);
		}



	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul
{
	/// <summary>
	/// given natural number :<var>n</var>, and a type G
	/// 
	///		for a family:{ Set[0],Set[1], ... Set[n-1]}, where the member of Set[i] is of type T[0], where T[0] is a subtype of G,
	///		we can get
	///			binary rel,
	///			ternary relation,
	///			n-ary relation,
	///			unary relation, like {}, { [e[0,0], e[1,0] ]}
	///			nary relation, {} or { [] }
	///	This is similar to a relational table, with difference at:
	///		1) the scope of each col is the set of all the instances of a type.
	///		2) the col is indexed by a name, not a positional ordinal num.
	///	
	/// </summary>
	public interface IRelation
	{
	}
	/*
	 The graph is called HyperGraph. see nilnul.rel.Graph
	 */
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul
{
	[Obsolete()]
	public class Rel1<D>
		:Rel<D,D>
	{
		public Rel1(
			IEqualityComparer<D> eq
		):base(eq,eq)
		{

		}

		public IEqualityComparer<D> elementEq {
			get {
				return domainElementEq;
			}
		}

		public HashSet<D> field
		{
			get
			{
				return new HashSet<D>( this.domain.Union(this.field), this.domainElementEq);
			}
		}

	}
}

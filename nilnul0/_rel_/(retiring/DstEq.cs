﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel_
{
	[Obsolete(nameof(nilnul._rel._mate_.TgtEqI<T>))]
	public interface DstEqI<T>
	{
		/// <summary>
		/// the rel must have an equalityComparer for the elements of duos.
		/// </summary>
		/// <returns></returns>
		IEqualityComparer<T> dstEq { get; }

	}

	
}

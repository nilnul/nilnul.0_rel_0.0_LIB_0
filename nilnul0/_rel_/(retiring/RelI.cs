﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel_
{
	[Obsolete()]
	public interface RelI<TDomain,TRange>
	{
		bool rel(TDomain x, TRange y);
	}

	[Obsolete()]
	public interface RelI<T> : RelI<T, T> {

	}
}

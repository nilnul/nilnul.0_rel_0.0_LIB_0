﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel_
{
	/// <summary>
	/// nilnul.Set is, always, finite
	///	alias:
	///		domain
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface Field<T>
	{
		/// <summary>
		/// </summary>
		/// <returns></returns>
		IEnumerable<T> field { get; }

	}

	
}

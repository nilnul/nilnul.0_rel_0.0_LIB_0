﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul
{
	/// <summary>
	/// 
	/// <seealso cref="nameof(nilnul.rel._net_.BlankI)"/> net's singluar are in fields; while their are two sets in the morph and the two sets both have singluars.
	/// <seealso cref="nameof(nilnul.rel)"/> 
	/// </summary>
	/// 
	[Obsolete(nameof(nilnul.rel._net_.BlankI))] //now rel definition is changed to include domain and range.
	public class Morph
	{

	}
}

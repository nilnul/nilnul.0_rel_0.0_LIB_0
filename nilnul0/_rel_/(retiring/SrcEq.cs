﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel_
{
	[Obsolete(nameof(_rel._mate_.SrcEqI<T>))]
	public interface SrcEqI<T>
	{
		/// <summary>
		/// the rel must have an equalityComparer for the elements of duos.
		/// </summary>
		/// <returns></returns>
		IEqualityComparer<T> srcEq { get; }

	}

	
}

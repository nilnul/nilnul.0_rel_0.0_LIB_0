﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul
{
	[Obsolete()]
	public class Rel<D, R>
		: HashSet<Tuple<D, R>>
		,
		nilnul._rel_.RelI<D,R>
	{
		private IEqualityComparer<D> _domainElementEq;

		public IEqualityComparer<D> domainElementEq
		{
			get { return _domainElementEq; }
			set { _domainElementEq = value; }
		}

		private IEqualityComparer<R> _rangeElementEq;

		public IEqualityComparer<R> rangeElementEq
		{
			get { return _rangeElementEq; }
			set { _rangeElementEq = value; }
		}


		public Rel(IEqualityComparer<D> domainEq, IEqualityComparer<R> rangeEq)
			: base(
				new tuple.Eq<D, R>(domainEq, rangeEq)
			 )
		{
			_domainElementEq = domainEq;
			_rangeElementEq = rangeEq;

		}


		public Rel(
			IEqualityComparer<D> domainEq
			,
			IEqualityComparer<R> rangeEq
			,
			IEnumerable<Tuple<D, R>> tuples
		)
			: base(tuples, new tuple.Eq<D, R>(domainEq, rangeEq))
		{
			_domainElementEq = domainEq;
			_rangeElementEq = rangeEq;

		}

		public HashSet<D> domain {
			get {
				return new HashSet<D>( this.Select(x => x.Item1),_domainElementEq);
			}
		}

		public HashSet<R> range {
			get {
				return new HashSet<R>( this.Select(x => x.Item2),_rangeElementEq);
			}
		}

	



		public bool rel(D x, R y)
		{
			return Contains(
				new Tuple<D, R>(x,y)
			);
			//throw new NotImplementedException();
		}
	}
}

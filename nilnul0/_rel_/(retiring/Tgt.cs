﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel_
{
	/// <summary>
	/// the set, per nilnul definition, must be finite
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <remarks>
	/// Alias:
	///		Destination/宿
	///		Tgt/Target
	///		
	/// </remarks>
	///
	[Obsolete("Tgt is reserved for use in Mate")]
	public interface TgtI<T>
	{
		/// <summary>
		/// </summary>
		/// <returns></returns>
		nilnul.obj.SetI1<T> tgt { get; }

	}

	
}

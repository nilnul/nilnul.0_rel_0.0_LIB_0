﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel_
{
	/// <summary>
	/// nilnul.Set is, always, finite
	/// aliase:
	///		domain
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <remarks>
	///
	/// </remarks>
	///
	[Obsolete("src is reserved for use in Mate")]
	public interface SrcI<T>
	{
		/// <summary>
		/// </summary>
		/// <returns></returns>
		nilnul.obj.SetI1<T> src { get; }

	}

	
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel_
{
	[Obsolete("src.elementEq")]
	public interface EleEqI<T>
	{
		/// <summary>
		/// the rel must have an equalityComparer for the elements of duos.
		/// </summary>
		/// <returns></returns>
		IEqualityComparer<T> eleEq { get; }

	}

	
}

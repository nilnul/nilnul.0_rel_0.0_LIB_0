﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel_
{
	/// <summary>
	/// previousely rel is defined as duos.
	/// currently rel is defined as two sets (domain and range) and one duos (the association)
	/// by the current definition: 
	///		duos and singluars (preivously called net) is now a special relation
	///			but net can still be used when:
	///				1) domain and range are the same set.
	///				3) some elements have no association.
	///		previous rel is now called duos (or association)
	///		previous function is now a relation that is total and injection(single-valued)
	///		previous partial mapping is now a special relation that is partial injection.
	/// </summary>
	public interface BlankI
	{

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel_
{
	/// <summary>
	/// the set, per nilnul definition, must be finite
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <remarks>
	/// Alias:
	///		Destination/宿
	///		Tgt/Target
	///		range
	///		
	/// </remarks>
	public interface RangeInClassI<T>
	{
		/// <summary>
		/// </summary>
		/// <returns></returns>
		nilnul.obj.Set1<T> range { get; }

	}

	
}

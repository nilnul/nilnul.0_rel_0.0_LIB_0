﻿using nilnul.obj;
using System.Collections.Generic;

namespace nilnul._rel._mate_
{
	public interface BlankI {
	}

	/// <summary>
	/// in a mate, Domain is considered female, as female come first,  before male; as is the case that 0 goes before 1.
	/// </summary>
	/// <typeparam name="TSrcEl"></typeparam>
	public interface SrcI<TSrcEl>
	{
		/// <summary>
		/// female side
		/// </summary>
		Set1<TSrcEl> src { get; }
	}

	/// <typeparam name="TTgtEl"></typeparam>
	public interface TgtEqI<TTgtEl> {
		IEqualityComparer<TTgtEl> tgtElEq { get; }
	}
	/// <summary>
	/// in mate, range is considered male. male come after female, as 1 come from 0
	/// </summary>
	public interface TgtI<TTgtEl> {
		/// <summary>
		/// target
		/// alias:
		///		dst:destination
		/// </summary>
		Set1<TTgtEl> tgt { get; }
	}
	public interface SrcEqI<TSrc> {
		IEqualityComparer<TSrc> srcElEq { get; }
	}
	public interface AddI<TSrc,TTgt> {
		void add((TSrc, TTgt) p);
	}
	static public class AddIX {
		static public void Add<TSrc, TTgt>(this AddI<TSrc, TTgt> add,TSrc node, TTgt node1) {
			add.add((node, node1));
		}
	}
	public abstract class AddA<TSrc,TTgt>:AddI<TSrc,TTgt>
	{
		public abstract void add((TSrc, TTgt) p);

		public  void add(TSrc node, TTgt node1) {
			AddIX.Add(this,  node,node1);
		}

	}
	public interface HasI<TSrc, TTgt> {
		bool has(TSrc x, TTgt y);
	}

	static public class HasIX
	{
		static public bool HasNot<TSrc, TTgt>(this HasI<TSrc,TTgt> has, TSrc x, TTgt y) {
			return !has.has(x,y);
		}
	}
	public abstract class HasA<TSrc, TTgt> : HasI<TSrc, TTgt>
	{
		public abstract bool has(TSrc x, TTgt y);
		public bool hasNot(TSrc x, TTgt y) {
			return HasIX.HasNot(this, x, y);
		}
	}
	



}
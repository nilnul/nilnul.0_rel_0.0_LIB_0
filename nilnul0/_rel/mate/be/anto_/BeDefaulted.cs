﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate.be.anto_
{
	public class BeDefaulted<TSrc, TTgt, TBe>
		: Anto<TSrc, TTgt>
		where TBe : nilnul.obj.BeI1<Mate<TSrc, TTgt>>, new()

	{
		public BeDefaulted() : base(nilnul.obj_.Singleton<TBe>.Instance)
		{
		}

		static public BeDefaulted<TSrc, TTgt, TBe> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<BeDefaulted<TSrc, TTgt, TBe>>.Instance;
			}
		}

	}
}

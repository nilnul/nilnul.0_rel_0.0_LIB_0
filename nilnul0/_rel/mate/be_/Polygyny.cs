﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate.be_
{
	/// <summary>
	/// a pattern of mating in which a male animal has more than one female mate.
	/// one ore more wives (from Domain). share one hubby (from Range); but one wife can have only one hubby (while a hubby can have one ore more wives)
	/// /// 
	/// </summary>
	/// <remarks>
	/// imaging the domain X, as, femail. 
	/// for each x, there is only one y. one Y can have multiple X
	/// like King with one queen and zero or more concubines.
	/// </remarks>
	public interface PolygynyI
	{

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate.be_
{
	/// <summary>
	/// Monogyny is a specialised mating system in which a male can only mate with zero, or one female throughout his lifetime but the female may(or may not) mate with more than one male.
	/// imagine: a male animal sticks out his one and only one bolt. a female may have two slots like a socket to allow accomodating of more than one bolts.
	/// the in-degree of tgt is all 1.
	/// </summary>
	/// <remarks>
	/// </remarks>
	public class Monogyny<TSrc, TTgt>
		:
		nilnul.obj_.singular.Heir<Monogyny<TSrc, TTgt>>
		,
		mate.BeI<TSrc, TTgt>
	{
		public bool be(Mate<TSrc, TTgt> obj)
		{
			return nilnul.obj.str.be_._DistinctX._IsDistinct_seqAssumeStr(obj.Select(x=>x.Item2));
			//throw new NotImplementedException();
		}


	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate.be_.monogyny_
{
	
	public class Polyandry<TSrc, TTgt>
		:
		nilnul.obj_.singular.Heir<Polyandry<TSrc, TTgt>>
,
		mate.BeI<TSrc, TTgt>
	{
		public bool be(Mate<TSrc, TTgt> obj)
		{
			return Monogyny<TSrc, TTgt>.Singleton.be(obj) && mate.be_.Polyandry<TSrc, TTgt>.Singleton.be(obj);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate.be_
{
	public class Monogamy<TSrc, TTgt>
		:
				nilnul.obj_.singular.Heir<Monogamy<TSrc, TTgt>>
,
		mate.BeI<TSrc, TTgt>
	{
		public bool be(Mate<TSrc, TTgt> obj)
		{
			return Monandry<TSrc, TTgt>.Singleton.be(obj) && Monogyny<TSrc, TTgt>.Singleton.be(obj);
		}
	}
}

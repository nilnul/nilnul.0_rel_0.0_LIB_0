﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate.be_
{
	/// <summary>
	/// a pattern of mating in which a male animal can have zero,one, or more than one female mate.
	/// also known func, but dict is a data structure and func is an operation/process resulting such data. for every key in domain, there is one and only one val in the range
	/// alias:
	///		func; but func will be used to denote lambda expression.
	///		map; but map may conflict with atlas
	///		polygyny
	///		dict
	/// </summary>
	/// <remarks>
	/// </remarks>
	public class Polygyny<TSrc, TTgt>
		:
		monogyny.Anto<TSrc,TTgt>
	{

	}
}

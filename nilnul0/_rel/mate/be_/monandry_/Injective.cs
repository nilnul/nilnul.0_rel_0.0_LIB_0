﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate.be_.monandry_
{
	/// <summary>
	/// for each male, there is one and only one female
	/// </summary>
	///
	[Obsolete("reserve this for Rel")]
	public interface IInjective:IMonogyny
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate.be_.monandry_
{

	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="TSrc"></typeparam>
	/// <typeparam name="TTgt"></typeparam>
	public class Polygyny<TSrc, TTgt>
		:
		nilnul.obj_.singular.Heir<Polygyny<TSrc, TTgt>>
,
		mate.BeI<TSrc, TTgt>
	{
		public bool be(Mate<TSrc, TTgt> obj)
		{
			return Monandry<TSrc, TTgt>.Singleton.be(obj) && mate.be_.Polygyny<TSrc, TTgt>.Singleton.be(obj);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate.be_
{
	/// <summary>
	/// alias:
	///		Polyamory
	/// </summary>
	/// <typeparam name="TSrc"></typeparam>
	/// <typeparam name="TTgt"></typeparam>
	public class Swinger<TSrc, TTgt>
		:
		nilnul.obj_.singular.Heir<Swinger<TSrc, TTgt>>
,
		mate.BeI<TSrc, TTgt>
	{
		public bool be(Mate<TSrc, TTgt> obj)
		{
			return Polyandry<TSrc, TTgt>.Singleton.be(obj) && Polygyny<TSrc, TTgt>.Singleton.be(obj);
		}
	}
}

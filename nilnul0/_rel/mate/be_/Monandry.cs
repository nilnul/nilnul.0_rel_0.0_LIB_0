﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate.be_
{
	/// <summary>
	/// a pattern of mating in which a female has only nil, or one mate at a time. (but how many females  a male does have is not specified here: it can be polygyny, or monogyny ; in other words, a male can have one, or many wives (note here in mating, male with nil wives is not taken into account) ).
	/// thinking every female sticks out one and one pipe (which is the mate coimage) interface to accept. some males may have two bolts to insert into two pipes simultaneously. monoandry:= the andry bolt is mono for each male
	/// the out-degree of src is all 1.
	/// alias:
	///		dict.
	///		injective.
	///		func
	///		
	/// </summary>
	/// <remarks>
	/// alias:
	///		monandry:
	///			"andry" in the husbandry
	/// </remarks>
	///
	public interface IMonandry {

	}
	public class Monandry<TSrc, TTgt>
		:
		nilnul.obj_.singular.Heir<Monandry<TSrc, TTgt>>
		,
		mate.BeI<TSrc, TTgt>
		,
		IMonandry
	{
		public bool be(Mate<TSrc, TTgt> obj)
		{
			return nilnul.obj.str.be_._DistinctX._IsDistinct_seqAssumeStr(obj.Select(x=>x.Item1)); /// -andry shall be understood as a binary thing -- an arrow pointing to the other part. The distinctness means mono-andry; mono is the distinctness, andry is the pointing outwards to the other part.
		}


	}
}

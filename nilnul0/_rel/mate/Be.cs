﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate
{
	public interface BeI<TSrc,TTgt>
		:
		nilnul.obj.BeI1<nilnul._rel.Mate<TSrc, TTgt> >
	{
	}

	public class Be<TSrc, TTgt>
		:
		nilnul.obj.Be1<nilnul._rel.Mate<TSrc, TTgt>>,
		BeI<TSrc, TTgt>
	{
		public Be(Func<Mate<TSrc, TTgt>, bool> func) : base(func)
		{
		}
	}
}

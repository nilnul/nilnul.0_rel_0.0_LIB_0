﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate.src.be_
{
	/// <summary>
	/// in the domain, a src is the start for every tgt.
	/// ie,
	///		given a src,
	///			for each tgt, there is  a co, the stop of which is the tgt, the start of which is the src.
	///	There might be plural starters.
	/// </summary>
	/// alias:
	///		genesis
	///		src4every
	///		start
	///		begin
	///		Apocalypse
	///		germination.
	class Start4every
	{
	}
}

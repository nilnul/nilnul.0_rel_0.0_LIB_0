﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry_.dict
{

	/// <summary>
	/// monandry
	/// keys are distinct
	/// </summary>
	///
	[Obsolete(nameof(_monandry.dict._ExtensionsX.Phrase))]
	static public class _PhraseX
	{
		static public string Phrase<TKey,TVal>(this Dictionary<TKey,TVal> dict) {
			return nilnul.obj.set.PhraseX.Phrase(
				dict.Select(
					kv=> nilnul.obj.co.phrase_.KeyValX.Phrase<TKey,TVal>(kv)
				)
			);
		}





	}
}

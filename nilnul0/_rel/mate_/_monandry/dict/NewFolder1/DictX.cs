﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry_
{

	/// <summary>
	/// monandry
	/// keys are distinct
	/// </summary>
	///
	[Obsolete(nameof(_monandry.dict._ExtensionsX))]
	static public class _DictX
	{
		static public string Phrase<TKey,TVal>(this Dictionary<TKey,TVal> dict) {
			return "{"+
				string.Join(
					","
					,
					dict.Select(
						e=> nilnul.obj.couple.KeyVal.Phrase(e) 
					)
				)
				+
				"}";
		}

		static public void AddEntry<TKey,TVal>(this Dictionary<TKey,TVal> dict, KeyValuePair<TKey,TVal> keyValues) {
			dict.Add(keyValues.Key,keyValues.Value);
		}

		static public void AddEntry<TKey,TVal>(this Dictionary<TKey,TVal> dict, (TKey,TVal) keyValues) {
			dict.Add(keyValues.Item1,keyValues.Item2);
		}

		static public void AddRange<TKey,TVal>(this Dictionary<TKey,TVal> dict, IEnumerable<KeyValuePair<TKey,TVal>> keyValues) {
			keyValues.Each(
				x=>dict.AddEntry(x)
			);
		}


		static public void UpdateOfMax<TKey,TVal>(this Dictionary<TKey, TVal> dict1, IEnumerable<(TKey,TVal)> newEntries,IComparer<TVal> comparer) {

			var re = new nilnul.obj.comp.Re<TVal>(comparer);

			newEntries.Each(
				kv=> {
					if (dict1.ContainsKey(kv.Item1))
					{
						if (re.gt( kv.Item2, dict1[kv.Item1] ))
						{
							dict1[kv.Item1] = kv.Item2;
						}
						//else do nothing
					}
					else
					{
						dict1.AddEntry(kv);
					}
				}
			);
		}

		static public Dictionary<TKey,TVal> DictOfMax<TKey,TVal>(this IEnumerable<(TKey,TVal)> dict,IComparer<TVal> comparer) {
			var r = new Dictionary<TKey, TVal>();
			UpdateOfMax(r,dict,comparer);
			return r;
		}


	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_._monandry.dict.be_.valNonul.vow
{
	public class Ee<TKey, TVal> :

		nilnul.obj.vow.ee_.Defaultable<Dictionary<TKey, TVal>, Vow<TKey, TVal>>
	{


		public Ee(Dictionary<TKey, TVal> val) : base(val)
		{
		}

		public override string ToString()
		{
			return dict._ExtensionsX.Phrase(this.ee);
		}

	}
}

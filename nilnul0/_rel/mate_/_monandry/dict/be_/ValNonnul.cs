﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_._monandry.dict.be_
{
	/// <summary>
	/// val is nonnul; thus it's a mate.
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	/// <typeparam name="TVal"></typeparam>
	public class ValNonnul<TKey, TVal> : BeI<TKey, TVal>
	{
		
		public bool be(Dictionary<TKey, TVal> obj)
		{
			return obj.Values.All(x=> x is not null);
		}


		static public ValNonnul<TKey, TVal> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<ValNonnul<TKey, TVal>>.Instance;
			}
		}

	}


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_._monandry
{

	///A key cannot be null, but a value can be, if its type TValue is a reference type.
	/// <summary>
	/// synonym to <see cref="System.Collections.IDictionary"/>, which can has nul as val, and thus is not <see cref="IMate"/>
	/// </summary>
	/// 
	/// <see cref="rel_"/>
	public interface IDict
	{
	}
}

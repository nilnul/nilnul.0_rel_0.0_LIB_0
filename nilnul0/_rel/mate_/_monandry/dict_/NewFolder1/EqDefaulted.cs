﻿using nilnul.obj.str;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry_.dict_
{


	[Obsolete(nameof(_monandry.dict_.KeyEqDefaulted<TKey,TVal, TKeyEq>))]
	public class KeyEqDefaulted<TKey,TVal, TKeyEq>:Dictionary<TKey, TVal>
		where TKeyEq:IEqualityComparer<TKey>,new()
	{
		public KeyEqDefaulted():base(
			nilnul.obj_.Singleton<TKeyEq>.Instance
		)
		{

		}

		public KeyEqDefaulted(IDictionary<TKey,TVal> dict):base(dict, nilnul.obj_.Singleton<TKeyEq>.Instance)
		{

		}

		public KeyEqDefaulted(IEnumerable<(TKey,TVal)> dict):base( nilnul.obj_.Singleton<TKeyEq>.Instance)
		{
			dict.Each(
				c=> Add(c.Item1,c.Item2)
			);
		}




	}
}

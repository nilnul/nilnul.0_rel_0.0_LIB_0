﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry_.dict_.keyEqDefaulted.re_.equiv_
{
	public class SameKeys<TKey, TVal, TKeyEq>
		: nilnul.obj.ReI<KeyEqDefaulted<TKey, TVal, TKeyEq>>

		where TKeyEq : IEqualityComparer<TKey>, new()
	{
		public bool re(KeyEqDefaulted<TKey, TVal, TKeyEq> a, KeyEqDefaulted<TKey, TVal, TKeyEq> b)
		{
			return nilnul.obj.set.eq_.EqDefaulted<TKey, TKeyEq>.Singleton.Equals(a.Keys,b.Keys);
			//throw new NotImplementedException();
		}


		static public SameKeys<TKey, TVal, TKeyEq> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<SameKeys<TKey, TVal, TKeyEq>>.Instance;
			}
		}

	}
}

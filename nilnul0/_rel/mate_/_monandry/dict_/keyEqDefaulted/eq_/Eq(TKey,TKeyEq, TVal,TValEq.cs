﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry_.dict_.keyEqDefaulted.eq_
{
	public class Eq<TKey, TVal, TKeyEq,TValEq>
		:
		Eq<TKey,TVal,TKeyEq>
		where TKeyEq : IEqualityComparer<TKey>, new()
		where TValEq:IEqualityComparer<TVal>,new()
	{
		

		public Eq():base(nilnul.obj_.Singleton<TValEq>.Instance)
		{
		}


		static public Eq<TKey, TVal, TKeyEq, TValEq> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<Eq<TKey, TVal, TKeyEq, TValEq>>.Instance;
			}
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry_.dict_.keyEqDefaulted
{
	public class Eq<TKey, TVal, TKeyEq> :

		IEqualityComparer<KeyEqDefaulted<TKey, TVal, TKeyEq>>
		where TKeyEq : IEqualityComparer<TKey>, new()
	{
		private IEqualityComparer<TVal> _valEq;

		public IEqualityComparer<TVal> valEq
		{
			get { return _valEq; }
			set { _valEq = value; }
		}

		public Eq(IEqualityComparer<TVal> valEq)
		{
			_valEq = valEq;
		}

		public bool Equals(KeyEqDefaulted<TKey, TVal, TKeyEq> x, KeyEqDefaulted<TKey, TVal, TKeyEq> y)
		{
			return nilnul.obj.set.eq_.EqDefaulted<TKey,TKeyEq>.Singleton.Equals(x.Keys, y.Keys)
				&&
				x.Keys.All(
					a => _valEq.Equals(x[a], y[a])
				)
			;
			//throw new NotImplementedException();
		}

		public int GetHashCode(KeyEqDefaulted<TKey, TVal, TKeyEq> obj)
		{
			return 0;
		}
	}
}

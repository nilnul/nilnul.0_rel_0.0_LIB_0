﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry_.dict_.checklist_.keyEqDefaulted

{
	public class Eq<TKey,TKeyEq> : checklist.Eq<TKey>,
		IEqualityComparer<checklist_.KeyEqDefaulted<TKey,TKeyEq>>
		where TKeyEq:IEqualityComparer<TKey>,new()
	{
		public Eq() : base(nilnul.obj_.Singleton<TKeyEq>.Instance)
		{
		}

		public bool Equals(checklist_.KeyEqDefaulted<TKey,TKeyEq> x, checklist_.KeyEqDefaulted<TKey,TKeyEq> y)
		{
			return base.Equals(x, y);
			throw new NotImplementedException();
		}

		public int GetHashCode(checklist_.KeyEqDefaulted<TKey,TKeyEq> obj)
		{
			return base.GetHashCode(obj);
			throw new NotImplementedException();
		}

		static public Eq<TKey, TKeyEq> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<Eq<TKey, TKeyEq>>.Instance;
			}
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry_.dict_.checklist_
{
	public class KeyEqDefaulted<T,TEq>:Checklist<T>
		where TEq:IEqualityComparer<T>,new()
	{
		public KeyEqDefaulted():base(nilnul.obj_.Singleton<TEq>.Instance)
		{

		}

		public KeyEqDefaulted(IDictionary<T,bool> dict):base(dict, nilnul.obj_.Singleton<TEq>.Instance)
		{

		}
	}
}

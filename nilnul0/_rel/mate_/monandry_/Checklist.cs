﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry_.dict_
{
	/// <summary>
	/// yes or no
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class Checklist<T>:Dictionary<T,bool>
	{

		public Checklist(IEqualityComparer<T> eq):base(eq)
		{

		}

		public Checklist(IDictionary<T,bool> dict, IEqualityComparer<T> eq) :base(dict,eq)
		{

		}
	}
}

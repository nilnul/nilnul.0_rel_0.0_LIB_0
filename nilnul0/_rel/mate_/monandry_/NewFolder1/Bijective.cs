﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry_.dict_
{
	/// <summary>
	/// values are distinct. the monandry is monogyny.
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	/// <typeparam name="TVal"></typeparam>
	///
	[Obsolete(nameof(mate_.Monogamy<TKey,TVal>))]
	public class Bijective<TKey, TVal> : dict.be_.bijective.vow.Ee<TKey, TVal>
	{
		public Bijective(IEqualityComparer<TVal> valEq) : base(valEq)
		{
		}

		public Bijective(Dictionary<TKey, TVal> val, IEqualityComparer<TVal> valEq) : base(val, valEq)
		{
		}

		//public IEqualityComparer<TVal> valEq { get { } }

		public TVal this[TKey key] {
			get {
				return ee[key];
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="val"></param>
		/// <returns></returns>
		/// <exception cref="">
		///		if val not found
		/// </exception>
		public TKey getKeyOfVal(TVal val) {
			return ee.Where(
				kv =>
					this.valEq.Equals(
						kv.Value, val
					)
			).First().Key;
		}

	}
}

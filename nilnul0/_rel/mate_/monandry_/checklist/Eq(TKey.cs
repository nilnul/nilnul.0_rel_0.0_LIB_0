﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry_.dict_.checklist
{
	public class Eq<TKey> : dict.Eq<TKey, bool>,
		IEqualityComparer<Checklist<TKey>>
	{
		public Eq(IEqualityComparer<TKey> keyEq) : base(keyEq, EqualityComparer<bool>.Default)
		{
		}

		public bool Equals(Checklist<TKey> x, Checklist<TKey> y)
		{
			return base.Equals(x, y);
			throw new NotImplementedException();
		}

		public int GetHashCode(Checklist<TKey> obj)
		{
			return base.GetHashCode(obj);
			throw new NotImplementedException();
		}
	}
}

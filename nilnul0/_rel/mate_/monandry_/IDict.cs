﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry_
{
	/// <summary>
	/// synonym to <see cref="nameof(mate_.IMonandry)"/>
	/// a collection of keyVal pair, but keys are distinct unlike mate in which keys can repeat. vs: Sys.Dict: value cannot be null. 
	///		
	/// </summary>
	/// <remarks>
	/// in mate_.Dict, val cannot be null.
	/// in rel_.Dict, val can be null.
	/// 
	/// dict, as in System.Dictionary, is implicitly allow nul in value. So we better put Dict under the namespace "mate_" to explicitly mean that it allows not null in value.
	/// vs
	///		<seealso cref="nameof(mate_.IMonandry)"/>
	///			(a,1), (a,2) is not dict, neither monandry
	///			(a,1), (b,1) is dict, also monandry
	///			(a,1), (a,1) is not mate, as distinct entries are merged into one.
	///			(a,1),(b,2) is dict and monandry.
	/// </remarks>
	public interface IDict:IMonandry
	{
	}
}

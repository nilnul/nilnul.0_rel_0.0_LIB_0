﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_
{
	/// <summary>
	/// a mate that is monandry <see cref="nameof(mate.be_.Monandry)"/>, i.e., one female has only one counterpart. (if a female has nil counterpart, the female is not shown in mate, but showable in rel).
	/// </summary>
	public interface IMonandry
	{
	}
}

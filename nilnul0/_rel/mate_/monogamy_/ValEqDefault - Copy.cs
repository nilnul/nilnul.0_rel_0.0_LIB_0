﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.bijection_
{
	/// <summary>
	/// values are distinct. the monandry is monogyny.
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	/// <typeparam name="TVal"></typeparam>
	public class ValEqDefault<TKey, TVal,TValEq>
		:
		_rel.mate_.monandry_. dict_.Bijective<TKey, TVal>
		where
		TValEq:IEqualityComparer<TVal>,new()

	{
		public ValEqDefault() : base(nilnul._obj.typ_.nilable_.unprimable_.singleton_.ByLazy<TValEq>.Instance)
		{
		}

		public ValEqDefault(Dictionary<TKey, TVal> val)
			:
		base(
			val
			,
			nilnul._obj.typ_.nilable_.unprimable_.singleton_.ByLazy<TValEq>.Instance
		)
		{
		}



	}
}

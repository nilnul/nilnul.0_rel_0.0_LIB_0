﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monogamy_.keyEqDefault_
{
	/// <summary>
	/// values are distinct. the monandry is monogyny.
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	/// <typeparam name="TVal"></typeparam>
	public class ValEqDefault<TKey, TVal,TKeyEq,TValEq>
		:
		_rel.mate_.monogamy_.KeyEqDefault<TKey, TVal,TKeyEq>
		where
		TKeyEq:IEqualityComparer<TKey>,new()
		where
		TValEq : IEqualityComparer<TVal>, new()

	{
		public ValEqDefault() :
			base(nilnul._obj.typ_.nilable_.unprimable_.singleton_.ByLazy<TValEq>.Instance)
		{
		}

		public ValEqDefault(IEnumerable<(TKey, TVal )> val)
			:
		base(
			val
			,
			nilnul._obj.typ_.nilable_.unprimable_.singleton_.ByLazy<TValEq>.Instance
		)
		{
		}

		public ValEqDefault(params (TKey, TVal )[] val)
			:
		this(
			(IEnumerable<(TKey, TVal )>)val
			
		)
		{
		}




	}
}

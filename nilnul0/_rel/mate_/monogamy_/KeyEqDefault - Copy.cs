﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.bijection_
{
	/// <summary>
	/// values are distinct. the monandry is monogyny.
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	/// <typeparam name="TVal"></typeparam>
	///
	[Obsolete(nameof(monogamy_.KeyEqDefault<TKey, TVal, TKeyEq>))]
	public class KeyEqDefault<TKey, TVal, TKeyEq>
		:
		_rel.mate_.monandry_.dict_.Bijective<TKey, TVal>
		where
		TKeyEq : IEqualityComparer<TKey>, new()

	{
		

		public KeyEqDefault(
			mate_.monandry_.dict_.KeyEqDefaulted<TKey, TVal, TKeyEq> dict, IEqualityComparer<TVal> valEq)
			:
			base(
				dict
			,
				valEq
		)
		{
		}
		public KeyEqDefault(
			IEnumerable<(TKey, TVal)> mate, IEqualityComparer<TVal> valEq
		)
			:
			this(
				new monandry_.dict_.KeyEqDefaulted<TKey, TVal, TKeyEq>(mate)
				
			,
				valEq
		)
		{
		}

		public KeyEqDefault(
			 IEqualityComparer<TVal> valEq
		)
			:
			base(
				
				valEq
		)
		{
		}


	}
}

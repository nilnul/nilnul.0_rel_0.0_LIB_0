﻿using System.Collections.Generic;

namespace nilnul._rel.mate_
{
	public class Monandry<TKey, TVal> : _monandry.dict.be_.valNonul.vow.Ee<TKey, TVal>
	{
		public Monandry(Dictionary<TKey, TVal> val) : base(val)
		{
		}

		public Monandry(IEqualityComparer<TKey> keyEq):this(
			new Dictionary<TKey, TVal>(keyEq)
		)
		{

		}

		public Monandry():this(new Dictionary<TKey, TVal>())
		{

		}
		public override string ToString()
		{
			return base.ToString();
		}


	}
}

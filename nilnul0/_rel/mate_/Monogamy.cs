﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_
{
	/// <summary>
	/// values are distinct. the monandry is monogyny.
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	/// <typeparam name="TVal"></typeparam>
	/// <remarks>
	/// to use this, it's better to utilize the builder pattern by building a dcit first.
	/// </remarks>
	public class Monogamy<TKey, TVal> : monandry.be_.bijective.vow.Ee<TKey, TVal>
	{
		public Monogamy(IEqualityComparer<TVal> valEq) : base(valEq)
		{
		}
		public Monogamy() : base(EqualityComparer<TVal>.Default)
		{
		}


		public Monogamy(Monandry<TKey, TVal> val, IEqualityComparer<TVal> valEq) : base(val, valEq)
		{
		}

		public Monogamy(Monandry<TKey, TVal> val)
			:
			this(
				val, EqualityComparer<TVal>.Default
			)
		{
		}


		public Monogamy(Dictionary<TKey, TVal> val, IEqualityComparer<TVal> valEq) : this(new Monandry<TKey,TVal>(val), valEq)
		{
		}

		public Monogamy(Dictionary<TKey, TVal> val) : this(
			val, EqualityComparer<TVal>.Default
		)
		{
		}
		

		//public IEqualityComparer<TVal> valEq { get { } }

		public TVal this[TKey key] {
			get {
				return this.eeByRef.ee[key];
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="val"></param>
		/// <returns></returns>
		/// <exception cref="">
		///		if val not found
		/// </exception>
		public TKey getKeyOfVal(TVal val) {
			return eeByRef.ee.Where(
				kv =>
					this.valEq.Equals(
						kv.Value, val
					)
			).First().Key;
		}

		public override string ToString()
		{
			return this.eeByRef.ToString();
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry_.dict.be_
{
	/// <summary>
	/// the monandry is monogyny, thus bijective.
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	/// <typeparam name="TVal"></typeparam>
	///
	[Obsolete(nameof(monandry.be_.Bijective<TKey,TVal>) )]
	public class Bijective<TKey, TVal> : BeI<TKey, TVal>
	{
		private IEqualityComparer<TVal> _valEq;

		public IEqualityComparer<TVal> valEq
		{
			get { return _valEq; }
			set { _valEq = value; }
		}

		public Bijective(IEqualityComparer<TVal> valEq)
		{
			_valEq = valEq;
		}
		public bool be(Dictionary<TKey, TVal> obj)
		{
			return nilnul.obj.str.be_._DistinctX._IsDistinct_seqAssumeStr(obj.Values, _valEq);
			//throw new NotImplementedException();
		}
	}


}

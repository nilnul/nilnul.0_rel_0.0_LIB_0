﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry_.dict.be_.bijective
{
	[Obsolete(nameof(monandry.be_.bijective.Vow<TKey, TVal>))]
	public class Vow<TKey, TVal>
		: nilnul.obj.be.Vow4<
			Dictionary<TKey, TVal>
		>
	{
		public Vow(IEqualityComparer<TVal> valEq) : base(new Bijective<TKey,TVal>(valEq))
		{
		}
	}

	[Obsolete(nameof(monandry.be_.bijective.vow_.ValEqDefaulted<TKey, TVal,TValEq>))]
	public class Vow_valEqDefaulted<TKey,TVal, TValEq>
		:nilnul.obj.be.vow_.BeDefaulted4<
			Dictionary<TKey,TVal>
			,Bijective_valEqDefaulted<TKey,TVal,TValEq>
		>
	where TValEq:IEqualityComparer<TVal>, new()
	{
	}

}

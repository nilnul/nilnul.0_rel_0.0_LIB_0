﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry.be_.bijective.vow_
{


	public class ValEqDefaulted<TKey,TVal, TValEq>
		:nilnul.obj.be.vow_.BeDefaulted4<
			Monandry<TKey,TVal>
			,bijective_.ValEqDefaulted<TKey,TVal,TValEq>
		>
	where TValEq:IEqualityComparer<TVal>, new()
	{
	}

}

﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry.be_.bijective
{
	public class Vow<TKey, TVal>
		: nilnul.obj.be.Vow4<
			Monandry<TKey, TVal>
		>
		,
		nilnul.obj.VowI_ofIn<
			Monandry<TKey, TVal>

		>
	{

		public Vow(IEqualityComparer<TVal> valEq) : base(new Bijective<TKey,TVal>(valEq))
		{
		}
		public Vow():base( Bijective<TKey,TVal>.Singleton )
		{

		}


		static public Vow<TKey, TVal> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<Vow<TKey, TVal>>.Instance;
			}
		}

		public void vow(in Monandry<TKey, TVal> obj)
		{
			base.vow(obj);
		}
	}

	

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry_.dict.be_.bijective.vow
{
	[Obsolete(nameof(monandry.be_.bijective.vow.Ee<TKey, TVal>))]
	public class Ee<TKey, TVal> :

		nilnul.obj.vow.Ee1<Dictionary<TKey, TVal>, Vow<TKey, TVal>>
	{
		private IEqualityComparer<TVal> _valEq;

		public IEqualityComparer<TVal> valEq
		{
			get { return _valEq; }
		}

		public Ee(Dictionary<TKey, TVal> val,IEqualityComparer<TVal> valEq0) : base(val, new Vow<TKey, TVal>(valEq0))
		{
			_valEq = valEq0;
		}
		public Ee(IEqualityComparer<TVal> valEq) :this( new Dictionary<TKey, TVal>(),valEq  )
		{
		}


		public override string ToString()
		{
			return nilnul._rel.mate_.monandry_._DictX.Phrase(boxed);
		}
	}

	[Obsolete(nameof(monandry.be_.bijective.vow.ee_.ValEqDefaulted<TKey, TVal, TValEq>))]
	public class Ee_valEqDefaulted<TKey, TVal, TValEq> :

		nilnul.obj.vow.ee_.Defaultable<Dictionary<TKey, TVal>, Vow_valEqDefaulted<TKey, TVal, TValEq>>
		where TValEq:IEqualityComparer<TVal>,new()
	{
		public Ee_valEqDefaulted(Dictionary<TKey, TVal> val) : base(val)
		{
		}
		public Ee_valEqDefaulted():this( new Dictionary<TKey, TVal>()  )
		{
		}


		public override string ToString()
		{
			return nilnul._rel.mate_.monandry_._DictX.Phrase(boxed);
		}
	}}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry.be_.bijective.vow.ee_
{
	public class ValEqDefaulted<TKey, TVal, TValEq> :

		nilnul.obj.vow.ee_.Defaultable<Monandry<TKey, TVal>, vow_.ValEqDefaulted<TKey, TVal, TValEq>>
		where TValEq : IEqualityComparer<TVal>, new()
	{
		public ValEqDefaulted(Monandry<TKey, TVal> val) : base(val)
		{
		}
		public ValEqDefaulted(Dictionary<TKey, TVal> val) : this( new Monandry<TKey, TVal>(val) )
		{
		}

		public ValEqDefaulted() : this(new Dictionary<TKey, TVal>())
		{
		}


		public override string ToString()
		{
			return nilnul._rel.mate_.monandry_._DictX.Phrase(boxed.ee);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry.be_.bijective.vow
{
	public class Ee<TKey, TVal> :

		nilnul.obj.vow.Ee_ofIn<Monandry<TKey, TVal>, Vow<TKey, TVal>>
	{
		public IEqualityComparer<TVal> _valEq;

		public IEqualityComparer<TVal> valEq
		{
			get { return _valEq; }
		}

		public Ee(Monandry<TKey, TVal> val, IEqualityComparer<TVal> valEq0) : base(val, new Vow<TKey, TVal>(valEq0))
		{
			_valEq = valEq0;
		}

		public Ee(Monandry<TKey, TVal> val) : this(val, EqualityComparer<TVal>.Default)
		{
		}
		public Ee(IEqualityComparer<TVal> valEq) : this(new Monandry<TKey, TVal>(), valEq)
		{

		}
		public Ee():this(EqualityComparer<TVal>.Default)
		{

		}


		public override string ToString()
		{
			return nilnul._rel.mate_.monandry_._DictX.Phrase(this.eeByRef.ee);
		}
	}
}
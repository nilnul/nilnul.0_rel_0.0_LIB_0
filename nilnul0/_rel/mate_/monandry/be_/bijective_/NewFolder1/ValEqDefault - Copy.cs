﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry_.dict.be_
{

	[Obsolete(nameof(monandry.be_.bijective_.ValEqDefaulted<TKey,TVal,TValEq>))]
	public class Bijective_valEqDefaulted<TKey, TVal, TValEq> : Bijective<TKey, TVal>
		where TValEq : IEqualityComparer<TVal>, new()
	{
		public Bijective_valEqDefaulted() : base(nilnul.obj_.Singleton<TValEq>.Instance)
		{
		}

		static public Bijective_valEqDefaulted<TKey, TVal, TValEq> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<Bijective_valEqDefaulted<TKey, TVal, TValEq>>.Instance;
			}
		}

	}
}

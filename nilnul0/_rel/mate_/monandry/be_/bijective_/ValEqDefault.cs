﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry.be_.bijective_
{


	public class ValEqDefaulted<TKey, TVal, TValEq> : Bijective<TKey, TVal>
		where TValEq : IEqualityComparer<TVal>, new()
	{
		public ValEqDefaulted() : base(
			 nilnul._obj.typ_.nilable_.unprimable_.singleton_.ByLazy<TValEq>.Instance
		)
		{
		}


		static public ValEqDefaulted<TKey, TVal, TValEq> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<ValEqDefaulted<TKey, TVal, TValEq>>.Instance;
			}
		}


	}
}

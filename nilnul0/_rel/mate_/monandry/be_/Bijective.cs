﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_.monandry.be_
{
	/// <summary>
	/// the monandry is monogyny, thus bijective.
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	/// <typeparam name="TVal"></typeparam>
	public class Bijective<TKey, TVal> : BeI<TKey, TVal>
	{
		private IEqualityComparer<TVal> _valEq;

		public IEqualityComparer<TVal> valEq
		{
			get { return _valEq; }
			set { _valEq = value; }
		}

		public Bijective(IEqualityComparer<TVal> valEq)
		{
			_valEq = valEq;
		}

		public Bijective():this(EqualityComparer<TVal>.Default)
		{

		}
		public bool _be_assumeValNonul(Dictionary<TKey, TVal> obj)
		{
			return nilnul.obj.str.be_._DistinctX._IsDistinct_seqAssumeStr(obj.Values, _valEq);
			//throw new NotImplementedException();
		}

		public bool be(Monandry<TKey, TVal> obj)
		{
			return _be_assumeValNonul(obj.ee);
		}


		static public Bijective<TKey, TVal> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<Bijective<TKey, TVal>>.Instance;
			}
		}

	}


}

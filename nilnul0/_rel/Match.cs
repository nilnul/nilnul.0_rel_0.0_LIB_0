﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel
{
	/// <summary>
	/// a collection of couples(2-tuple).\
	/// The domain is called X, the range is called Y. Incidence with the chromesome in that XX is female, and XY is male if we put female first before male like we put 0 before 1.
	/// alias:
	///		map
	///		mate: sounds like map
	///		pairing
	///		match
	///	vs:
	///		dict: one key has one val; but match one key can has multiple values
	/// </summary>
	public interface IMap {

	}

	//public interface MatchI<TS, TT>:MatchI { }
	//public class Match
	//{
	//}
}

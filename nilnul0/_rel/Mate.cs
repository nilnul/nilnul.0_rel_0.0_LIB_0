﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel
{
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// nomenclature:
	///		mate is a collection of co. Why not name it as cos?
	///			cuz co's src/tgt are of the same type, while in mate, the domain and range are objects of different types.
	///	alias:
	///		map
	///		map. map is used for atlas; and mate is more vivid/intuitive/easy-to-image/materializable
	///		mapping
	///		mate
	///		match
	///		match. same begining of "mat"
	///		morph
	///
	/// </remarks>
	/// alias:
	///		mate
	///		mat
	///		match
	///			which is used by <see cref="rel_._net.IMatch"/>
	///		map
	public interface IMate {

	}

	public interface IMate<TSrc, TTgt> : IMate { }
	/// <summary>
	/// a set(not str, or bag, or sortie) of "co"S
	/// alias:
	///		meet?met
	///		lookup
	/// </summary>
	/// <remarks>
	///	alias:
	///		meet?met
	///		map? (for georaphic, use atlas)
	///			but map is tinged of map from one Type to a different Type, so fit in _rel.Map. In rel_._net.Map, we can use mate.
	///		mate?
	///		match
	///		

	/// </remarks>
	/// <typeparam name="T"></typeparam>
	public class Mate<TSrcEl, TTgtEl>
		:
		nilnul.obj.Set1<(TSrcEl, TTgtEl)>
		,
		_mate_.SrcEqI<TSrcEl>
		,
		_mate_.TgtEqI<TTgtEl>
		,
		_mate_.SrcI<TSrcEl>
		,
		_mate_.TgtI<TTgtEl>
		,
		_mate_.HasI<TSrcEl,TTgtEl>
	{
		private IEqualityComparer<TSrcEl> _srcElEq;

		public IEqualityComparer<TSrcEl> srcElEq
		{
			get { return _srcElEq; }
			set { _srcElEq = value; }
		}

		private IEqualityComparer<TTgtEl> _tgtElEq;

		public IEqualityComparer<TTgtEl> tgtElEq
		{
			get { return _tgtElEq; }
			set { _tgtElEq = value; }
		}


		/// <summary>
		/// generate a new set
		/// </summary>
		public nilnul.obj.Set1<TSrcEl> src
		{
			get
			{
				return new Set1<TSrcEl>(
					_srcElEq
					,
					this.Select(x => x.Item1)
				);

			}
		}

		/// <summary>
		/// generate a new set
		/// </summary>
		public nilnul.obj.Set1<TTgtEl> tgt
		{
			get
			{
				return new Set1<TTgtEl>(
					_tgtElEq
					,
					this.Select(x => x.Item2)
				);

			}
		}


		public Mate(IEqualityComparer<TSrcEl> srcEq0, IEqualityComparer<TTgtEl> dstEq1, IEnumerable<(TSrcEl, TTgtEl)> duos)
			:
			base(
				  new nilnul._rel._mate.duo.Eq<TSrcEl, TTgtEl>(srcEq0, dstEq1)
				  ,
			 duos
		)
		{
			_srcElEq = srcEq0;
			_tgtElEq = dstEq1;
		}


		public Mate(IEqualityComparer<TSrcEl> srcEq0, IEqualityComparer<TTgtEl> dstEq1, IEnumerable<Tuple<TSrcEl, TTgtEl>> duos)

			: this(
				  srcEq0, dstEq1
				  ,
			 duos.Select(t => (t.Item1, t.Item2))
		)


		{

		}

		public Mate(IEqualityComparer<TSrcEl> srcEq0, IEqualityComparer<TTgtEl> dstEq1)

			: this(
				  srcEq0, dstEq1
				  ,
			 new (TSrcEl, TTgtEl)[0]
		)


		{

		}



		public void add((TSrcEl, TTgtEl) p)
		{
			base.Add(
				p
			);
		}

		public void add(TSrcEl node, TTgtEl node1)
		{
			add((node, node1));
		}

		public bool has(TSrcEl x, TTgtEl y)
		{
			return base.Contains(
				(x, y)
			);
		}

		public bool hasNot(TSrcEl x, TTgtEl y)
		{
			return _mate_.HasIX.HasNot(this, x, y);
		}

	}

	public class Mate<T> : Mate<T, T>
	{
		public Mate(IEqualityComparer<T> srcEq0) : base(srcEq0, srcEq0)
		{
		}

		public Mate(IEqualityComparer<T> srcEq0,  IEnumerable<(T, T)> duos) : base(srcEq0, srcEq0, duos)
		{
		}

		public Mate(IEqualityComparer<T> srcEq0,  IEnumerable<Tuple<T, T>> duos) : base(srcEq0, srcEq0, duos)
		{
		}
	}
}

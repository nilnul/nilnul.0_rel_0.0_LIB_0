﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel._mate.duo
{
	public class Eq<T, T1>
		: IEqualityComparer<(T, T1)>
	{
		/// <summary>
		/// src means individual. domain means collection
		/// </summary>
		private IEqualityComparer<T> _srcEq;

		public IEqualityComparer<T> srcEq
		{
			get { return _srcEq; }
			set { _srcEq = value; }
		}

		private IEqualityComparer<T1> _dstEq;

		public IEqualityComparer<T1> dstEq
		{
			get { return _dstEq; }
			set { _dstEq = value; }
		}

		public Eq(
			IEqualityComparer<T> srcEq0
			,
			IEqualityComparer<T1> dstEq0
		)
		{
			_srcEq = srcEq0;
			_dstEq = dstEq0;
		}
		public bool Equals((T, T1) x, (T, T1) y)
		{
			return  _srcEq.Equals(x.Item1,y.Item1) && _dstEq.Equals(x.Item2,y.Item2);
		}

		public int GetHashCode((T, T1) obj)
		{
			return _srcEq.GetHashCode(obj.Item1) & _dstEq.GetHashCode(obj.Item2);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel._mate.duo.eq_.srcEqDefaulted
{
	public class DstEqDefaulted<T, T1, TEq,TDstEq> : SrcEqDefaulted<T, T1,TEq>
		where TEq : IEqualityComparer<T>, new()
		where TDstEq : IEqualityComparer<T1>, new()
	{
		public DstEqDefaulted( ) : base(
			
			nilnul.obj_.Singleton<TDstEq>.Instance


			)
		{
		}

		static public DstEqDefaulted<T, T1, TEq,TDstEq> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<DstEqDefaulted<T, T1, TEq,TDstEq>>.Instance;
			}
		}

	}
}

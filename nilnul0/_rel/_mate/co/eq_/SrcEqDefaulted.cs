﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel._mate.duo.eq_
{
	public class SrcEqDefaulted<T, T1, TEq> : Eq<T, T1>
		where TEq : IEqualityComparer<T>, new()
	{
		public SrcEqDefaulted( IEqualityComparer<T1> dstEq0) : base(nilnul.obj_.Singleton<TEq>.Instance, dstEq0)
		{
		}
	}
}

﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.mate_._dict.entry
{

	/// <summary>
	/// monandry
	/// keys are distinct
	/// </summary>
	static public class _PhraseX
	{
		static public string Phrase<TKey,TVal>(TKey key,TVal val) {
			return  $"{key}:{val}";
		}


		static public string Phrase<TKey,TVal>(this (TKey,TVal) dict) {
			return Phrase(dict.Item1,dict.Item2);
		}




	}
}

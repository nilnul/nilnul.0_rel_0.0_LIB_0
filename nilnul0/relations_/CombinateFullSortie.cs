﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.relations_
{

	/// <summary>
	/// given  sets (some of them might be eq), indexed by placing them in a str or named differently,
	///		for each arity, we combinate(per the order of the str; not permutate as conversed can be deduced) from the str of sets 
	/// This is useful in validating form input.
	/// </summary>
	public interface ICombinated
	{

	}
}

/*****
 supposing a form with n input controls, some controls might be of the same type /scope:
	by checking the valid of the input n-tup
		,
		we first check whether 0-tup relation is empty.

		 then we check each 1-tup
			(1-tup table is equivalent to a n-tup where all other cols take all possible values. To use 1-tup, we save the storing place / processing time.

			0-tup table is equivalent to a n-tup where all other cols take all possible values.  If 0-tup is nonempty, then that means all values are possible so far; if 0-tup is empty, then that means the eventual n-tup talbe is empty, hence no values is valid, and we can stop the checking right away. 

			)
	,
		then we check each 2-tup
,
	...
		,
		then we check whether the n-tup is in a relation-nAry table.

		

	in implementation, some combination can be ignored,if it allows all.

	


 * 
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel
{
	/* 	we can also abstract this in <see cref="nameof(nilnul.rel._has.Axiomatic)"/>, but that is not simpler, neither well interpretable per the name.
	///
	/// this is placed under <see cref="nameof(nilnul._rel)"/>, in that:
	///		1) this is a further abstraction
	///		2) for a specific relation, the has may have a domain/range smaller than all instances fo the type.
	/// or we can simple use <see cref="nilnul.obj.IRe"/> ?
	///

	the above way of thinking would make one think that nilnul_rel.Re is prior to nilnul.Rel.
	But we want the finite case first, and then extend it to infinite domain.
	So we better put infinite case on the basis of the current namespace.


	******/
	/// <summary>
	/// whether the rel has a pair in its mate.
	/// To pull out this from rel as an extension, so as to facilitate to abstract the rel to be axiomatic if no literal collection is possible in case of infinity.
	/// </summary>
	/// <remarks>
	/// </remarks>
	/// 
	public interface IHas
	{
	}

	/// 


	public interface IHas<T, T1>:IHas { }

	public interface HasI<T,T1> :
		_has_.ReI<T,T1>
		,
		IHas<T,T1>
	{
	}
}

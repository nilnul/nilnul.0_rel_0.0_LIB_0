﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel
{
	static public class DomainX
	{

		static public HashSet<T> Domain<T, T1, TEq>(this HashSet<Tuple<T, T1>> rel, TEq eq)
			where TEq : IEqualityComparer<T>

		{

			return new HashSet<T>(rel.Select(x => x.Item1), eq);



		}



		static public Set_eqDefault<T, TEq> Domain<T, T1, TEq, T1Eq, TDuo>(

			this Rel_elementEqDefault1<T, T1, TEq, T1Eq, TDuo> rel
		)
			where TEq : IEqualityComparer<T>, new()
			where T1Eq : IEqualityComparer<T1>, new()
			where TDuo : Tuple<T, T1>

		{

			return new Set_eqDefault<T, TEq>(rel.Select(x => x.Item1));



		}



		static public Set_eqDefault<T, TEq> Domain<T, TEq, TDuo>(

			this Rel_elementEqDefault<T, TEq, TDuo> rel
		)
			where TEq : IEqualityComparer<T>, new()
			where TDuo : Tuple<T, T>
		{
			return DomainX.Domain<T, T, TEq, TEq, TDuo>(rel);
		}

		static public HashSet<T> Domain<T, T1, TEq, TDuo>(this HashSet<TDuo> rel, TEq eq)
			where TEq : IEqualityComparer<T>
			where TDuo : Tuple<T, T1>
		{
			return new HashSet<T>(rel.Select(x => x.Item1), eq);
		}

		static public HashSet<T> Domain<T, TEq, TDuo>(this HashSet<TDuo> rel, TEq eq)
			where TEq : IEqualityComparer<T>
			where TDuo : Tuple<T, T>
		{
			return new HashSet<T>(rel.Select(x => x.Item1), eq);
		}

		static public HashSet<T> Domain<T, TEq>(this HashSet<Tuple<T, T>> rel, TEq eq)
			where TEq : IEqualityComparer<T>
		{
			return DomainX.Domain<T, T, TEq>(rel, eq);
		}

		static public Set<T, TEq> Domain<T, TEq>(this Rel1<T, TEq> rel)
			where TEq : IEqualityComparer<T>
		{
			return new Set<T, TEq>(
				rel.elementEq,
				rel.Select(x => x.Item1)
			);

		}
	}
}

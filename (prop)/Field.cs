﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel
{
	static public class FieldX
	{

		static public Set_eqDefault<T, TEq> Field<T, TEq, TDuo>(

			this Rel_elementEqDefault<T, TEq, TDuo> rel
		)
			where TEq : IEqualityComparer<T>, new()
			where TDuo : Tuple<T, T>

		{

			return nilnul.set.duo.op.Union<T,TEq>.Singleton.eval(  rel.Domain(),rel.Range()  );



		}





		static public HashSet<T> Field<T, TEq, TDuo>(this HashSet<TDuo> rel, TEq eq)
			where TEq : IEqualityComparer<T>
			where TDuo : Tuple<T, T>

		{
			var r = new HashSet<T>(eq);
			r.UnionWith(rel.Domain<T, TEq, TDuo>(eq));
			r.UnionWith(rel.Range<T, TEq, TDuo>(eq));
			return r;


		}


		static public HashSet<T> Field<T, TEq>(this HashSet<Tuple<T, T>> rel, TEq eq)
			where TEq : IEqualityComparer<T>

		{
			var r = new HashSet<T>(eq);
			r.UnionWith(rel.Domain<T, TEq>(eq));
			r.UnionWith(rel.Range<T, TEq>(eq));
			return r;


		}

		static public Set<T,TEq> Field<T, TEq>(this Rel1<T, TEq> rel)
			where TEq : IEqualityComparer<T>

		{
			var r = new Set<T,TEq>(rel.elementEq);


			r.UnionWith(rel.Domain<T, TEq>());
			r.UnionWith(rel.Range<T, TEq>());
			return r;


		}




	}
}

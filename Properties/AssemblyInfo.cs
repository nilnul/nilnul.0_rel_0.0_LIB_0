using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("nilnul._rel_._LIB_")]
[assembly: AssemblyDescription("relation(a domain or source, a range or target, and a match or a collection of couples)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("nilnul.com")]
[assembly: AssemblyProduct("nilnul._rel_._LIB_")]
[assembly: AssemblyCopyright("Copyright ©  2013")]
[assembly: AssemblyTrademark("nilnul._rel_._LIB_")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6f49f0aa-779f-41ba-a07e-6067501060ba")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.2.0.1")]
//[assembly: AssemblyInformationalVersion("1.0.0")]

[assembly: AssemblyFileVersion("1.0.0.0")]

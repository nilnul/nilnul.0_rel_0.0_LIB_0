﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel
{
	/// <summary>
	/// 
	/// </summary>
	public interface BeI2<TSrc,TTgt>:

		nilnul.obj.BeI1<nilnul.Rel3<TSrc, TTgt>>
	{

	}

	public class Be<TSrc, TTgt> :
		nilnul.obj.Be1<nilnul.Rel3<TSrc, TTgt>>
		,
		BeI2<TSrc, TTgt>
	{
		public Be(Func<Rel3<TSrc, TTgt>, bool> func) : base(func)
		{
		}
	}


}

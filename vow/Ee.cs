﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.vow
{
	public class Ee<TSrc, TTgt>
		: nilnul.obj.vow.Ee1<Rel3<TSrc, TTgt>>
	{
		public Ee(Rel3<TSrc, TTgt> val, VowI2<Rel3<TSrc, TTgt>> vow) : base(val, vow)
		{
		}
	}
}

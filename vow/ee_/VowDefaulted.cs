﻿using nilnul.obj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.vow.ee_
{
	public class VowDefaulted<TSrc, TTgt, TVow>
		: rel.vow.Ee<TSrc, TTgt>
		where TVow : nilnul.rel.VowI<TSrc, TTgt>, new()

	{
		public VowDefaulted(Rel3<TSrc, TTgt> val) : base(val, nilnul.obj_.Singleton<TVow>.Instance)
		{
		}
	}
}

﻿using nilnul.obj.be.vow_.xpN_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be.en_
{
	public class BeDefaulted<TSrc, TTgt, TBe>
		: be.En<TSrc, TTgt>
		where TBe : nilnul.obj.BeI1<nilnul.Rel3<TSrc, TTgt>>, new()
	{
		

		public BeDefaulted(Rel3<TSrc, TTgt> obj) : base(obj, nilnul.obj_.Singleton<TBe>.Instance)
		{
		}
	}
}

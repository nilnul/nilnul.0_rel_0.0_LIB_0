﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be
{
	public class Vow<TSrc, TTgt>
		:
		nilnul.obj.be.Vow4<nilnul.Rel3<TSrc, TTgt>>
		,
		nilnul.rel.VowI<TSrc, TTgt>
	{
		public Vow(obj.BeI1<Rel3<TSrc, TTgt>> be) : base(be)
		{
		}
	}
}

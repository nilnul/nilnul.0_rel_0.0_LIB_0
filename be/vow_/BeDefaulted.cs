﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be.vow_
{
	public class BeDefaulted<TSrc, TTgt, TBe>
		: be.Vow<TSrc, TTgt>
		where TBe : nilnul.obj.BeI1<nilnul.Rel3<TSrc, TTgt>>,new()
	{
		public BeDefaulted() : base(nilnul.obj_.Singleton< TBe>.Instance)
		{
		}
	}
}

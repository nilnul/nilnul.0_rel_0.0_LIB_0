﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_
{
	public interface INet:IRe
	{
	}
	public interface INet<T> : INet, IRe<T,T> { }
	public interface NetI<T> : INet<T>, nilnul.rel._has.ReI<T,T> { }
}

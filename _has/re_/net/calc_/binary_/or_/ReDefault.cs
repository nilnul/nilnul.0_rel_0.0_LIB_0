﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net.calc_.binary_.or_
{
	public class ReDefault<TObj, TRe, TRe1>
		:
		re_.net.calc_.binary_.Or<TObj>
		where TRe : re_.NetI<TObj>, new()
		where TRe1 : re_.NetI<TObj>, new()

	{
		public ReDefault() : base(
			nilnul._obj.typ_.nilable_.unprimable_.singleton_.ByLazy<TRe>.Instance
			,
			nilnul._obj.typ_.nilable_.unprimable_.singleton_.ByLazy<TRe1>.Instance

		)
		{
		}

		static public ReDefault<TObj, TRe, TRe1> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<ReDefault<TObj, TRe, TRe1>>.Instance;
			}
		}

	}
}

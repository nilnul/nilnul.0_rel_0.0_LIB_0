﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net.calc_.binary_
{
	public class And<TObj>
		:
		nilnul.obj.Box1<
			nilnul.rel._has.re_.net.Co<TObj>
		>
		,
		nilnul.rel._has.re_.NetI<TObj>
	{
		public And(Co<TObj> val) : base(val)
		{
		}

		public And(nilnul.rel._has.re_.NetI<TObj> instance1, nilnul.rel._has.re_.NetI<TObj> instance2) : this(
			new Co<TObj>(instance1, instance2)
		)
		{
		}
		public bool re(TObj a, TObj b)
		{
			return boxed.Item1.re(a, b) && boxed.Item2.re(a, b);
			;
		}


	}
}

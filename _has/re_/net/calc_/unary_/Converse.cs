﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net.calc_.unary_
{
	/// <summary>
	/// switch the two args
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class Converse<T> 
		:
		nilnul.obj._form.Arg<re_.NetI<T>>
		,
		nilnul.rel._has.re_.NetI<T>
	{

		public Converse(re_.NetI<T> re):base(re)
		{
		}
		public bool re(T a, T b)
		{
			return arg.re(b,a);
			//throw new NotImplementedException();
		}
	}
}

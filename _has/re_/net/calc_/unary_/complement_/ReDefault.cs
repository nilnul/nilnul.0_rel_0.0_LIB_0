﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net.calc_.unary_.complement_
{
	
	public class ReDefault<T, TRe> : Complement<T>
		where TRe : nilnul.rel._has.re_.NetI<T>, new()
	{
		public ReDefault() : base(nilnul.obj_.Singleton<TRe>.Instance)
		{
		}


		static public ReDefault<T,TRe> Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<ReDefault<T,TRe>>.Instance;
			}
		}

	}
}

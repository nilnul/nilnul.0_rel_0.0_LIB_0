﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net.calc_.unary_
{
	/// <summary>
	/// domain and range is the same. but we get the complement of the mate (supposing universal set is omega, or the cartesian production).
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class Complement<T> 
		: 
		nilnul.obj._form.Arg<re_.NetI<T>>
		,
		nilnul.rel._has.re_.NetI<T>
	{
		//private ReI<T> _arg;

		//public ReI<T> arg
		//{
		//	get { return _arg; }
		//	//set { _arg = value; }
		//}

		public Complement(re_.NetI<T> re):base(re)
		{
		}
		public bool re(T a, T b)
		{
			return !arg.re(a,b);
			//throw new NotImplementedException();
		}
	}

	
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net
{
	public class Co<TObj> : nilnul.obj.Co<nilnul.rel._has.re_.NetI<TObj>>
	{
		public Co(Tuple<NetI<TObj>, NetI<TObj>> tuple) : base(tuple)
		{
		}

		public Co((NetI<TObj>, NetI<TObj>) valTuple) : base(valTuple)
		{
		}

		public Co(NetI<TObj> item1, NetI<TObj> item2) : base(item1, item2)
		{
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// not trans
	/// </summary>
	public interface INontrans
		:
		nilnul._obj.typ.child.calc_.unary_.IComplement<INet,ITrans>

	{
	}
}

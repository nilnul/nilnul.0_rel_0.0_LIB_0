﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_.le.ge_
{
	/// <summary>
	/// aka: inverse, sup,converse
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TOrd"></typeparam>
	public class LeDefault<T, TOrd> :
		re_.net_.poset_.le.Ge<T>

		where TOrd: nilnul.rel._has.re_.net_.poset_.LeI<T>,new()
	{
		public LeDefault() : base(nilnul._obj.typ_.nilable_.unprimable_.singleton_.ByLazy<TOrd>.Instance)
		{
		}


		static public LeDefault<T, TOrd> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<LeDefault<T, TOrd>>.Instance;
			}
		}




	}
}

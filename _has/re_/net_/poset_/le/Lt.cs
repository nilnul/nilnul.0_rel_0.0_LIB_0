﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_.le
{
	public class Lt<T>
		:
		poset.Strict<T>

	{
		public Lt(LeI<T> re) : base(re)
		{
		}
	}
}

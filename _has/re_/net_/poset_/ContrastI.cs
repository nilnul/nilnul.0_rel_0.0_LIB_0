﻿using nilnul.rel._has.re_.net_.poset_._contraster;
using nilnul.rel._has.re_.net_.poset_._contraster._contrast;
using nilnul.rel._has.re_.net_.poset_._contraster;
using nilnul.rel._has.re_.net_.poset_._contraster._contrast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_
{

	/// <summary>
	/// constrast is another way to express poset relation. Its advantage is you got the full information of a pair conparison with one run, while for <see cref="nameof(IPoset)"/> we need to run the <see cref="nameof(PosetI{T}.re)"/> twice(one for the pair, one for the conversed pair)
	/// </summary>
	/// <typeparam name="T"></typeparam>
	//public interface ContrastI<T,TContraster>
	//	:
	//	_contraster_._ContrasterI<T>, PosetI<T> {
	//}

	public interface ContrasterI<T>:_contraster_.ContrastI<T>, PosetI<T> {
	}

	public class Contraster<T>
		:
		nilnul.obj.Box1<_contraster.ContrastI<T>>
		
		,
		ContrasterI<T>
	{
		public Contraster(_contraster.ContrastI<T> val) : base(val)
		{
		}

		public _contraster.ContrastI<T> contraster => boxed;

		public bool re(T a
			, T b)
		{
			return _contraster.ContrastX.Re(boxed, a, b);
			//throw new NotImplementedException();
		}
	}

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_
{
	/// <summary>
	/// cannonical form of sub: left is no more than right. all sub relation among a set is called PartialOrd.
	/// Eq can be derived from le; comparer cannot be derived as some co is not comparable.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <remarks>
	///
	/// </remarks>
	public interface LeI<T> :  PosetI<T>
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_
{
	/// <summary>
	/// total. <seealso cref="nameof(nilnul.obj.IComp)"/>
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// linear
	/// comparer
	public interface LinearI<T>:

		PosetI<T>
		,
		re_.net_.ITotal
		


	{

	}
}

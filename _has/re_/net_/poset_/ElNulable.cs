﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_
{
	/// <summary>
	/// nul el is considered
	/// </summary>
	public interface ElNulableI<T>
		:
		nilnul.rel._has.re_.net_.PosetI<T>
		where T:class
	{
	}
}

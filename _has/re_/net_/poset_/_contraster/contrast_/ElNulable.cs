﻿using nilnul.rel._has.re_.net_.poset_._contraster_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_._contraster.contrast_
{

	/// <summary>
	/// constrast is another way to express poset relation. Its advantage is you got the full information of a pair conparison with one run, while for <see cref="nameof(IPoset)"/> we need to run the <see cref="nameof(PosetI{T}.re)"/> twice(one for the pair, one for the conversed pair)
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface ElNulableI<T>: _contraster.ContrastI<T>
		where T:class// class
		//where T:interface

	{
	}

}

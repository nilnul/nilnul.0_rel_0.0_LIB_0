﻿using nilnul.rel._has.re_.net_.poset_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_._contraster.contrast_.elNulable.onStr_
{
	/// <summary>
	/// nul min
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class NulMax<T>
		:
		OnStr<T>
		
		where T: class
	{


		public NulMax(poset_._contraster.ContrastI<T> val) : base(
	new poset_._contraster.contrast.NulMax<T>(val)
)
		{
		}





	}
}

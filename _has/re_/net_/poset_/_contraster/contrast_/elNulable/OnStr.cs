﻿using nilnul.rel._has.re_.net_.poset;
using nilnul.rel._has.re_.net_.poset_;
using nilnul.rel._has.re_.net_.poset_._contraster_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_._contraster.contrast_.elNulable
{
	/// <summary>
	/// nul min
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class OnStr<T>
		:
		nilnul.obj.Box1<
			ElNulableI<T>	//nulable element 's contrast
		>
		//,
		//PosetI<
		//	IEnumerable<T>
		//>
		,
		ContrastI<IEnumerable<T>> //the str is not nulable
		where T: class
	{

		
		public OnStr(poset_._contraster.contrast_.ElNulableI<T> val) : base(
			(val) //of nulable
		)
		{
		}



		private _contrast.Outlier _contrast_assumeStreams(IEnumerator<T> enumeratorA, IEnumerator<T> enumeratorB)
		{
			while (true)
			{
				enumeratorA.MoveNext();
				enumeratorB.MoveNext();
				if (enumeratorA.Current is null)
				{
					if (enumeratorB.Current is null )
					{
						//end
						return _contrast.Outlier.Eq;// 0;
					}

				}

				var contrasted = boxed.contrast(
					enumeratorA.Current
					,
					enumeratorB.Current
				);

				if (contrasted!=  _contrast.Outlier.Eq)
				{
					return contrasted;
				}
			}



	
		}
		public  _contrast.Outlier _contrast_assumeStreams(IEnumerable<T> x, IEnumerable<T> y)
		{
			return _contrast_assumeStreams(x.GetEnumerator(),y.GetEnumerator());
			//throw new NotImplementedException();
		}

		public  _contrast.Outlier contrast(IEnumerable<T> x, IEnumerable<T> y)
		{
			return _contrast_assumeStreams(
				nilnul.obj.stream.of_.seq_._AppendX.Seq(x)
				,
				nilnul.obj.stream.of_.seq_._AppendX.Seq(y)


			);
			//throw new NotImplementedException();
		}
		public bool re(IEnumerable<T> a, IEnumerable<T> b)
		{
			return this.Re(a,b);

			//var t = contrast(a, b);
			//return !(t is null) && t <= 0;

			//throw new NotImplementedException();
		}


	}
}

﻿using nilnul.rel._has.re_.net_.poset_._contraster;
using nilnul.rel._has.re_.net_.poset_._contraster._contrast;
using nilnul.rel._has.re_.net_.poset_._contraster;
using nilnul.rel._has.re_.net_.poset_._contraster._contrast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_._contraster
{



	public abstract class ContrastA<T> :
		//nilnul.obj.Box1<_contraster.ContrastI<T>>
		//,
		_contraster.ContrastI<T>
		,
		PosetI<T>
	{
		public abstract _contraster._contrast.Outlier  contrast(T x, T y);
		//public ContrasterA(_contraster.ContrastI<T> val) : base(val)
		//{
		//}

		//public _contraster.ContrastI<T> contraster => boxed;

		//public Outlier contrast(T x, T y)
		//{
		//	return boxed.contrast(x, y);
		//}

		public bool re(T a, T b)
		{
			return _contraster.ContrastX.Re(this, a, b);

		}


	}

	
}

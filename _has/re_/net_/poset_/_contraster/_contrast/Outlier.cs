﻿using nilnul.rel._has.re_.net_.poset_._contraster._contrast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_._contraster._contrast
{


	public enum Outlier
	//:sbyte
	{
		Lt = -1
			,
		Eq
			,
		Gt
			,
		/// <summary>
		/// Un?
		/// </summary>
		Incomparable

	}

	static public class OutlierX
	{
		static public Outlier OfCompared(int compared)
		{
			return compared switch
			{
				0 => Outlier.Eq
				,
				< 0 => Outlier.Lt
				,
				_ => Outlier.Gt

			};
		}

		static public Outlier Converse(Outlier compared)
		{
			return compared switch
			{
				Outlier.Incomparable => Outlier.Incomparable
				,

				_ => (Outlier)(-(int)compared)

			};
		}


	}

}

﻿using nilnul.rel._has.re_.net_.poset_._contraster;
using nilnul.rel._has.re_.net_.poset_._contraster._contrast;
using nilnul.rel._has.re_.net_.poset_._contraster;
using nilnul.rel._has.re_.net_.poset_._contraster._contrast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_._contraster
{



	/// <summary>
	/// developer must ensure that the Func fed is really a poset.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class Contrast<T> : nilnul.obj.Box1<Func<T, T, Outlier>>
		,
		ContrastI<T>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="val">developer must bensure that this is a poset</param>
		public Contrast(Func<T, T, Outlier> val) : base(val)
		{
		}

		public Outlier contrast(T x, T y)
		{
			return boxed(x, y);
			//throw new NotImplementedException();
		}
	}
}

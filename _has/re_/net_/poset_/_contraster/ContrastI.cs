﻿using nilnul.rel._has.re_.net_.poset_._contraster._contrast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_._contraster
{


	public interface ContrastI<T>
	{
		Outlier contrast(T x, T y);
	}

	static public class ContrastX
	{
		/// <summary>
		/// le
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="c"></param>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		static public bool Re<T>(this _contraster.ContrastI<T> c, T a, T b)
		{
			var contrasted = c.contrast(a, b);

			return contrasted <= Outlier.Eq;
			//return !(contrasted is null) && contrasted.Value <= 0;
		}
		static public bool Ge<T>(this _contraster.ContrastI<T> c, T a, T b)
		{
			var contrasted = c.contrast(a, b);

			return contrasted >= Outlier.Eq && contrasted != Outlier.Incomparable;
			//return !(contrasted is null) && contrasted.Value <= 0;
		}

		static public bool Ne<T>(this _contraster.ContrastI<T> c, T a, T b)
		{
			var contrasted = c.contrast(a, b);

			return contrasted != Outlier.Eq;
			//return !(contrasted is null) && contrasted.Value <= 0;
		}



	}
}

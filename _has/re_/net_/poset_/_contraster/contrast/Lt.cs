﻿using nilnul.rel._has.re_.net_.poset;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#nullable enable

namespace nilnul.rel._has.re_.net_.poset_._contraster.contrast
{
	public class Lt<T>
		: nilnul.obj.Box1<poset_._contraster.ContrastI<T>>
	{
		public Lt(ContrastI<T> val) : base(val)
		{
		}

		public bool lt(T x, T y) {
			return  boxed.contrast(x, y)== _contrast.Outlier.Lt;
			//return !(t is null) && t.Value < 0;
		}
	}
}

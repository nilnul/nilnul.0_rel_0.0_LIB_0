﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_._contraster.contrast
{
	public interface ExtremI<T>
		:_extrem_.MaxI<T>
		,
		_extrem_.MinI<T>
	{
		
	}
}

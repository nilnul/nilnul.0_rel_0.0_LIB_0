﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_._contraster.contrast._extrem_
{
	public interface MinI<T>
	{
		IEnumerable<T> min(
			IEnumerable<T> set
		);
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_._contraster.contrast._extrem_
{
	public interface MaxI<T>
	{
		IEnumerable<T> max(
			IEnumerable<T> set
		);
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_._contraster.contrast.nulMin_
{
	public class ContrastDefault<T, TContrast>
		:
		NulMin<T>
		where TContrast : poset_._contraster.ContrastI<T>, new()
		where T:class
	{
		public ContrastDefault() : base(nilnul._obj.typ_.nilable_.unprimable_.singleton_.ByLazy<TContrast>.Instance)
		{
		}

		static public ContrastDefault<T, TContrast> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<ContrastDefault<T, TContrast>>.Instance;
			}
		}

	}
}

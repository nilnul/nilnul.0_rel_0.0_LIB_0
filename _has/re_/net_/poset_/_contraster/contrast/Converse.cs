﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_._contraster.contrast
{
	/// <summary>
	/// aka: inverse, sup,converse,ge
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TOrd"></typeparam>
	public class Converse<T> :
		nilnul.obj.Box1<ContrastI<T>>
		,
		ContrastI<T>
		,
		PosetI<T>
	{
		public Converse(ContrastI<T> val) : base(val)
		{
		}

		public  _contrast.Outlier contrast(T x, T y)
		{
			return boxed.contrast( y,x);

		}

		public bool re(T a, T b)
		{
			return ContrastX.Re(this, a, b);
			//return boxed.re(b, a);
			//throw new NotImplementedException();
		}
	}
}

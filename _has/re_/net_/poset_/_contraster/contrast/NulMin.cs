﻿using nilnul.rel._has.re_.net_.poset;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#nullable enable

namespace nilnul.rel._has.re_.net_.poset_._contraster.contrast
{
	public class NulMin<T>
		: nilnul.obj.Box1<poset_._contraster.ContrastI<T>>
		,
		poset_._contraster.contrast_.ElNulableI<T>
		where T:class
	{
		public NulMin(ContrastI<T> val) : base(val)
		{
		}

		public _contrast.Outlier contrast(T x, T y)
		{
			if (x is null)
			{
				if (y is null)
				{
					return _contrast.Outlier.Eq;
				}
				return _contrast.Outlier.Lt;
			}
			if (y is null)
			{
				return  _contrast.Outlier.Gt;
			}
			return boxed.contrast(x, y);

			//throw new NotImplementedException();
		}

		
	}
}

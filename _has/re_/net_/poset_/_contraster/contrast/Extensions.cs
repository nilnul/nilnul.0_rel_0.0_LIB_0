﻿using nilnul.obj.seq;
using nilnul.obj.seq.be_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_._contraster.contrast
{
	static public class _ExtensionsX
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="contrast"></param>
		/// <param name="set">a poset</param>
		/// <returns>
		/// there might be equiv els.
		/// </returns>
		static public IEnumerable<T> Max<T>(
			this ContrastI<T> contrast
			,
			IEnumerable<T> set
		) {
			return set.Where(
				i=> set.None(
					a=> contrast.contrast(a,i) ==  _contrast.Outlier.Gt
				)
			);

		}
		static public IEnumerable<T> Min<T>(
			this ContrastI<T> contrast
			,
			IEnumerable<T> set
		) {
			return Max(
				new re_.net_.poset_._contraster.contrast.Converse<T>(
				contrast
				)
				,
				set
			);

		}

	}
}

﻿using nilnul._rel.re_.net_.poset_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._rel.re_.net_.poset.elNulable.contrast
{
	/// <summary>
	/// nul min
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class OnStr_bySeq<T>
		:
		nilnul.obj.Box1<poset_.Contrast<T>>	/// <see cref="nameof(t)"/> is nulable

		,
		ContrastI<IEnumerable<T>> //str is nonnulable
	{

		public OnStr_bySeq(Contrast<T> val) : base(val)
		{
		}
		public OnStr_bySeq(poset.ElNulableI<T> val) : this(new poset_.Contrast<T>(val))
		{
		}

		private int? contrast(IEnumerator<T> enumeratorA, IEnumerator<T> enumeratorB)
		{

			while (enumeratorA.MoveNext())
			{
				if (enumeratorB.MoveNext())
				{
					//var le = boxed.re(
					//	enumeratorA.Current
					//	,
					//	enumeratorB.Current
					//);

					//var ge = boxed.re(
					//	enumeratorB.Current
					//	,
					//	enumeratorA.Current
					//);
					var contrasted = this.boxed.contrast(enumeratorA.Current, enumeratorB.Current);

					//if (contrasted is null)
					//{
					//	return null;
					//}
					//if (contrasted < 0)
					//{
					//	return -1;
					//}
					//if (contrasted > 0)
					//{
					//	return 1;
					//}
					if (contrasted == 0)
					{
						continue;
					}
					return contrasted;

				}
				else
				{
					return 1;
				}
			}
			if (enumeratorB.MoveNext())
			{ return -1; }
			return 0;
		}

		public int? contrast(IEnumerable<T> x, IEnumerable<T> y)
		{
			return contrast(x.GetEnumerator(),y.GetEnumerator());
			//throw new NotImplementedException();
		}
		private bool _re(IEnumerator<T> enumeratorA, IEnumerator<T> enumeratorB)
		{

			while (enumeratorA.MoveNext())
			{
				if (enumeratorB.MoveNext())
				{
					
					var contrasted = this.boxed.contrast(enumeratorA.Current, enumeratorB.Current);

					if (contrasted is null)
					{
						return false;
					}
					if (contrasted < 0)
					{
						return true;
					}
					if (contrasted > 0)
					{
						return false;
					}
					//if (contrasted ==0)
					//{
					//continue;
					//}
				}
				else
				{
					return false;
				}
			}
			return enumeratorB.MoveNext();
		}

		public bool re(IEnumerable<T> a, IEnumerable<T> b)
		{
			var enumeratorA = a.GetEnumerator();
			var enumeratorB = b.GetEnumerator();
			return _re(enumeratorA, enumeratorB);

			//throw new NotImplementedException();
		}
	}
}

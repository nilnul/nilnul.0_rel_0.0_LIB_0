﻿using nilnul.rel._has.re_.net_.poset_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset_.elNulable.contrast.onStr_
{
	/// <summary>
	/// nul min
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class NulMax<T>
		:
		OnStr<T>
		
		where T: class
	{

		
		public NulMax(PosetI<T> val) : base(new poset.NulMax<T>(val))
		{
		}

		

	


	}
}

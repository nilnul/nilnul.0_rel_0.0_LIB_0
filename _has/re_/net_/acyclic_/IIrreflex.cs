﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.acyclic_
{
	public interface IIrreflex
		:ICyclic4none
		,
		nilnul._obj.typ.child.calc_.unary_.IAlias<ICyclic4none,IIrreflex> /// if any co is reflexive, then this is cyclic
	{
	}
}

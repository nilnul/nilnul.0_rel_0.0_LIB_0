﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.acyclic_
{
	/// <summary>
	/// eg:
	///		a->b, b->a
	/// </summary>
	public interface INonreflex
		:net_.ICyclic4none
		,
		_obj.typ.child.calc_.unary_.comple_.IReflec<ICyclic4none, ReflexException>

		,
		net_.INonreflex
	{
	}
}

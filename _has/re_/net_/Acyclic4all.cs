﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	///
	/// no cyclic
	/// </summary>
	/// <remarks>
	/// alias:
	/// </remarks>
	public interface IAcyclic4all

		:
		_obj.typ.calc_.unary_.IAlias< ICyclic4none>
		,
		_obj.typ.child.calc_.unary_.IComplement<INet, ICyclic4some>
		,
	
		INet

		
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.compat_
{
	/// <summary>
	/// eg:
	///		interest group of students : {{a,b},{b,c}}.
	///
	///  Is a co in some group?
	///		
	///		(a,a),(b,b),(c,c)
	///		,
	///		(a,b)
	///		,
	///		(b,a)
	///		,
	///		(b,c)
	///		,
	///		(c,b)
	///
	/// (a, b) & (b,c) cannot deduce (a,c)
	///		
	/// </summary>
	class Nontrans
	{
	}
}

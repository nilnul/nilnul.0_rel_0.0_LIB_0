﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.nonreflex_
{
	/// <summary>
	/// eg:
	///		a->b, b->a
	/// </summary>
	public interface ICyclic
		:
		INonreflex
		,
		_obj.typ.child.calc_.unary_.IComplement<INonreflex, IAcyclic>
		
		,
		net_.ICyclic4all

	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// when x!=y, 
	///		if (x,y) then there is no (x,y)
	/// </summary>
	public interface IAntisym:INet
	{
	}
}

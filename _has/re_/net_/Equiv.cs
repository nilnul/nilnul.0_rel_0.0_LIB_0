﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// equivalence relation
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface EquivI<T>:

		IReflex
		,
		ISym
		,
		ITrans
		,IPreord
		,
		ICompatible
		,
		PosetI<T>
		,
		NetI<T>

	{

	}
}

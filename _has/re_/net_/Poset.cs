﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// partial ordered set. use poset to emphasize this is a set, and regarde <see cref="nameof(PosetI{T}.re)"/> as memberOf set predicate.
	/// </summary>
	public interface IPoset : INet { }
	public interface IPoset<T> : IPoset, re_.INet<T> { }


}

﻿using nilnul.rel._has.re_.net_.poset_;
using nilnul.rel._has.re_.net_.poset_._contraster._contrast;
using nilnul.rel._has.re_.net_.poset_._contraster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset
{
	
	public class Contrast<T>
		:
		nilnul.obj.Box1<
			PosetI<T>
		>
		//,
		//_contraster_._ContrastI<T>
		,
		ContrastI<T>
		,
		PosetI<T>
	{
		

		public Contrast(PosetI<T> val) : base(val)
		{

		}

		public  Outlier  contrast(
			T x,T y
		) {
			var le = boxed.re(x, y);
			var ge = boxed.re( y,x);

			if (le)
			{
				if (ge)
				{
					return  Outlier.Eq;
				}
				return  Outlier.Lt;
			}
			if (ge)
			{
				return  Outlier.Gt;
			}
			return  Outlier.Incomparable;


		}



		public bool re(T a, T b)
		{
			return poset_._contraster.ContrastX.Re(this, a, b);
			
		}
	}
}

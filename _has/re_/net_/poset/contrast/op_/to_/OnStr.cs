﻿using nilnul.rel._has.re_.net_.poset_;
using nilnul.rel._has.re_.net_.poset_._contraster;
using nilnul.rel._has.re_.net_.poset_._contraster._contrast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset.contrast.op_.to_
{
	/// <summary>
	/// nul min
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class OnStr<T>
		:
		nilnul.obj.Box1<poset.Contrast<T>>
		,
		poset_._contraster.ContrastI<
			IEnumerable<T>
		>
		,
		PosetI<
IEnumerable<T>
			>

	{
		public OnStr(PosetI<T> val) : base(new Contrast<T>(val))
		{
		}

		private poset_._contraster._contrast.Outlier contrast(IEnumerator<T> enumeratorA, IEnumerator<T> enumeratorB)
		{

			while (enumeratorA.MoveNext())
			{
				if (enumeratorB.MoveNext())
				{
					
					var contrasted = this.boxed.contrast(enumeratorA.Current, enumeratorB.Current);

					if (contrasted== poset_._contraster._contrast.Outlier.Eq)
					{
						continue;
					}
					return contrasted;

					//if (contrasted is null)
					//{
					//	return null;
					//}
					//if (contrasted < 0)
					//{
					//	return -1;
					//}
					//if (contrasted > 0)
					//{
					//	return 1;
					//}
					//if (contrasted ==0)
					//{
					//continue;
					//}
				}
				else
				{
					return poset_._contraster._contrast.Outlier.Gt;
				}
			}
			if (enumeratorB.MoveNext())
			{
				return poset_._contraster._contrast.Outlier.Lt;// -1;
			}
			return poset_._contraster._contrast.Outlier.Eq;// 0;
		}
		public poset_._contraster._contrast.Outlier contrast(IEnumerable<T> x, IEnumerable<T> y)
		{
			return contrast(
				x.GetEnumerator()
				,
				y.GetEnumerator()
			);
			//throw new NotImplementedException();
		}
		/// <summary>
		/// use contrast in case you need both this and the converse 
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public bool re(IEnumerable<T> a, IEnumerable<T> b)
		{
			return re_.net_.poset_._contraster.ContrastX.Re(this, a, b);
			//var constrasted = contrast(a,b); ;
			//return (!(constrasted is null) && constrasted <= 0);
		}


	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset
{
	/// <summary>
	/// lt. 
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TLe"></typeparam>
	public class Strict<T> :
		nilnul.obj.Box1<PosetI<T>>
		,
		net_.IDominant
		,
		NetI<T>
	{
		public Strict(PosetI<T> val) : base(val)
		{
		}

		public bool re(T a, T b)
		{
			return boxed.re(a, b)  && !boxed.re(b, a);
		}

		

	}
}

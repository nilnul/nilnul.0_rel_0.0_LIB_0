﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset.converse.strict
{
	/// <summary>
	/// ngt, le or incomparable
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TOrd"></typeparam>
	public class Complement<T> :
		poset.strict.Complement<T> //based on converse as poset
		
	{
		public Complement(PosetI<T> re) : base(new Converse<T>(re) )
		{
		}

		

	}
}

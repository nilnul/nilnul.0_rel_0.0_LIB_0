﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset.converse
{
	/// <summary>
	/// nge. lt0incomparable 
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TLe"></typeparam>
	public class Complement<T>
		:
		poset.Complement<T >
		,
		NetI<T>
	{
		public Complement(PosetI<T> val) : base(new Converse<T>(val) )
		{
		}


		

	}
}

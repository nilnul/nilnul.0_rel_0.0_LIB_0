﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset
{
	/// <summary>
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TLe"></typeparam>
	public class Equiv<T> :
		nilnul.obj.Box1<PosetI<T>>,

		net_.EquivI<T>
	{
		private poset.Converse<T> _converse;

		public poset.Converse<T> converse
		{
			get { return _converse; }
			set { _converse = value; }
		}

		public Equiv(PosetI<T> val) : base(val)
		{
			_converse = new Converse<T>(val);
		}

		public bool re(T a, T b)
		{
			return boxed.re(a,b) && _converse.re(a, b) ;
		}
		
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset.nulMax_
{
	public class PosetDefault<T,TContain> :
		poset.NulMax<T>
		
		where T : class
		where TContain: PosetI<T>,new()
	{
		public PosetDefault() : base(nilnul._obj.typ_.nilable_.unprimable_.singleton_.ByLazy<TContain>.Instance)
		{
		}

		static public PosetDefault<T,TContain> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<PosetDefault<T,TContain>>.Instance;
			}
		}



	}
}

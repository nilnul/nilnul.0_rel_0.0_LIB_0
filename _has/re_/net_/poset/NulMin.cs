﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset
{
	/// <summary>
	/// poset of nulable
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class NulMin<T> :
		obj.Box1<re_.net_.PosetI<T>>
		,
		poset_.ElNulableI<T>
		where T : class
	{
		public NulMin(PosetI<T> val) : base(val)
		{
		}

		public bool re(T x, T y)
		{
			if (x is null)
			{
				return true;
			}
			if (y is null)
			{
				return false;
			}
			return boxed.re(x, y);
		//	throw new NotImplementedException();
		}
	}
}

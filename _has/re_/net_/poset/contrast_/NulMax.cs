﻿using nilnul.rel._has.re_.net_.poset_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset.contrast_
{
	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <remarks>
	/// place this under the namespace <see cref="nameof(contrast)"/>, rather than <see cref="nameof(contrast_)"/> , as the underlying type is changed to <see cref="nameof(Nullable{T})"/> 
	/// </remarks>
	public class NulMax<T>
		:
		Contrast<T>
		//, ContrasterI<T>
		where T : class
	{
		public NulMax(PosetI<T> val) : base(
			new poset.NulMax<T>(val)
		)
		{
		}
	}
}

﻿using nilnul.rel._has.re_.net_.poset_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset.contrast_
{
	public class NulMin<T>
		:
		Contrast<T>
		//, ContrasterI<T>
		where T : class
	{
		public NulMin(PosetI<T> val) : base(
			new poset.NulMin<T>(val)
		)
		{
		}
	}
}

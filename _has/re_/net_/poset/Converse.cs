﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset
{
	/// <summary>
	/// aka: inverse, sup,converse,ge
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TOrd"></typeparam>
	public class Converse<T> :
		re_.net.calc_.unary_.Converse<T>
		,
		PosetI<T>
	{
		public Converse(PosetI<T> re) : base(re)
		{
		}

		

	}
}

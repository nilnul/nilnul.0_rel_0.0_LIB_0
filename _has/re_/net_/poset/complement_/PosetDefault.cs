﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset.comple_
{
	/// <summary>
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TOrd"></typeparam>
	public class PosetDefault<T, TOrd> :

		Complement<T>
		where TOrd: nilnul.rel._has.re_.net_.PosetI<T>,new()
	{
		public PosetDefault() : base(
			nilnul._obj.typ_.nilable_.unprimable_.singleton_.ByLazy<TOrd>.Instance
		)
		{
		}


		static public PosetDefault<T, TOrd> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<PosetDefault<T, TOrd>>.Instance;
			}
		}




	}
}

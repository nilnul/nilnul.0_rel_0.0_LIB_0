﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset.strict_
{
	/// <summary>
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TLe"></typeparam>
	public class PosetDefault<T,  TLe> :
		Strict<T>
		where TLe: nilnul.rel._has.re_.net_.PosetI<T>,new()
	{
		public PosetDefault() : base(
			nilnul._obj.typ_.nilable_.unprimable_.singleton_.ByLazy<TLe>.Instance
		)
		{
		}


		static public PosetDefault<T,  TLe> Singleton
		{
			get
			{
				return nilnul._obj.typ_.nilable_.unprimable_.Singleton<PosetDefault<T,  TLe>>.Instance;
			}
		}


	}
}

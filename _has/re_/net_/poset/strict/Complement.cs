﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset.strict
{
	/// <summary>
	/// nlt. ge or incomparale
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TOrd"></typeparam>
	public class Complement<T> :
		re_.net.calc_.unary_.Complement<T>
		
	{
		public Complement(PosetI<T> re) : base(new Strict<T>(re) )
		{
		}

		

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset
{
	/// <summary>
	/// not le. might be gt, or incomparable.
	/// For gt, this is a stricOrd, or dominant relation, which is irreflexive/asymmetric/transitive; for incomparable, it's irreflexive/symmetric/ ( might be not transitive) relation.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TOrd"></typeparam>
	public class Complement<T> :
		re_.net.calc_.unary_.Complement<T>
		,
		
		IIrreflex
		

		//,
		//PosetI<T>
	{
		public Complement(PosetI<T> re) : base(re)
		{
		}

		

	}
}

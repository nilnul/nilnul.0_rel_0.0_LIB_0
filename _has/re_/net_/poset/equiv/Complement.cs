﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset.equiv
{
	/// <summary>
	/// not equiv, might be lt, gt, or incomparable
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TOrd"></typeparam>
	public class Complement<T> :
		re_.net.calc_.unary_.Complement<T>
		
	{
		public Complement(PosetI<T> re) : base(new Equiv<T>(re) )
		{
		}

		

	}
}

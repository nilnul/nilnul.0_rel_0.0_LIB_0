﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.poset
{
	/// <summary>
	/// poset of nulable
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class NulMax<T> :
		obj.Box1<PosetI<T>>
		,
		poset_.ElNulableI<T>

		where T : class
	{
		public NulMax(PosetI<T> val) : base(val)
		{
		}

		public bool re(T x, T y)
		{
			if (x is null)
			{
				return false;
			}
			if (y is null)
			{
				return true;
			}
			return boxed.re(x, y);
		//	throw new NotImplementedException();
		}
	}
}

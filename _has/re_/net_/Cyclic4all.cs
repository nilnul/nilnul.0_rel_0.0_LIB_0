﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// every edge is in a circle.
	/// (not every end is in  a circle, as for {a}, there is no circle if we dont take empty circle.)
	/// </summary>
	/// <remarks>
	/// no edge is dangling; ie, the edge can always find way back.
	/// alias:
	///		cyclic
	///		cyclics
	///		cyclic4each
	///		nondeadend
	///		nondangling
	///		
	/// </remarks>
	public interface ICyclic4all /*4each*/

		:
	#region all might be none
		//ICyclic4some
		//,
		//_obj.typ.child.calc_.unary_.IComplement<INet,IAcyclic4some>

	#endregion

		_obj.typ.child.calc_.unary_.IAlias<INet, IAcyclic4none>
		,
		_obj.typ.child.calc_.unary_.IComplement<INet, IAcyclic4some>
		,

		INet

		
	{
	}
}

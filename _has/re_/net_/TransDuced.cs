﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// there is no short cut.
	/// transitive reduced.
	/// Note:
	///		for a->b, b->c, c->d,   a -> d
	///			, it's not transitive
	///			, it's not intrans, as it is still reducible in transitivity.
	///		We can see here this doesnot mean that only triangular transitive is avoided; in fact this means any polyline transitive is avoided.
	///	counter eg:
	///		a->a
	///		
	/// </summary>
	/// <remarks>
	/// alias:
	///		intrans
	///		transduced
	///		tranduced
	///		
	/// </remarks>
	public interface ITransReduced
		:INet
		,
		_obj.typ.child.calc_.unary_.IComplement<INet, net_.ITransReducible>

	//:
	#region eg: a->b is intrans, and trans. {} is intrans and trans
	//INontrans

	#endregion
	{
	}
}

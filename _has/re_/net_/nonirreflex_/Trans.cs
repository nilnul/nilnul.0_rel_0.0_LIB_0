﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.nonirreflex_
{
	/// <summary>
	/// eg:
	///		a->a 
	/// </summary>
	public interface ITrans
		:
		IReflex4some
		,

		nilnul._obj.typ.child.calc_.unary_.IComplement<IReflex4some, INontrans> 
		,
		_obj.typ.calc_.unary_.IAlias<trans_.IAcyclic>

		,
		net_.ITrans
		


		
	{
	}
}

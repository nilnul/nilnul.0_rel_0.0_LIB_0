﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.nonirreflex_
{
	public interface ICyclic
		:
		IReflex4some
		,
		nilnul._obj.typ.child.calc_.unary_.IAlias<IReflex4some,ICyclic> /// if it's nonirreflex, then it's cyclic
	{
	}
}

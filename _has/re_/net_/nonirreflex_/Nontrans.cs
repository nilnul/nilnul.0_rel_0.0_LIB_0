﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.nonirreflex_
{
	/// <summary>
	/// eg:
	///		a->a, a->b, b->c
	/// </summary>
	public interface INontrans
		:
		IReflex4some
		,
		
		_obj.typ.child.calc_.unary_.IComplement<
			IReflex4some,
			re_.net_.nonirreflex_.ITrans
		>
		,
		net_.INontrans

	{
	}
}

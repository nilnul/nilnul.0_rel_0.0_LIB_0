﻿namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// a partial order. might be nontotal.
	/// If this is definitely total, <see cref="nameof(obj.IComp)"/>
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface PosetI<T>:

		IReflex
		,
		IAntisym
		,
		ITrans
		,IPreord
		,
		NetI<T>
		,
		IPoset<T>
	{

	}


}

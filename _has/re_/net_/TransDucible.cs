﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// still reducible with respect to transitivity.
	/// eg: a->b, b->c, c->d,   a -> d
	///		 it's not transitive,
	///		 it's not intrans, as it is still reducible in transitivity.
	/// </summary>
	/// <remarks>
	/// alias:
	///		nonintrans
	///		transducible
	///		tranducible
	/// </remarks>
	public interface ITransReducible:INet
		,
		_obj.typ.child.calc_.unary_.IComplement<INet, ITransReduced>
	{
	}
}

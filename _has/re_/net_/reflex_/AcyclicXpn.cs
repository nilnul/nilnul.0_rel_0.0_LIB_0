﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.reflex_
{

	[Serializable]
	public class AcyclicException : Exception
		,
		IReflex

	{
		public AcyclicException() { }
		public AcyclicException(string message) : base(message) { }
		public AcyclicException(string message, Exception inner) : base(message, inner) { }
		protected AcyclicException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

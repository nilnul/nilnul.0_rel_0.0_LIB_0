﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// total. every co, or the converse of that pair,  is inside the relation. some co is not in, but its converse is in.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <remarks>
	/// vs:
	///		<see cref="nameof(net_.IComplete)"/>
	///			any co is true
	///				erstwhile <see cref="ITotal"/> returns true for either co or co.converesed, but might not for both.
	/// </remarks>
	///
	public interface ITotal:IRe { }
	public interface TotalI<T>:

		ReI<T>
		,ITotal


	{

	}
}

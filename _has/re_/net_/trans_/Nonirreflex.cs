﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.trans_
{
	/// <summary>
	/// 
	/// </summary>
	public interface INonirreflex
		:
		ITrans,

		_obj.typ.child.calc_.unary_.IComplement<ITrans, IIrreflex>
		,
		net_.IReflex4some
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.trans_.asym_
{
	/// <summary>
	///  a transitive relation is
	///		asymmetric if and only if it is irreflexive
	/// </summary>
	public interface Iirreflex:
		
		IAsym
		,
		net_.IIrreflex
		,
		_obj.typ.child.calc_.unary_.IAlias<IAsym, Iirreflex>
		,
		_obj.typ.child.calc_.unary_.comple_.IReflec<IAsym,NonirreflexException>
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.trans_.irreflex_
{
	/// <summary>
	/// 
	/// </summary>
	public interface IAsym
		:
		nilnul._obj.typ.child.calc_.unary_.comple_.IReflec<IIrreflex, Sym4someException>	//if a co is symmetric, by transitivity, the el is reflex, resulting a contradiction.
		,
		IIrreflex
	{
	}
}

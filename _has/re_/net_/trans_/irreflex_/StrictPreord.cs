﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.trans_.irreflex_
{
	/// <summary>
	/// A binary relation is a strict preorder if and only if it is a strict partial order.
	/// </summary>
	/// <remarks>
	/// alias:
	///		stripPreord?
	///			strictPreord is not preord_.Strict. i.e, strictPreord is not preord.
	/// </remarks>
	public interface IStrictPreord
		:
		nilnul.obj.IAlias< trans_.asym_.irreflex_.IStrictPoset>
		,
		nilnul.obj.IAlias<IIrreflex>
		,
		IIrreflex
	{
	}
}

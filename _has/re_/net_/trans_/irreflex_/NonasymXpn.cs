﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.trans_.irreflex_
{

	/// <summary>
	/// must be asymmetric. If there were some symmetry, then by trans, the el involved would be reflexive, resulting contradiction to <see cref="nameof(irreflex_)"/>
	/// </summary>
	[Serializable]
	public class Sym4someException :
		Exception
		,
		IIrreflex
	{
		public Sym4someException() { }
		public Sym4someException(string message) : base(message) { }
		public Sym4someException(string message, Exception inner) : base(message, inner) { }
		protected Sym4someException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

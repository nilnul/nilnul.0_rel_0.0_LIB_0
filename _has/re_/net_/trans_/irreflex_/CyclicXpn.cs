﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.re_.net_.trans_.irreflex_
{
	/// <summary>
	/// if it's cyclic, then there is cycle. then there is reflexivity and symmetry.
	/// </summary>

	[Serializable]
	public class CyclicException : Exception
		,
		net_.trans_.IIrreflex
	{
		public CyclicException() { }
		public CyclicException(string message) : base(message) { }
		public CyclicException(string message, Exception inner) : base(message, inner) { }
		protected CyclicException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}

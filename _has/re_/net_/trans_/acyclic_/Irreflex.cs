﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.trans_.acyclic_
{
	public interface IIrreflex
		:IAcyclic
		,
		nilnul._obj.typ.child.calc_.unary_.comple_.IReflec<IAcyclic, acyclic_.NonirreflexException>
		,
		trans_.IIrreflex
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// not cyclic4each
	/// </summary>
	/// <remarks>
	/// alias:
	///		acyclic4some
	/// </remarks>
	public interface IAcyclic4some
		:
		_obj.typ.child.calc_.unary_.IComplement<INet, IAcyclic4none>
		,
		_obj.typ.child.calc_.unary_.IComplement<INet, ICyclic4all>

		,
		INet
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// not reflexive for every co. 
	/// </summary>
	/// <remarks>
	/// alias:
	///		reflex 4 none
	/// </remarks>
	public interface IIrreflex:INonreflex
	{
	}
}

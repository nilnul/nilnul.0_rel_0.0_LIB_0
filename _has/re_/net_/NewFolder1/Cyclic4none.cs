﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// no circle is found should we allow transitivity. cyclic for none
	///		
	/// </summary>
	/// <remarks>
	/// alias:
	///		cyclic4none
	///		acyclic:
	///			"a" means all none.
	/// </remarks>
	///
	[Obsolete(nameof(IAcyclic4all))]
	public interface ICyclic4none
		:
	#region both cylic4all and cyclic4none/Acyclic4all, as all is none: {}
		//IAcyclic4some
		//,

	#endregion
		nilnul._obj.typ.child.calc_.unary_.IComplement<INet, ICyclic4some>
		,
		nilnul._obj.typ.child.calc_.unary_.IAlias<INet, IAcyclic4all>

		,
		INet
	{
	}
}

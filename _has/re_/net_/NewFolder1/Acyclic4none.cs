﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// all is cyclic
	/// </summary>
	/// <remarks>
	/// alias:
	/// </remarks>
	///
	[Obsolete(nameof(ICyclic4all))]
	public interface IAcyclic4none
		:
		INet
		,
		_obj.typ.child.calc_.unary_.IAlias<INet,ICyclic4all>
	{
	}
}

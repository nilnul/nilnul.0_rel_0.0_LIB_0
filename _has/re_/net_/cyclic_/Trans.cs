﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.cyclic_
{
	/// <summary>
	/// eg:
	///		a->a
	/// </summary>
	public interface ITrans
		:net_.ICyclic4all
		,
		net_.ITrans
	{
	}
}

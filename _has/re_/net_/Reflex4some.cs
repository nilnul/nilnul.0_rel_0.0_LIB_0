﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// some co is reflexive
	/// </summary>
	/// <remarks>
	/// alias:
	///		reflex4some
	/// </remarks>
	public interface IReflex4some
		:INet
		,
		_obj.typ.child.calc_.unary_.IComplement<INet, IReflex4some>
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// mark the <see cref="nameof(obj.IRe)"/> as reflexive everywhere.
	/// </summary>
	public interface IReflex:IReflex4some
		,
		INet
	{
	}
}

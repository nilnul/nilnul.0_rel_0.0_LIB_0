﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.nontrans_
{
	/// <summary>
	///  eg:
	///		a->b
	/// </summary>
	public interface IAcyclic
		:
		INontrans
		,
		net_.ICyclic4none
		,
		_obj.typ.child.calc_.unary_.IComplement<INontrans, ICyclic>
		
	{}
}

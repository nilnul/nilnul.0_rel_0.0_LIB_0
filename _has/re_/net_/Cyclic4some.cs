﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	///not acyclic. ie, some cirle is found
	/// </summary>
	/// <remarks>
	/// alias:
	///		cyclic4some
	/// </remarks>
	public interface ICyclic4some
		:INet
		,
		_obj.typ.child.calc_.unary_.IComplement<INet, ICyclic4none>
		,
		_obj.typ.child.calc_.unary_.IComplement<INet, IAcyclic4all>

	{
	}
}

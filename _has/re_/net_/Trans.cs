﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// transitive everywhere.
	/// </summary>
	/// <remarks>
	/// alias:
	///		tran4any
	/// </remarks>
	public interface ITrans
		:INet
		//:
		
	#region empty match makes a rel trans and intrans,
		///eg: a->b, b->c, a->c is trans and nonintrans
		///eg: {  }
		///eg:  a -> b, is trans and intrans
		///
		///eg: a -> b, b -> c, is nontrans, and intrans
		///eg: a->b, b->c, c -> d, a ->d is nontrans, and nonintrans
		//INonintrans

	#endregion
	{
	}
}

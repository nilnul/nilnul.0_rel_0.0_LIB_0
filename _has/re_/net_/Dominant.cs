﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_
{
	/// <summary>
	/// strict preord; strict poset
	/// </summary>
	/// <remarks>
	/// alias:
	///		
	/// </remarks>
	public interface IDominant
		:
		INet
		,
		ITrans
		,
		IIrreflex
		//,IAsym
		//,IAcyclic4all
	
	{
	}
}

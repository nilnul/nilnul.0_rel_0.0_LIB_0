﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.irreflex_
{
	/// <summary>
	/// eg:
	///		a->b, b->c, a->c
	/// </summary>
	public interface ITrans
		:
		IIrreflex
		,

		nilnul._obj.typ.child.calc_.unary_.IComplement<IIrreflex, INontrans> /// hence, Acyclic
		,
		_obj.typ.calc_.unary_.IAlias<trans_.IAcyclic>

		,
		IAcyclic
		


		
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.irreflex_.cyclic_
{
	/// <summary>
	/// 
	/// </summary>
	public interface INontrans
		:
		ICyclic
		,

		nilnul._obj.typ.child.calc_.unary_.comple_.IReflec<
			ICyclic,
			cyclic_.TransException
		>  // this means cyclic_.Intrans =  Cyclic; doesnot mean intrans = cyclic.
		,
		_obj.typ.calc_.unary_.IAlias<
			irreflex_.nontrans_.ICyclic
		>
		,
		irreflex_.INontrans

	{
	}
}

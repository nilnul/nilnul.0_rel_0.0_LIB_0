﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.irreflex_.nontrans_
{
	/// <summary>
	/// eg:
	///		a -> b, b -> c
	/// </summary>
	public interface IAcyclic
		:
		INontrans
		,
		_obj.typ.child.calc_.unary_.IComplement<INontrans, ICyclic >
		,

		_obj.typ.calc_.unary_.IAlias<acyclic_.INontrans>
		,
		irreflex_.IAcyclic
		
	{
	}
}

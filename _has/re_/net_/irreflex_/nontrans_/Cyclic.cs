﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.irreflex_.nontrans_
{
	/// <summary>
	/// eg:
	///		a->b, b->a
	/// </summary>
	public interface ICyclic
		:
		INontrans
		,
		nilnul._obj.typ.child.calc_.unary_.IComplement<INontrans,IAcyclic>
		,
		_obj.typ.calc_.unary_.IAlias<cyclic_.INontrans> // further means: alias<irreflexive_.Cyclic >
		,
		irreflex_.ICyclic
		

	{
	}
}

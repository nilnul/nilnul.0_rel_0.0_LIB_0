﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.irreflex_
{
	/// <summary>
	/// there is cycle should we allow transitivity
	/// eg:
	///		a->b, b->a
	/// </summary>
	public interface ICyclic
		:
		IIrreflex
		,
		_obj.typ.child.calc_.unary_.IComplement<IIrreflex, IAcyclic>
		,
		_obj.typ.calc_.unary_.IAlias<cyclic_.INontrans>
		,
		INontrans

	{
	}
}

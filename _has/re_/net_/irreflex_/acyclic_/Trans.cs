﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.irreflex_.acyclic_
{
	/// <summary>
	/// eg:
	///		a -> b, b -> c, a -> c
	/// </summary>
	public interface ITrans
		:
		
		IAcyclic
		,
		_obj.typ.child.calc_.unary_.IComplement<IAcyclic,INontrans>
		,
		_obj.typ.calc_.unary_.IAlias<trans_.IAcyclic>
		,
		_obj.typ.calc_.unary_.IAlias<irreflex_.ITrans >

		,
		irreflex_.ITrans
	{
	}
}

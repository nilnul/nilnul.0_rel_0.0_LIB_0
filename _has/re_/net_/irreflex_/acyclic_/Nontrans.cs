﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.irreflex_.acyclic_
{
	/// <summary>
	/// eg:
	///		a -> b, b -> c
	/// </summary>
	public interface INontrans
		:
		
		IAcyclic
		,
		nilnul._obj.typ.calc_.unary_.IAlias<nontrans_.IAcyclic>
		,
		_obj.typ.child.calc_.unary_.IComplement<IAcyclic, acyclic_.ITrans>
		,
		irreflex_.INontrans

	{
	}
}

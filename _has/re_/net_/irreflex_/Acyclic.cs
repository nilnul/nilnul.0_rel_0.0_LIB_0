﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.irreflex_
{
	/// <summary>
	/// eg:
	/// </summary>
	public interface IAcyclic
		:
		IIrreflex
		,
		nilnul._obj.typ.child.calc_.unary_.IComplement<IIrreflex, ICyclic>
		// ,
		//nilnul._obj.typ.calc_.binary_.IOr<acyclic_.IIntrans,acyclic_.ITrans>
		,
		_obj.typ.child.calc_.binary_.IOr<IAcyclic, acyclic_.INontrans,acyclic_.ITrans>
	{}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re_.net_.irreflex_
{
	/// <summary>
	/// 
	/// </summary>
	public interface INontrans
		:
		
		_obj.typ.child.calc_.unary_.IComplement<
			IIrreflex,
			re_.net_.irreflex_.ITrans
		>
		//,
		//_obj.typ.calc_.binary_.IOr<intrans_.ICyclic,intrans_.IAcyclic>
		,
		_obj.typ.child.calc_.binary_.IOr<INontrans, nontrans_.ICyclic,nontrans_.IAcyclic>
		,
		IIrreflex

	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has.re
{
	static public class _ConverseX
	{
		static public bool Converse<S,T>(this Func<T,S, bool> re, S s, T t) {
			return re(t, s);
		}
	}
}

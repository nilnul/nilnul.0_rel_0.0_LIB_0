﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul
{
	/// <summary>
	/// the extension of rel, into infinite
	/// </summary>
	public interface IRe
	{
	}
	public interface IRe<T,T1> : IRe { }

	public interface ReI1<T,T1> : IRe<T,T1> {
		bool re(T a, T1 b);
	}
}

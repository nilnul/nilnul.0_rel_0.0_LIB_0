﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel._has
{
	/*
	 ///
	 
	 */
	/// <see cref="nilnul.set._has.CollectionI{T}"/>. We follow that methodology to define <see cref="nilnul.rel._has.re_"/>, and then we can extend it to infinite.
	/// <summary>
	/// use <see cref="nameof(IRe)"/> to denote whether the co is contained in a relation.
	/// If we take all instances of a type as the set for domain or range, this is a rel for possibley infinite sets or finite sets.
	/// </summary>
	/// <remarks>
	/// <see cref="nameof(nilnul.IRel)"/> is a finite set. So the "has" can be implicitly implemented there; Then why we  define this prior to the definition of rel?
	///		eg:
	///			for nomVer strings, the whole collection is infinite. But we often deal with a finite subset. And the "has" is valid for any subset (finite or infinite). So we define the "has" relation outside a specific finite subset, and define it with respect to the whole infinite omega set.
	///				Then when we deal with a specific finite subset, we just "plug" this "has" member into that finite list of elements.
	/// </remarks>
	public interface  IRe
		:nilnul.obj.IRe
	{

	}

	/// <summary>
	/// the domain is all the instances of <typeparamref name="T"/>
	/// the range is all the instances of <typeparamref name="T1"/>
	/// 
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="T1"></typeparam>
	public interface IRe<T, T1> : IRe, nilnul.obj.IRe<T, T1> { }

	public interface ReI<T, T1>:IRe<T,T1>, nilnul.obj.ReI<T,T1> { }
}

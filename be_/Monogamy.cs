﻿using nilnul.relation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be_
{
	/// <summary>
	/// the match is monogamy.
	/// </summary>
	/// <remarks>
	/// vs Bijective:
	///		monogamy relation may be not surjective, or not total/complete
	/// </remarks>
	public class Monogamy<TSrc, TTgt> :

		nilnul.obj_.singular.Heir<Monogamy<TSrc,TTgt>>
		,
		nilnul.rel.BeI2<TSrc, TTgt>
	{
		public bool be(Rel3<TSrc, TTgt> obj)
		{
			return nilnul._rel.mate.be_.Monogamy<TSrc,TTgt>.Singleton.be( obj.mate );
		}

	}
}

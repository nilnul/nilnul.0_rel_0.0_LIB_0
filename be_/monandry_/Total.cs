﻿using nilnul.relation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be_.monandry_
{
	/// <summary>
	/// alias: func
	/// </summary>
	public class Total<TSrc, TTgt>
	   :
		nilnul.obj_.singular.Heir< Total<TSrc, TTgt>>
		,
		rel.BeI2<TSrc, TTgt>
	{
		public bool be(Rel3<TSrc, TTgt> obj)
		{
			return rel.be_.Monandry<TSrc, TTgt>.Singleton.be(obj)
				&&
				nilnul.rel.be_.Total<TSrc,TTgt>.Singleton.be(
					obj
				);
		}
	}
}

﻿using nilnul.relation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be_.monandry_
{
	/// <summary>
	/// monandry and surjective. each src can have no tgt. If a src does have a tgt, it can have only one. Every tgt has a src.
	/// alias: dict. This is inline with the defintion of dict in dotNet core library where key cannot be null whileas val can be null (a key can have no val.). 
	/// </summary>
	public class Surjective<TSrc, TTgt>
	   :
		nilnul.obj_.singular.Heir< Surjective<TSrc, TTgt>>
		,
		rel.BeI2<TSrc, TTgt>
	{
		public bool be(Rel3<TSrc, TTgt> obj)
		{
			return rel.be_.Monandry<TSrc, TTgt>.Singleton.be(obj)
				&&
				nilnul.rel.be_.Monogyny<TSrc,TTgt>.Singleton.be(
					obj
				);
		}
	}
}

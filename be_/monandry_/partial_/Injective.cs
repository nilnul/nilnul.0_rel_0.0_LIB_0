﻿using nilnul.relation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be_.monandry_.partial_
{
	/// <summary>
	/// there may be widowTgtS
	/// </summary>
	public class Injective<TSrc, TTgt>
	   :
		nilnul.obj_.singular.Heir< Injective<TSrc, TTgt>>
		,
		rel.BeI2<TSrc, TTgt>
	{
		public bool be(Rel3<TSrc, TTgt> obj)
		{
			return monandry_.Partial<TSrc, TTgt>.Singleton.be(obj)
				&&
				nilnul._rel.mate.be_.Monogyny<TSrc,TTgt>.Singleton.be(
					obj.mate
				);
		}
	}
}

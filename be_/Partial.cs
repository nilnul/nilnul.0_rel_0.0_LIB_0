﻿
using nilnul.relation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be_
{
	/// <summary>
	/// alias: complete.
	/// if the domain of the match equals to the domain of the rel.
	/// </summary>
	public class Partial<TSrc, TTgt>
	   :
		nilnul.obj_.singular.Heir<Partial<TSrc, TTgt>>
		,
		rel.BeI2<TSrc, TTgt>
	{
		public bool be(Rel3<TSrc, TTgt> obj)
		{
			return obj._domainWidows.Any();
		}
	}
}

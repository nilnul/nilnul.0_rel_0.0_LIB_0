﻿using nilnul.relation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be_.partial_.monandry_.injective_
{
	/// <summary>
	/// bijective relation if regardless of widowSrcS
	/// </summary>
	public class Surjective<TSrc, TTgt>
	   :
		nilnul.obj_.singular.Heir< Surjective<TSrc, TTgt>>
		,
		rel.BeI2<TSrc, TTgt>
	{
		public bool be(Rel3<TSrc, TTgt> obj)
		{
			return partial_.monandry_.Injective<TSrc, TTgt>.Singleton.be(obj)
				&&
				nilnul.rel.be_.Surjective<TSrc,TTgt>.Singleton.be(
					obj
				);
		}
	}
}

﻿namespace nilnul.rel.be_
{
	/// <summary>
	/// the domain of the rel is the domain of the mate.
	/// </summary>
	/// <remarks>
	/// alias:
	///		left total
	///		total
	///		searial
	///			"s" in "Surjective"
	/// </remarks>
	public interface ISerial { }
}

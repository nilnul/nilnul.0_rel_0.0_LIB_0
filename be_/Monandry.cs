﻿using nilnul.relation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be_
{
	/// <summary>
	/// the mate is monandry.
	/// 
	/// </summary>
	/// <remarks>
	/// disregard widowSrcS, there is one and only one tgt for each non-widow src.
	/// also called partial-or-total function
	///
	/// </remarks>
	public class Monandry<TSrc, TTgt>
	   :
		nilnul.obj_.singular.Heir<Monandry<TSrc, TTgt>>
		,
		rel.BeI2<TSrc, TTgt>
	{
		public bool be(Rel3<TSrc, TTgt> obj)
		{
			return 
				nilnul._rel.mate.be_.Monandry<TSrc,TTgt>.Singleton.be(
					obj.mate
				);
		}
	}
}

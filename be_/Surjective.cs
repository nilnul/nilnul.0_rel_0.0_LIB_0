﻿using nilnul.obj.seq;
using nilnul.relation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be_
{
	/// <summary>
	/// if the range of the match equals to the range of the relation.
	/// </summary>
	public class Surjective<TSrc, TTgt> :
		nilnul.obj_.singular.Heir<Surjective<TSrc, TTgt>>
		,
		rel.BeI2<TSrc, TTgt>
	{
		public bool be(Rel3<TSrc, TTgt> obj)
		{
			return obj._rangeWidows.None()
			;
		}
	}
}

﻿using nilnul.relation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be_.total_
{
	/// <summary>
	/// there is one and only one src for each tgt
	/// </summary>
	public class Monogyny<TSrc, TTgt>
	   :
		nilnul.obj_.singular.Heir< Monogyny<TSrc, TTgt>>
		,
		rel.BeI2<TSrc, TTgt>
	{
		public bool be(Rel3<TSrc, TTgt> obj)
		{
			return be_.Total<TSrc, TTgt>.Singleton.be(obj)
				&&
				nilnul.rel.be_.Monogyny<TSrc,TTgt>.Singleton.be(
					obj
				);
		}
	}
}

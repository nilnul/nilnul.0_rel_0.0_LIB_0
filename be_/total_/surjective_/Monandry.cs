﻿using nilnul.relation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be_.total_.surjective_
{
	/// <summary>
	/// surjective func
	/// </summary>
	public class Monandry<TSrc, TTgt>
	   :
		nilnul.obj_.singular.Heir< Monandry<TSrc, TTgt>>
		,
		rel.BeI2<TSrc, TTgt>
	{
		public bool be(Rel3<TSrc, TTgt> obj)
		{
			return total_.Surjective<TSrc, TTgt>.Singleton.be(obj)
				&&
				nilnul.rel.be_.Monandry<TSrc,TTgt>.Singleton.be(
					obj
				);
		}
	}
}

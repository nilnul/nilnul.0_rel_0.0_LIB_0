﻿using nilnul.relation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be_.total_.surjective_
{
	/// <summary>
	/// </summary>
	public class Monogamy<TSrc, TTgt>
	   :
		nilnul.obj_.singular.Heir< Monogamy<TSrc, TTgt>>
		,
		rel.BeI2<TSrc, TTgt>
	{
		public bool be(Rel3<TSrc, TTgt> obj)
		{
			return total_.Surjective<TSrc, TTgt>.Singleton.be(obj)
				&&
				nilnul.rel.be_.Monogamy<TSrc,TTgt>.Singleton.be(
					obj
				);
		}
	}
}

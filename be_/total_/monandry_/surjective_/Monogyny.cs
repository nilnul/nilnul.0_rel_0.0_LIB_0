﻿using nilnul.relation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be_.total_.monandry_.surjective_
{
	/// <summary>
	/// 
	/// </summary>
	public class Monogyny<TSrc, TTgt>
	   :
		nilnul.obj_.singular.Heir< Monogyny<TSrc, TTgt>>
		,
		rel.BeI2<TSrc, TTgt>
	{
		public bool be(Rel3<TSrc, TTgt> obj)
		{
			return total_.monandry_.Surjective<TSrc, TTgt>.Singleton.be(obj)
				&&
				nilnul._rel.mate.be_.Monogyny<TSrc,TTgt>.Singleton.be(
					obj.mate
				);
		}
	}
}

﻿
using nilnul.obj.seq;
using nilnul.relation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.rel.be_
{
	/// <summary>
	/// alias: complete.
	/// if the domain of the match equals to the domain of the rel.
	/// </summary>
	///
	[Obsolete("change this to Serial, as total is used in " + nameof(nilnul.rel_.net_.ITotal))]
	public class Total<TSrc, TTgt>
	   :
		nilnul.obj_.singular.Heir<Total<TSrc, TTgt>>
		,
		rel.BeI2<TSrc, TTgt>
		,
		ISerial
	{
		public bool be(Rel3<TSrc, TTgt> obj)
		{
			return obj._domainWidows.None();
		}
	}
}
